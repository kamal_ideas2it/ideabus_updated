package com.ideas2it.ideabus.servlets;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ideas2it.ideabus.busStop.BusStop;
import com.ideas2it.ideabus.busStop.controller.BusStopController;
import com.ideas2it.ideabus.util.Constant;

/**
 * 1.Get inputs from web page and store them into database
 * 2.Get data from database based on conditions
 *   and check whether given inputs exists or not.
 * 3.Display data on web page from database and
 *   get the modified inputs and update the record in database.
 * 4.Remove the selected busStop from busStop table.
 */
public class BusStopServlet extends HttpServlet {  
    private static final long serialVersionUID = 1L;
    private HttpSession session;
    private RequestDispatcher requestDispatcher;
    private BusStopController busStopController;
    
    /**
     * Get the inputs from web page and pass them to service
     * @param request
     *              - contains object of HttpServletRequest.
     * @param response
     *              - contains object of HttpServletResponse.
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response)  
                                        throws ServletException, IOException {
        response.setContentType(Constant.TEXT_HTML);//setting the content type  
        String name = request.getParameter(Constant.STOP);
		busStopController = new BusStopController();
        String message = busStopController.addBusStop(name);
        if (message == Constant.ADDED) {
            response.sendRedirect(Constant.JSP_FILES
                + Constant.AGENT + Constant.OPERATIONS + Constant.JSP);
        } else {
            response.sendRedirect(Constant.JSP_FILES
                + Constant.ADD + Constant.BUS_STOP_1 + Constant.JSP);
        }
    }
    
    /**
     * Get the value of submit from web page and based on its value,
     * call respective functions like,
     * If it's value is
     * 2. view then view busStops.
     * 3. update then display busStops later update selected busStop.
     * 4. delete then delete selected busStop.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void doGet(HttpServletRequest request
                       , HttpServletResponse response)  
                       throws ServletException, IOException {
                           
        response.setContentType(Constant.TEXT_HTML);
        String count = request.getParameter(Constant.COUNT);
        String submit = request.getParameter(Constant.SUBMIT);
        if (submit.equals(Constant.VIEW)) {
            displayBusStops(request,response,Constant.VIEW,count);
        } else if (submit.equals(Constant.UPDATE_1)) {
            displayBusStops(request,response,Constant.UPDATE_1,count);
        } else if (submit.equals(Constant.UPDATE_1 + " " + Constant.BUS_STOP)) {
            updateBusStop(request,response);
        }else if (submit.equals(Constant.DELETE_1)) {
            displayBusStops(request,response,Constant.DELETE_1,count);
        } else if (submit.equals(Constant.DELETE_1 + " " + Constant.BUS_STOP)) {
            deleteBusStop(request,response);
        }
    }

    /**
     * 1.Get the list of the busStops from busStop table.
     * 2.Set the busStops into the attribute.
     * 3.Display them on web page.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     * @param fileName
     *              - contains a string indicating a name of the file.
     */
    public void displayBusStops(HttpServletRequest request,
        HttpServletResponse response, String fileName, String count)  
        throws ServletException, IOException { 
        busStopController = new BusStopController();
        List<String> stops = busStopController.getStops(count);
        if (true == stops.isEmpty()) {
            response.sendRedirect(Constant.ERROR + Constant.JSP);
        }
        session = request.getSession();
        session.setAttribute(Constant.BUS_STOPS, stops);
        requestDispatcher = request.getRequestDispatcher
            ( Constant.JSP_FILES + fileName + Constant.BUS_STOP_1 + Constant.JSP);
        requestDispatcher.forward(request, response);
        
    }
    
    /**
     * 1.Get the selected busStop from list and input.
     * 2.Update name of the busStop with input.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void updateBusStop(HttpServletRequest request
                       , HttpServletResponse response)  
                       throws ServletException, IOException {
        response.setContentType(Constant.TEXT_HTML);
        String busStop = request.getParameter(Constant.BUS_STOP);
        String input = request.getParameter(Constant.NAME);
        busStopController = new BusStopController();
        busStopController.updateBusStop(busStop,input);
        response.sendRedirect(Constant.JSP_FILES
                + Constant.AGENT +  Constant.OPERATIONS + Constant.JSP);
    }
        
    /**
     * 1.Get the selected busStop from list.
     * 2.Delete instance of the selected busStop.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void deleteBusStop(HttpServletRequest request
                       , HttpServletResponse response)  
                       throws ServletException, IOException {
        response.setContentType(Constant.TEXT_HTML);
        String busStop = request.getParameter(Constant.BUS_STOP);
        busStopController = new BusStopController();
        busStopController.deleteBusStop(busStop);
        response.sendRedirect(Constant.JSP_FILES
                + Constant.AGENT +  Constant.OPERATIONS + Constant.JSP);
    }
}
