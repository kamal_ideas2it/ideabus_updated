package com.ideas2it.ideabus;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;  

import com.ideas2it.ideabus.booking.controller.BookingController;
import com.ideas2it.ideabus.ticket.Ticket;
import com.ideas2it.ideabus.util.Constant;

/**
 * 1.Get inputs from web page and store them into database
 * 2.Get data from database based on conditions
 * and check whether given inputs exists or not.
 */
public class BookingServlet extends HttpServlet {  
    private static final long serialVersionUID = 1L;
    private BookingController bookingController;

    /**
     * Get the inputs from web page and pass them to service
     * @param request
     *              - contains object of HttpServletRequest.
     * @param response
     *              - contains object of HttpServletResponse.
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response)  
                                        throws ServletException, IOException {
        response.setContentType("text/html");//setting the content type  
        HttpSession session = request.getSession();  
            String userEmailId 
                     = (String)session.getAttribute(Constant.EMAILID);
        int numberOfPassengers = Integer.parseInt(request.getParameter
                                               (Constant.NUMBER_OF_PASSENGERS));
        String[] firstName = new String[numberOfPassengers];
        String[] lastName = new String[numberOfPassengers]; 
        String[] gender = new String[numberOfPassengers];
        String[] seat = new String[numberOfPassengers]; 
        int[] age = new int[numberOfPassengers];
        for ( int index = 0 ; index < numberOfPassengers ; index++) {
		    firstName[index]= request.getParameter((Constant.FIRST_NAME+index));
            lastName[index] = request.getParameter((Constant.LAST_NAME+index));
            gender[index] = request.getParameter((Constant.GENDER+index));
            age[index] = Integer.parseInt(request.getParameter
                                                        ((Constant.AGE+index)));
            seat[index] = request.getParameter((Constant.SEAT_NAME+index));
	    }
        String emailId = request.getParameter(Constant.EMAILID);
        String phoneNumber = request.getParameter(Constant.PHONE_NUMBER);
        String date = request.getParameter(Constant.DATE);
        String journeyDate = request.getParameter(Constant.JOURNEY_DATE);
        String cardNumber = request.getParameter(Constant.CARD_NUMBER);
        String cardName = request.getParameter(Constant.CARD_NAME);
        float totalAmount = Float.parseFloat(request.getParameter
                                                       (Constant.TOTAL_AMOUNT));
        String regNumber = request.getParameter(Constant.REGISTER_NUMBER);
		bookingController = new BookingController();
        bookingController.addBooking(numberOfPassengers, firstName, lastName,
                                       gender, age, phoneNumber, emailId, date,
                                       seat, userEmailId, cardNumber, 
                                       cardName, totalAmount, journeyDate, 
                                       regNumber);
        response.sendRedirect(Constant.JSP_FILES + Constant.SUCCESS
                                  + Constant.JSP);
    }

    /**
     * Get the inputs from web page and check if they exists in database
     * or not, then redirect to next web page based on condition.
     * @param request
     *              - contains object of HttpServletRequest.
     * @param response
     *              - contains object of HttpServletResponse.
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response)
                      throws ServletException, IOException {
        String submit = request.getParameter(Constant.SUBMIT);
        HttpSession session = request.getSession();  
        if (submit.equals(Constant.SHOW)) {
            String emailId 
                     = (String)session.getAttribute(Constant.EMAILID);
            displayBookedTicket(request, response, emailId);
        } else if (submit.equals(Constant.CANCEL)) {
            cancelBookedTicket(request, response);
        }
    }

    /**
     * Get the inputs from web page and pass them to service
     * @param request
     *              - contains object of HttpServletRequest.
     * @param response
     *              - contains object of HttpServletResponse.
     * @param emailId
     *              - Email Id of the user.
     */
    public void displayBookedTicket(HttpServletRequest request, 
                                   HttpServletResponse response, String emailId) 
                                   throws ServletException, IOException {
		bookingController = new BookingController();
        List<Ticket> tickets = bookingController.showBookingTicket(emailId);
        if (tickets != null) {
            request.setAttribute(Constant.TICKETS, tickets);  
            RequestDispatcher requestDispatcher = 
                      request.getRequestDispatcher(Constant.JSP_FILES 
                                  + Constant.SHOW_BOOKED_TICKET + Constant.JSP);  
            requestDispatcher.forward(request, response);
        }
        response.sendRedirect(Constant.JSP_FILES + Constant.ERROR
                                   + Constant.JSP);
    }

    /**
     * Get the inputs from web page and pass them to service
     * @param request
     *              - contains object of HttpServletRequest.
     * @param response
     *              - contains object of HttpServletResponse.
     * @param emailId
     *              - Email Id of the user.
     */
    public void cancelBookedTicket(HttpServletRequest request, 
                                   HttpServletResponse response) 
                                   throws ServletException, IOException {
        String ticketNumber = request.getParameter(Constant.TICKET_NUMBER);
		bookingController = new BookingController();
        bookingController.cancelBookedTicket(ticketNumber);
        response.sendRedirect(Constant.JSP_FILES + Constant.SUCCESS
                                   + Constant.JSP);
    }
}
