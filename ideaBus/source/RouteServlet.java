package com.ideas2it.ideabus.servlets;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;  
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ideas2it.ideabus.busStop.BusStop;
import com.ideas2it.ideabus.route.Route;
import com.ideas2it.ideabus.route.controller.RouteController;
import com.ideas2it.ideabus.util.Constant;

/**
 * 1.Get inputs from web page and store them into database
 * 2.Get data from database based on conditions
 *   and check whether given inputs exists or not.
 * 3.Display data on web page from database and
 *   get the modified inputs and update the record in database.
 * 4.Remove the selected route from route table.
 */
public class RouteServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private HttpSession session;
    private RequestDispatcher requestDispatcher;
    private RouteController routeController;

    /**
     * Get the input from web page and add it into the route table,
     * then redirect to their respective pages based on results.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response)  
        throws ServletException, IOException {
        response.setContentType(Constant.TEXT_HTML);//setting the content type  
        String name = request.getParameter(Constant.NAME);
        String busStop = request.getParameter(Constant.BUS_STOP);
        routeController = new RouteController();
        String message = routeController.addRoute(name, busStop);
        if (message == Constant.ADDED) {
            response.sendRedirect(Constant.JSP_FILES
                + Constant.AGENT +  Constant.OPERATIONS + Constant.JSP);
        } else {
            response.sendRedirect(Constant.JSP_FILES
                + Constant.ADD +  Constant.ROUTE + Constant.JSP);
        }
    }
    
    /**
     * Get the value of submit from web page and based on its value,
     * call respective functions like,
     * If it's value is
     * 2. view then view routes.
     * 3. update then display routes later update selected route.
     * 4. delete then delete selected route.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void doGet(HttpServletRequest request
                       , HttpServletResponse response)  
                       throws ServletException, IOException {
                           
        response.setContentType(Constant.TEXT_HTML);
        String submit = request.getParameter(Constant.SUBMIT);
        if (submit.equals(Constant.VIEW)) {
            displayRoutes(request,response,Constant.VIEW);
        } else if (submit.equals(Constant.VIEW + " " + Constant.STOPS)) {
            displayStops(request,response,Constant.VIEW);
        } else if (submit.equals(Constant.UPDATE_1)) {
            displayRoutes(request,response,Constant.UPDATE_1);
        } else if (submit.equals(Constant.UPDATE_1 + " " + Constant.ROUTE)) {
            updateRoute(request,response);
        }else if (submit.equals(Constant.DELETE_1)) {
            displayRoutes(request,response,Constant.DELETE_1);
        } else if (submit.equals(Constant.DELETE_1 + " " + Constant.ROUTE)) {
            deleteRoute(request,response);
        }
    }

    /**
     * 1.Get the list of the routes from route table.
     * 2.Set the routes into the attribute.
     * 3.Display them on web page.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     * @param fileName
     *              - contains a string indicating a name of the file.
     */
    public void displayRoutes(HttpServletRequest request
                       , HttpServletResponse response, String fileName)  
                       throws ServletException, IOException { 
        routeController = new RouteController();
        List<String> routes = routeController.getRoutes();
        if (true == routes.isEmpty()) {
            response.sendRedirect(Constant.ERROR + Constant.JSP);
        }
        session = request.getSession();
        session.setAttribute(Constant.ROUTES, routes);
        requestDispatcher = request.getRequestDispatcher
            ( Constant.JSP_FILES + fileName + Constant.ROUTE_1 + Constant.JSP);
        requestDispatcher.forward(request, response);
        
    }
    
    
    /**
     * 1.Get the list of the stops from route table.
     * 2.Set the stops into the attribute.
     * 3.Display them on web page.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     * @param fileName
     *              - contains a string indicating a name of the file.
     */
    public void displayStops(HttpServletRequest request,
        HttpServletResponse response, String fileName)  
        throws ServletException, IOException { 
        routeController = new RouteController();
        String route = request.getParameter(Constant.ROUTE);
        List<String> stops = routeController.getStops(route);
        if (true == stops.isEmpty()) {
            response.sendRedirect(Constant.ERROR + Constant.JSP);
        }
        session = request.getSession();
        session.setAttribute(Constant.STOPS, stops);
        requestDispatcher = request.getRequestDispatcher
            ( Constant.JSP_FILES + fileName + Constant.STOPS_1 + Constant.JSP);
        requestDispatcher.forward(request, response);
        
    }
    
    
    /**
     * 1.Get the selected route from list and input.
     * 2.Update name of the route with input.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void updateRoute(HttpServletRequest request
                       , HttpServletResponse response)  
                       throws ServletException, IOException {
        response.setContentType(Constant.TEXT_HTML);
        String route = request.getParameter(Constant.ROUTE);
        String input = request.getParameter(Constant.NAME);
        String choice = request.getParameter(Constant.UPDATE_1);
        routeController = new RouteController();
        routeController.updateRoute(route,input,choice);
        response.sendRedirect(Constant.JSP_FILES
                + Constant.AGENT +  Constant.OPERATIONS + Constant.JSP);
    }
        
    /**
     * 1.Get the selected route from list.
     * 2.Delete instance of the selected route.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void deleteRoute(HttpServletRequest request
                       , HttpServletResponse response)  
                       throws ServletException, IOException {
        response.setContentType(Constant.TEXT_HTML);
        String route = request.getParameter(Constant.ROUTE);
        routeController = new RouteController();
        routeController.deleteRoute(route);
        response.sendRedirect(Constant.JSP_FILES
                + Constant.AGENT +  Constant.OPERATIONS + Constant.JSP);
    }
}
