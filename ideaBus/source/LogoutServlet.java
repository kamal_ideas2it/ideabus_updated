package com.ideas2it.ideabus;

import java.io.IOException;  
import java.io.PrintWriter;  
import javax.servlet.ServletException;  
import javax.servlet.http.Cookie;  
import javax.servlet.http.HttpSession;  
import javax.servlet.http.HttpServlet;  
import javax.servlet.http.HttpServletRequest;  
import javax.servlet.http.HttpServletResponse;

import com.ideas2it.ideabus.util.Constant;

public class LogoutServlet extends HttpServlet {  
    protected void doGet(HttpServletRequest request, HttpServletResponse response)  
                        throws ServletException, IOException {  
        response.setContentType("text/html");  
         
          
        Cookie ck=new Cookie(Constant.EMAILID,"");  
        ck.setMaxAge(0);  
        response.addCookie(ck);  
    response.sendRedirect("jsp files/logout.jsp");
    }
}
