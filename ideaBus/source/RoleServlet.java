package com.ideas2it.ideabus.servlets;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ideas2it.ideabus.role.Role;
import com.ideas2it.ideabus.role.controller.RoleController;
import com.ideas2it.ideabus.util.Constant;

/**
 * 1.Get inputs from web page and store them into database
 * 2.Get data from database based on conditions
 *   and check whether given inputs exists or not.
 * 3.Display data on web page from database and
 *   get the modified inputs and update the record in database.
 * 4.Remove the selected role from role table.
 */
public class RoleServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private HttpSession session;
    private RequestDispatcher requestDispatcher;
    private RoleController roleController;
    
    /**
     * Get the input from web page and add it into the role table,
     * then redirect to their respective pages.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void doPost(HttpServletRequest request ,HttpServletResponse response)  
        throws ServletException, IOException {
        response.setContentType(Constant.TEXT_HTML);
        String name = request.getParameter(Constant.NAME);
        roleController = new RoleController();
        String message = roleController.addRole(name);
        if (message.equals(Constant.ADDED)) {
            response.sendRedirect(Constant.JSP_FILES
                + Constant.ADMIN +  Constant.OPERATIONS + Constant.JSP);
        } else {
            response.sendRedirect(Constant.JSP_FILES
                + Constant.ADD +  Constant.ROLE + Constant.JSP);
        }
    }
    
    /**
     * Get the value of submit from web page and based on its value,
     * call respective functions like,
     * If it's value is
     * 2. view then view roles.
     * 3. update then display roles later update selected role.
     * 4. delete then delete selected role.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void doGet(HttpServletRequest request
                       , HttpServletResponse response)  
                       throws ServletException, IOException {                   
        response.setContentType(Constant.TEXT_HTML);
        String submit = request.getParameter(Constant.SUBMIT);
        if (submit.equals(Constant.VIEW)) {
            displayRoles(request,response,Constant.VIEW);
        } else if (submit.equals(Constant.UPDATE_1)) {
            displayRoles(request,response,Constant.UPDATE_1);
        } else if (submit.equals(Constant.UPDATE_1 + " " + Constant.ROLE)) {
            updateRole(request,response);
        }else if (submit.equals(Constant.DELETE_1)) {
            displayRoles(request,response,Constant.DELETE_1);
        } else if (submit.equals(Constant.DELETE_1 + " " + Constant.ROLE)) {
            deleteRole(request,response);
        }
    }

    /**
     * 1.Get the list of the roles from role table.
     * 2.Set the roles into the attribute.
     * 3.Display them on web page.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     * @param fileName
     *              - contains a string indicating a name of the file.
     */
    public void displayRoles(HttpServletRequest request
        , HttpServletResponse response, String fileName)  
        throws ServletException, IOException { 
        roleController = new RoleController();
        List<String> roles = roleController.getRoles();
        if (roles.isEmpty() == true) {
            response.sendRedirect(Constant.ERROR + Constant.JSP);
        }
        session = request.getSession();
        session.setAttribute(Constant.ROLES, roles);
        requestDispatcher = request.getRequestDispatcher
            ( Constant.JSP_FILES + fileName + Constant.ROLE_1 + Constant.JSP);
        requestDispatcher.forward(request, response);
        
    }
    
    /**
     * 1.Get the selected role from list and input.
     * 2.Update name of the role with input.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void updateRole(HttpServletRequest request
        , HttpServletResponse response)  
        throws ServletException, IOException {
        response.setContentType(Constant.TEXT_HTML);
        String role = request.getParameter(Constant.ROLE);
        String input = request.getParameter(Constant.NAME);
        roleController = new RoleController();
        roleController.updateRole(role,input);
        response.sendRedirect(Constant.JSP_FILES
                + Constant.ADMIN +  Constant.OPERATIONS + Constant.JSP);
    }
        
    /**
     * 1.Get the selected role from list.
     * 2.Delete instance of the selected role.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void deleteRole(HttpServletRequest request,
        HttpServletResponse response)  
        throws ServletException, IOException {
        response.setContentType(Constant.TEXT_HTML);
        String role = request.getParameter(Constant.ROLE);
        roleController = new RoleController();
        roleController.deleteRole(role);
        response.sendRedirect(Constant.JSP_FILES
                + Constant.ADMIN +  Constant.OPERATIONS + Constant.JSP);
    }
}
