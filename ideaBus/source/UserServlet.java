package com.ideas2it.ideabus.servlets;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.Cookie;  
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ideas2it.ideabus.user.User;
import com.ideas2it.ideabus.user.controller.UserController;
import com.ideas2it.ideabus.util.Constant;

/**
 * This class UserServlet extends HttpServlet
 * and perform the following functions,<br/>
 * 1.Get inputs from web page and store them into user table.<br/>
 * 2.Get data from database based on conditions.<br/>
 * 3.Display data on web page from database.<br/>
 * 4.Get the modified inputs to update the record in user table.<br/>
 * 5.Get the mailId of the user and disable it's record from user table.
 */
public class UserServlet extends HttpServlet {
    public static final long serialVersionUID = 1L;
    private HttpSession session;
    private RequestDispatcher requestDispatcher;
    private UserController userController;
    
    /**
     * Get the inputs and add them into the user table then,
     * doRedirect redirects to operations based on arguments role<br/>
     * and feedback.
     *
     * @param request
     *              - contains the instance of HttpServletRequest.
     * @param response
     *              - contains the instance of HttpServletResponse.
     * @throws ServletException
     *              - if servlet encounters difficulty.
     * @throws IOException
     *              - if fail occurs during I/O operations.
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        response.setContentType(Constant.TEXT_HTML);
        String firstName = request.getParameter(Constant.FIRST_NAME);
        String lastName = request.getParameter(Constant.LAST_NAME);
        String gender = request.getParameter(Constant.GENDER);
        String role = request.getParameter(Constant.ROLE);
        String age = request.getParameter(Constant.AGE);
        String phone = request.getParameter(Constant.PHONE_NUMBER);
        String emailId = request.getParameter(Constant.EMAILID);
        String password = request.getParameter(Constant.PASSWORD);
        userController = new UserController();
        String feedback = userController.addUser(firstName, lastName, gender,
            age, phone, emailId, password, role);
        doRedirect(role, feedback, request, response);    
    }
    
    /**
     * Redirect user to their respective pages based on their role.
     *
     * @param role
     *              - name of role.
     * @param feedback
     *              - a feedback or response in doPost operation.
     * @param request
     *              - instance of HttpServletRequest.
     * @param response
     *              - instance of HttpServletResponse.
     */
    private void doRedirect(String role, String feedback,
        HttpServletRequest request, HttpServletResponse response) 
        throws ServletException, IOException {
        switch(role) {
            case Constant.ADMIN:
                redirectAdmin(feedback, request, response);
                break;
            case Constant.AGENT:
                redirectAgent(feedback, request, response);
                break;
            case Constant.CUSTOMER:
                redirectCustomer(feedback, request, response);
                break;
        }
    }

    /**
     * Peform post operations for admin registeration like,<br/>
     * If user is,<br/>
     * 1.Is added to user table then redirect to operations page.<br/>
     * 2.Is not added to user table then redirect to error page.<br/>
     * 3.Already in user table then redirect to register page.
     *
     * @param feedback
     *              - a feedback or response in doPost operation.
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    private void redirectAdmin(String feedback, HttpServletRequest request, 
        HttpServletResponse response) 
        throws ServletException, IOException {
        if (Constant.REGISTERED.equals(feedback)) {
            response.sendRedirect(Constant.JSP_FILES + Constant.ADMIN 
                                      + Constant.OPERATIONS + Constant.JSP);
        } else if (Constant.ERROR.equals(feedback)) {
            response.sendRedirect(Constant.JSP_FILES
                                      + Constant.ERROR + Constant.JSP);
        } else {
            response.sendRedirect(Constant.JSP_FILES + Constant.ADMIN 
                                     + Constant.REGISTER + Constant.JSP);
        }    
    }
    
    /**
     * Peform post operations for agent registerations like,<br/>
     * If user is,<br/>
     * 1.Is added to user table then redirect to operations page.<br/>
     * 2.Is not added to user table then redirect to error page.<br/>
     * 3.Already in user table then redirect to register page.<br/>
     *
     * @param feedback
     *              - a feedback or response in doPost operation.
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    private void redirectAgent(String feedback, HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException{
        if (Constant.REGISTERED.equals(feedback)) {
            response.sendRedirect(Constant.JSP_FILES + Constant.ADMIN 
                                      + Constant.OPERATIONS + Constant.JSP);
        } else if (Constant.ERROR.equals(feedback)) {
            response.sendRedirect(Constant.JSP_FILES + Constant.ERROR 
                                      + Constant.JSP);
        } else {
            response.sendRedirect(Constant.JSP_FILES + Constant.AGENT 
                                       + Constant.REGISTER + Constant.JSP);
        }
    }
    
    /**
     * Peform post operations for customer registerations like,<br/>
     * If user is,<br/>
     * 1.Is added to user table then redirect to operations page.<br/>
     * 2.Is not added to user table then redirect to error page.<br/>
     * 3.Already in user table then redirect to register page.
     *
     * @param feedback
     *              - a feedback or response in doPost operation.
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    private void redirectCustomer(String feedback,
        HttpServletRequest request, HttpServletResponse response) 
        throws ServletException, IOException{
        if (Constant.REGISTERED.equals(feedback)) {
            response.sendRedirect(Constant.JSP_FILES + Constant.CUSTOMER 
                                     + Constant.OPERATIONS + Constant.JSP);
        } else if (Constant.ERROR.equals(feedback)) {
            response.sendRedirect(Constant.JSP_FILES + Constant.ERROR
                                     + Constant.JSP);
        } else {
            response.sendRedirect(Constant.JSP_FILES + Constant.CUSTOMER 
                                     + Constant.REGISTER + Constant.JSP);
        }
    }
    
    /**
     * Get the value of choice and based on its value 
     * call respective functions like,<br/>
     * If choice's value is,<br/>
     * 1. login do login operation.<br/>
     * 2. view then view user details.<br/>
     * 3. update first display user details then do update function.<br/>
     * 4. delete do delete operation.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {     
        session = request.getSession();
        String choice = request.getParameter(Constant.SUBMIT);
        if (choice.equals(Constant.LOGIN)) {
            checkAndLogin(request, response);
        } else if (choice.equals(Constant.VIEW)) {
            checkAndViewUser(request, response);
        } else if (choice.equals(Constant.UPDATE_1)) {
            String emailId = (String)session.getAttribute(Constant.EMAILID);
            checkAndDisplayUser(request, response, emailId);
        } else if (choice.equals(Constant.UPDATE_1 + " " + Constant.AGENT)) {
            String emailId = request.getParameter(Constant.EMAILID);   
            session.setAttribute(Constant.MAILID, emailId);
            checkAndDisplayUser(request, response, emailId);
        } else if (choice.equals(Constant.UPDATE_2)) {
            String mailId = (String)session.getAttribute(Constant.EMAILID);
            updateUser(request, response, mailId);
        } else if (choice.equals(Constant.DELETE_1)) {
            String emailId = (String)session.getAttribute(Constant.EMAILID);
            deleteUser(request, response, emailId);
        } else if (choice.equals(Constant.DELETE_1 + " " + Constant.AGENT)) {
            String emailId = request.getParameter(Constant.EMAILID);
            deleteUser(request, response, emailId);
        }
    }
    
    /**
     * Get the input mailId and check if user for given mailId exists,<br/>
     * then based on return value and their role
     * redirect to their respective operations.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    private void checkAndLogin(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        String emailId = request.getParameter(Constant.EMAILID);   
        String password = request.getParameter(Constant.PASSWORD);
        String role = request.getParameter(Constant.ROLE);
        session = request.getSession();
        session.setAttribute(Constant.EMAILID, emailId);
        userController = new UserController();
        boolean result = userController.checkUser(emailId, password, role);
        if ( true == result ) {
            Cookie ck=new Cookie(Constant.EMAILID, emailId);  
            response.addCookie(ck);  
            response.sendRedirect(Constant.JSP_FILES
                + role + Constant.OPERATIONS + Constant.JSP);
        } else {
            response.sendRedirect(Constant.JSP_FILES + Constant.ERROR + Constant.JSP);
        }        
    }
    
    /**
     * 1.Get the details of the user using their mailId.<br/>
     * 2.Set the user into the attribute.<br/>
     * 3.Display user details.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    private void checkAndViewUser(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        session = request.getSession();
        String emailId = (String)session.getAttribute(Constant.EMAILID);
        userController = new UserController();
        User user = userController.getUserByEmailId(emailId);
        if ( null == user ) {
            response.sendRedirect(Constant.JSP_FILES + Constant.ERROR 
                                     + Constant.JSP);
        }
        request.setAttribute(Constant.USER_1,user);  
        requestDispatcher = request.getRequestDispatcher
                                 ( Constant.JSP_FILES + Constant.VIEW 
                                     + Constant.USER_2 + Constant.JSP);
        requestDispatcher.forward(request, response); 
    }
    
    /**
     * 1.Check if the user for the given mailID and role exists.<br/>
     * 2.Get the details of the user using their mailId.<br/>
     * 3.Set the user into the attribute.<br/>
     * 4.Display user details in input fields.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     * @param emailId
     *              - contains emailId of user.
     */
    private void checkAndDisplayUser(HttpServletRequest request,
        HttpServletResponse response,String emailId)
        throws ServletException, IOException {
        userController = new UserController();
        String role = request.getParameter(Constant.ROLE);
        boolean isUser = userController.isUser(emailId,role);
        if ( true == isUser ) {
            User user = userController.getUserByEmailId(emailId);
            request.setAttribute(Constant.USER_1,user);
            requestDispatcher = request.getRequestDispatcher
                                (Constant.JSP_FILES + Constant.UPDATE_1
                                     + Constant.USER_2 + Constant.JSP);
            requestDispatcher.forward(request, response);
        } else {
            response.sendRedirect(Constant.JSP_FILES + Constant.ERROR
                                     + Constant.JSP);
        }    
    }
    
    /**
     * Get the inputs and update the user for the given mailId.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     * @param mailId
     *              - contains mailId of user.
     */
    private void updateUser(HttpServletRequest request, 
        HttpServletResponse response, String mailId)
        throws ServletException, IOException  {
        response.setContentType(Constant.TEXT_HTML);
        String firstName = request.getParameter(Constant.FIRST_NAME);
        String lastName = request.getParameter(Constant.LAST_NAME);
        String gender = request.getParameter(Constant.GENDER);
        String age = request.getParameter(Constant.AGE);
        String phone = request.getParameter(Constant.PHONE_NUMBER);
        String emailId = request.getParameter(Constant.EMAILID);
        String password = request.getParameter(Constant.PASSWORD);
        String status = request.getParameter(Constant.STATUS);
        userController = new UserController();
        userController.updateUser(mailId, firstName, lastName, gender, age,
                                      phone, emailId, password, status);
        response.sendRedirect(Constant.JSP_FILES + Constant.UPDATE_1 
                                 + Constant.JSP);
    }
    
    /**
     * 1.Check does the user exists using their mailId and role.
     * 2.Then based on above operation change the status of user.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     * @param emailId
     *              - contains emailId of user.
     */
    private void deleteUser(HttpServletRequest request,
        HttpServletResponse response, String emailId)
        throws ServletException, IOException {
        userController = new UserController();
        String role = request.getParameter(Constant.ROLE);
        boolean isUser = userController.isUser(emailId,role);
        if ( true == isUser ) {
            userController.deleteUser(emailId);
        }
        response.sendRedirect( Constant.JSP_FILES
            + Constant.DELETE_1 + Constant.JSP);
    }
}
