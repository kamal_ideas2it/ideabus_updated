package com.ideas2it.ideabus.servlets;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ideas2it.ideabus.organization.Organization;
import com.ideas2it.ideabus.organization.controller.OrganizationController;
import com.ideas2it.ideabus.util.Constant;

/**
 * 1.Get inputs from web page and store them into database
 * 2.Get data from database based on conditions
 *   and check whether given inputs exists or not.
 * 3.Display data on web page from database and
 *   get the modified inputs and update the record in database.
 * 4.Remove the selected organization from organization table.
 */
public class OrganizationServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private HttpSession session;
    private RequestDispatcher requestDispatcher;
    private OrganizationController organizationController;
    
    /**
     * Get the input from web page and add it into the organization table,
     * then redirect to their respective pages.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void doPost(HttpServletRequest request ,HttpServletResponse response)  
        throws ServletException, IOException {
        response.setContentType(Constant.TEXT_HTML);
        organizationController = new OrganizationController();
        String name = request.getParameter(Constant.ORGANIZATION);
        String message = organizationController.addOrganization(name);
        if (message == Constant.ADDED) {
            response.sendRedirect(Constant.JSP_FILES
                + Constant.AGENT +  Constant.OPERATIONS + Constant.JSP);
        } else {
            response.sendRedirect(Constant.JSP_FILES
                + Constant.ADD +  Constant.ORGANIZATION + Constant.JSP);
        }
    }    
        
    /**
     * Get the value of submit from web page and based on its value,
     * call respective functions like,
     * If it's value is
     * 2. view then view organizations.
     * 3. update then display organizations later update selected organization.
     * 4. delete then delete selected organization.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void doGet(HttpServletRequest request
                       , HttpServletResponse response)  
                       throws ServletException, IOException {
                           
        response.setContentType(Constant.TEXT_HTML);
        String submit = request.getParameter(Constant.SUBMIT);
        if (submit.equals(Constant.VIEW)) {
            displayOrganizations(request,response,Constant.VIEW);
        } else if (submit.equals(Constant.UPDATE_1)) {
            displayOrganizations(request,response,Constant.UPDATE_1);
        } else if (submit.equals(Constant.UPDATE_1 + " " + Constant.ORGANIZATION)) {
            updateOrganization(request,response);
        }else if (submit.equals(Constant.DELETE_1)) {
            displayOrganizations(request,response,Constant.DELETE_1);
        } else if (submit.equals(Constant.DELETE_1 + " " + Constant.ORGANIZATION)) {
            deleteOrganization(request,response);
        }
    }
    
    /**
     * 1.Get the list of the organizations from organization table.
     * 2.Set the organizations into the attribute.
     * 3.Display them on web page.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     * @param fileName
     *              - contains a string indicating a name of the file.
     */
    public void displayOrganizations(HttpServletRequest request
                       , HttpServletResponse response, String fileName)  
                       throws ServletException, IOException { 
        organizationController = new OrganizationController();
        List<String> organizations = organizationController.getOrganizations();
        if (organizations.isEmpty() == true) {
            response.sendRedirect(Constant.ERROR + Constant.JSP);
        }
        session = request.getSession();
        session.setAttribute(Constant.ORGANIZATIONS, organizations);
        requestDispatcher = request.getRequestDispatcher
            ( Constant.JSP_FILES + fileName + Constant.ORGANIZATION_1 + Constant.JSP);
        requestDispatcher.forward(request, response);
        
    }
    
    /**
     * 1.Get the selected organization from list and input.
     * 2.Update name of the organization with input.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void updateOrganization(HttpServletRequest request
                       , HttpServletResponse response)  
                       throws ServletException, IOException {
        response.setContentType(Constant.TEXT_HTML);
        String organization = request.getParameter(Constant.ORGANIZATION);
        String input = request.getParameter(Constant.NAME);
        organizationController = new OrganizationController();
        organizationController.updateOrganization(organization,input);
        response.sendRedirect(Constant.JSP_FILES
                + Constant.AGENT +  Constant.OPERATIONS + Constant.JSP);
    }
        
    /**
     * 1.Get the selected organization from list.
     * 2.Delete instance of the selected organization.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void deleteOrganization(HttpServletRequest request
                       , HttpServletResponse response)  
                       throws ServletException, IOException {
        response.setContentType(Constant.TEXT_HTML);
        String organization = request.getParameter(Constant.ORGANIZATION);
        organizationController = new OrganizationController();
        organizationController.deleteOrganization(organization);
        response.sendRedirect(Constant.JSP_FILES
                + Constant.AGENT +  Constant.OPERATIONS + Constant.JSP);
    }
}
