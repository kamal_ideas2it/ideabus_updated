package com.ideas2it.ideabus.servlets;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher; 
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.ideas2it.ideabus.busDetail.BusDetail;
import com.ideas2it.ideabus.busDetail.controller.BusDetailController;
import com.ideas2it.ideabus.route.controller.RouteController;
import com.ideas2it.ideabus.seat.Seat;
import com.ideas2it.ideabus.type.Type;
import com.ideas2it.ideabus.type.controller.TypeController;
import com.ideas2it.ideabus.util.Constant;

/**
 * Perform following operations,
 * 1.Get inputs from web page and store them into busDetail table.
 * 2.Get data from database based on conditions
 *  and check whether given inputs exists or not.
 * 3.Display data from database on web page and
 *  get the modified inputs to update the record in busDetail table.
 * 4.Get the registeredNumber of the bus and disable it's record from busDetail table.
 */
public class BusDetailServlet extends HttpServlet {  
    private static final Logger logger = Logger.getLogger
        (BusDetailServlet.class);
    public static final long serialVersionUID = 1L;
    
    private BusDetailController busDetailController;
    private HttpSession session;
    private RequestDispatcher requestDispatcher;
    private TypeController typeController;
    private RouteController routeController;
    
    /**
     * Get the inputs from web page and add them into the busDetail table
     * then redirect to their respective pages.
     *
     * @param request
     *              - contains object of HttpServletRequest.
     * @param response
     *              - contains object of HttpServletResponse.
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response)  
                                        throws ServletException, IOException {
        try {
            response.setContentType(Constant.TEXT_HTML);//setting the content type
            String emailId = request.getParameter(Constant.MAILID);
            String name = request.getParameter(Constant.NAME);
            String registerNumber = request.getParameter(Constant.REGISTER_NUMBER);
            String route = request.getParameter(Constant.ROUTE);
            String type = request.getParameter(Constant.BUS_TYPE);
            String seat = request.getParameter(Constant.SEAT + Constant.NAME_1);
            float price = 0;
            try {
                price = Float.parseFloat(request.getParameter(Constant.PRICE));
            } catch (NumberFormatException exception) {
                response.sendRedirect(Constant.JSP_FILES + Constant.ERROR 
                                         + Constant.JSP);
            }
            String date = request.getParameter(Constant.DATE);
            busDetailController = new BusDetailController();
            busDetailController.addBusDetail(name, registerNumber, route,
                type,seat, price, emailId, date);
            response.sendRedirect(Constant.JSP_FILES + Constant.AGENT 
                                     + Constant.OPERATIONS + Constant.JSP);
        } catch (NullPointerException exception) {
            logger.error(exception);
            response.sendRedirect(Constant.JSP_FILES + Constant.ERROR 
                                     + Constant.JSP);
        }
    }

    /**
     * Get the value of submit from web page.
     * Based on submit value do following functions,
     * If value of submit is
     * 1.search then display the requested details of bus.
     *
     * @param request
     *              - contains object of HttpServletRequest.
     * @param response
     *              - contains object of HttpServletResponse.
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException  {
        String submit = request.getParameter(Constant.SUBMIT);
        session = request.getSession();  
        if (submit.equals(Constant.ADD)) {
            preloadData(request,response);
        } else if (submit.equals(Constant.VIEW)) {
            viewBuses(request, response,Constant.VIEW);
        } else if (submit.equals(Constant.UPDATE_1)) {
            viewRegisterNumbers(request, response,Constant.VIEW);
        } else if (submit.equals(Constant.UPDATE_1 + " " + Constant.BUS)) {
            displayBusDetail(request, response, Constant.UPDATE_1);
        } else if (submit.equals(Constant.UPDATE_2)) {
            updateBusDetail(request, response);
        } else if (submit.equals(Constant.ACTIVATE + " " + Constant.BUS)) {
            setBusStatus(request, response,Constant.ACTIVE);
        } else if (submit.equals(Constant.DEACTIVATE + " " + Constant.BUS)) {
            setBusStatus(request, response,Constant.INACTIVE);
        } else if (submit.equals(Constant.SEARCH)) {
            displayBusDetailByBusStops(request, response);
        }  else if (submit.equals(Constant.SELECT)) {
            displayAvailableSeats(request, response);
        } /*else if (submit.equals("select")) {
            displaySeat(request, response);
        } */
    }

    /**
     * Set attributes for routes and types.
     *
     * @param request
     *              - contains object of HttpServletRequest.
     * @param response
     *              - contains object of HttpServletResponse.
     */
    public void preloadData(HttpServletRequest request, 
        HttpServletResponse response) 
        throws ServletException, IOException  {
        typeController = new TypeController();
        routeController = new RouteController();
        List<String> types = typeController.getTypes();
        List<String> routes = routeController.getRoutes();
        session.setAttribute(Constant.TYPES, types);
        session.setAttribute(Constant.ROUTES, routes);
        requestDispatcher = request.getRequestDispatcher
            ( Constant.JSP_FILES + Constant.BUS_DETAIL + Constant.JSP);
        requestDispatcher.forward(request, response);
    }
        
    /**
     * 1.Get the details of the user's bus using user's mailId
     * 2.Set the busDetails into the attribute.
     * 3.Display it on web page.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     * @param emailId
     *              - contains emailId of user.
     */
    public void viewBuses(HttpServletRequest request
        ,HttpServletResponse response,String fileName)
        throws ServletException, IOException {
        busDetailController = new BusDetailController();
        String mailId = request.getParameter(Constant.MAILID);
        Set<BusDetail> busDetails 
                                = busDetailController.getBusesByEmailId(mailId);
        if ( null == busDetails ) {
            response.sendRedirect(Constant.ERROR + Constant.JSP);
        }
        request.setAttribute(Constant.BUS_DETAILS,busDetails);  
        requestDispatcher = request.getRequestDispatcher
                    ( Constant.JSP_FILES + fileName + Constant.BUS_DETAIL_1 
                         + Constant.JSP);
        requestDispatcher.forward(request, response);
    }
            
    /**
     * 1.Get the details of the user's bus using user's mailId
     * 2.Set the busDetails into the attribute.
     * 3.Display it on web page.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     * @param emailId
     *              - contains emailId of user.
     */
    public void viewRegisterNumbers(HttpServletRequest request
        ,HttpServletResponse response,String fileName)
        throws ServletException, IOException {
        busDetailController = new BusDetailController();
        String mailId = request.getParameter(Constant.MAILID);
        List<String> registerNumbers
            = busDetailController.getRegisterNumbers(mailId);
        if ( null == registerNumbers ) {
            response.sendRedirect(Constant.ERROR + Constant.JSP);
        }
        request.setAttribute(Constant.REGISTER_NUMBERS,registerNumbers);  
        requestDispatcher = request.getRequestDispatcher
            ( Constant.JSP_FILES + fileName + Constant.REGISTER_NUMBERS_1 + Constant.JSP);
        requestDispatcher.forward(request, response);
    }
    
    /**
     * Get the busDetail record from busDetail table and display them.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     * @param fileName
     *              - contains name of the file.
     */
    public void displayBusDetail(HttpServletRequest request ,
        HttpServletResponse response ,String fileName)
        throws ServletException, IOException {
        String registerNumber = request.getParameter(Constant.REGISTER_NUMBER);
        busDetailController = new BusDetailController();
        BusDetail busDetail = busDetailController.getBusDetail(registerNumber);
        if (null == busDetail) {
            response.sendRedirect(Constant.ERROR + Constant.JSP);
        }
        routeController = new RouteController();
        List<String> routes = routeController.getRoutes();
        request.setAttribute(Constant.ROUTES, routes);
        request.setAttribute(Constant.BUS_DETAIL,busDetail);
        requestDispatcher = request.getRequestDispatcher
            ( Constant.JSP_FILES + fileName
                + Constant.BUS_DETAIL_1 + Constant.JSP);
        requestDispatcher.forward(request, response);
    }
    
    /**
     * Get the inputs from input fields and update the details of bus.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void updateBusDetail(HttpServletRequest request
        , HttpServletResponse response)
        throws ServletException, IOException  {
        response.setContentType(Constant.TEXT_HTML);
        String organization = request.getParameter(Constant.ORGANIZATION);
        String route = request.getParameter(Constant.ROUTE);
        String type = request.getParameter(Constant.TYPE);
        float price = 0;
        try {
            price = Float.parseFloat(request.getParameter(Constant.PRICE));
        } catch (NumberFormatException exception) {
            response.sendRedirect(Constant.JSP_FILES + Constant.ERROR 
                                       + Constant.JSP);
        }
        String date = request.getParameter(Constant.DATE);
        String registerNumber = request.getParameter(Constant.REGISTER_NUMBER);
        String oldRegisterNumber = request.getParameter
            (Constant.REGISTRATION_NUMBER);
        busDetailController = new BusDetailController();
        busDetailController.updateBusDetail(organization, route, type,
            price, registerNumber, oldRegisterNumber, date);
        response.sendRedirect(Constant.JSP_FILES
            + Constant.AGENT + Constant.OPERATIONS + Constant.JSP);
    }
    
    /**
     * 1.Get the register number of the bus.
     * 2.Using the number get the busDetail from the table.
     * 3.Change the status of that bus.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void setBusStatus(HttpServletRequest request,
        HttpServletResponse response,String status)
        throws ServletException, IOException  {
        String registerNumber = request.getParameter(Constant.REGISTER_NUMBER);
        busDetailController = new BusDetailController();
        busDetailController.setBusStatus(registerNumber,status);
        response.sendRedirect(Constant.JSP_FILES
            + Constant.AGENT + Constant.OPERATIONS + Constant.JSP);
    }

    /**
     * Get a Bus Detail record using bus Stops and date.
     *
     * @param request
     *              - contains object of HttpServletRequest.
     *
     * @param response
     *              - contains object of HttpServletResponse.
     */
    public void displayBusDetailByBusStops(HttpServletRequest request, 
                                         HttpServletResponse response) 
                                         throws ServletException, IOException  {
        String fromPlace = request.getParameter(Constant.FROM_PLACE);
        String toPlace = request.getParameter(Constant.TO_PLACE);
        String onward = request.getParameter(Constant.ONWARD);
        busDetailController = new BusDetailController();
        Set<BusDetail> busDetails = busDetailController.getBusDetailByBusStops
                                         (fromPlace, toPlace, onward);
        request.setAttribute(Constant.BUS_DETAILS,busDetails);
        request.setAttribute(Constant.ONWARD,onward);
        RequestDispatcher requestDispatcher = 
                   request.getRequestDispatcher(Constant.JSP_FILES
                                          + Constant.SELECT_BUS + Constant.JSP);  
        requestDispatcher.forward(request, response);
    }

    /**
     * 1.Get the inputs from web page.
     * 2.Get the list of buses for given inputs.
     * 3.Display the list of buses.
     * @param request
     *              - contains object of HttpServletRequest.
     * @param response
     *              - contains object of HttpServletResponse.
     */
    public void displayAvailableSeats(HttpServletRequest request, 
                            HttpServletResponse response) 
                            throws ServletException, IOException  {
        String registrationNumber = request.getParameter
                                                     (Constant.REGISTER_NUMBER);
        String ticketPrice = request.getParameter(Constant.PRICE);
        String onward = request.getParameter(Constant.ONWARD);
        busDetailController = new BusDetailController();
        Set<Seat> seats = busDetailController.getAvailableSeats
                                                           (registrationNumber);
        if (null != seats) {
            request.setAttribute(Constant.SEATS, seats);
            request.setAttribute(Constant.REGISTER_NUMBER, registrationNumber);
            request.setAttribute(Constant.TICKET_PRICE, ticketPrice);
            request.setAttribute(Constant.ONWARD, onward);
            RequestDispatcher requestDispatcher = 
                           request.getRequestDispatcher(Constant.JSP_FILES
                                         + Constant.SELECT_SEAT + Constant.JSP);  
            requestDispatcher.forward(request, response);
        } else {
            response.sendRedirect(Constant.JSP_FILES
                                   + Constant.NO_AVAILABLE_SEAT + Constant.JSP);
        }
    }
}
