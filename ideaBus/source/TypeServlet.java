package com.ideas2it.ideabus.servlets;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ideas2it.ideabus.type.Type;
import com.ideas2it.ideabus.type.controller.TypeController;
import com.ideas2it.ideabus.util.Constant;

/**
 * 1.Get inputs from web page and store them into database
 * 2.Get data from database based on conditions
 *   and check whether given inputs exists or not.
 * 3.Display data on web page from database and
 *   get the modified inputs and update the record in database.
 * 4.Remove the selected type from type table.
 */
public class TypeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private HttpSession session;
    private RequestDispatcher requestDispatcher;
    private TypeController typeController;
    
    /**
     * Get the input from web page and add it into the type table,
     * then redirect to their respective pages.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void doPost(HttpServletRequest request ,HttpServletResponse response)  
        throws ServletException, IOException {
        response.setContentType(Constant.TEXT_HTML);
        typeController = new TypeController();
        String name = request.getParameter(Constant.TYPE);
        String message = typeController.addType(name);
        if (message.equals(Constant.ADDED)) {
            response.sendRedirect(Constant.JSP_FILES
                + Constant.AGENT +  Constant.OPERATIONS + Constant.JSP);
        } else {
            response.sendRedirect(Constant.JSP_FILES
                + Constant.ADD +  Constant.TYPE + Constant.JSP);
        }
    }    
        
    /**
     * Get the value of submit from web page and based on its value,
     * call respective functions like,
     * If it's value is
     * 2. view then view types.
     * 3. update then display types later update selected type.
     * 4. delete then delete selected type.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void doGet(HttpServletRequest request
                       , HttpServletResponse response)  
                       throws ServletException, IOException {
                           
        response.setContentType(Constant.TEXT_HTML);
        String submit = request.getParameter(Constant.SUBMIT);
        if (submit.equals(Constant.VIEW)) {
            displayTypes(request,response,Constant.VIEW);
        } else if (submit.equals(Constant.UPDATE_1)) {
            displayTypes(request,response,Constant.UPDATE_1);
        } else if (submit.equals(Constant.UPDATE_1 + " " + Constant.TYPE)) {
            updateType(request,response);
        }else if (submit.equals(Constant.DELETE_1)) {
            displayTypes(request,response,Constant.DELETE_1);
        } else if (submit.equals(Constant.DELETE_1 + " " + Constant.TYPE)) {
            deleteType(request,response);
        }
    }
    
    /**
     * 1.Get the list of the types from type table.
     * 2.Set the types into the attribute.
     * 3.Display them on web page.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     * @param fileName
     *              - contains a string indicating a name of the file.
     */
    public void displayTypes(HttpServletRequest request
                       , HttpServletResponse response, String fileName)  
                       throws ServletException, IOException { 
        typeController = new TypeController();
        List<String> types = typeController.getTypes();
        if (types.isEmpty() == true) {
            response.sendRedirect(Constant.ERROR + Constant.JSP);
        }
        session = request.getSession();
        session.setAttribute(Constant.TYPES, types);
        requestDispatcher = request.getRequestDispatcher
            ( Constant.JSP_FILES + fileName + Constant.TYPE_1 + Constant.JSP);
        requestDispatcher.forward(request, response);
        
    }
    
    /**
     * 1.Get the selected type from list and input.
     * 2.Update name of the type with input.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void updateType(HttpServletRequest request
                       , HttpServletResponse response)  
                       throws ServletException, IOException {
        response.setContentType(Constant.TEXT_HTML);
        String type = request.getParameter(Constant.TYPE);
        String input = request.getParameter(Constant.NAME);
        typeController = new TypeController();
        typeController.updateType(type,input);
        response.sendRedirect(Constant.JSP_FILES
                + Constant.AGENT +  Constant.OPERATIONS + Constant.JSP);
    }
        
    /**
     * 1.Get the selected type from list.
     * 2.Delete instance of the selected type.
     *
     * @param request
     *              - contains instance of HttpServletRequest.
     * @param response
     *              - contains instance of HttpServletResponse.
     */
    public void deleteType(HttpServletRequest request
                       , HttpServletResponse response)  
                       throws ServletException, IOException {
        response.setContentType(Constant.TEXT_HTML);
        String type = request.getParameter(Constant.TYPE);
        typeController = new TypeController();
        typeController.deleteType(type);
        response.sendRedirect(Constant.JSP_FILES
                + Constant.AGENT +  Constant.OPERATIONS + Constant.JSP);
    }
}
