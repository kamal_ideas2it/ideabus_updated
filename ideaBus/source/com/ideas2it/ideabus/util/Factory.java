package com.ideas2it.ideabus.util;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import org.apache.log4j.Logger;

import com.ideas2it.ideabus.util.Constant;

/**
 * Build session factory and provide instance of session.
 */
public final class Factory {
    private static final Logger logger = Logger.getLogger(Factory.class);
    private static final SessionFactory sessionFactory;

    private Factory() {
    }
    
    /**
     * Build a session factory for the configuration file
     * and handle it's exception.
     *
     * @return sessionFactory returns an instance of sessionFactory.
     */
    static {
        try{
            sessionFactory = new Configuration().configure
                (Constant.HIBERNATE_FILE).buildSessionFactory();
        } catch (Exception exception) {
            logger.error(Constant.ERROR_CODE_001,exception);
            throw new ExceptionInInitializerError(exception); 
        }
    }
    
    /**
     * Open a session using sessionFactory.
     *
     * @return session an instance of session.
     */
    public static Session getSession() {
        return sessionFactory.openSession();
    }
    
    /**
     * Close both session and sessionfactory.
     *
     * @param session an instance of session.
     */
    public static void closeSession(Session session) {
        if ( null != session ) {
            session.close();
        }
    }
}
