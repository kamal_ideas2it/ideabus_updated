package com.ideas2it.ideabus.util;

/**
 * Declare variables as constants and initialize them.
 *
 * @author Abi Showkath Ali and Kamal Batcha.
 */
public final class Constant {

    public static final String HIBERNATE_FILE = "hibernate/hibernate.cfg.xml";
    public static final String TEXT_HTML = "text/html";
    
    //strings for path.
    public static final String JSP_FILES = "jsp files/";
    public static final String OPERATIONS = "Operations";
    public static final String ERROR = "error";
    public static final String JSP = ".jsp";
    
    //strings for name of submit parameter.
    public static final String SUBMIT = "submit";
    public static final String LOGIN = "Login";
    public static final String BUTTON = "button";

    //strings for return statements.
    public static final String ADD = "add";
    public static final String ADDED = "Added";
    public static final String REGISTER = "Register";
    public static final String REGISTERED = "Registered";
    public static final String ALREADY = "Already";
    public static final String EMPTY = "empty";

    //strings for user and roles.
    public static final String USER_1 = "user";
    public static final String USER_2 = "User";
    public static final String USER = "user";
    public static final String ADMIN = "admin";
    public static final String CUSTOMER = "customer";
    public static final String AGENT = "agent";
    public static final String AGENT_1 = "Agent";
    
    //strings for input variable names.
    public static final String NAME = "name";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String ROLE = "role";
    public static final String ROLE_1 = "Role";
    public static final String GENDER = "gender";
    public static final String AGE = "age";
    public static final String DATE = "date";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String MAILID = "mailId";
    public static final String EMAILID = "emailId";
    public static final String PASSWORD = "password";
    public static final String STATUS = "status";
    public static final String ACTIVE = "active";
    public static final String INACTIVE = "inactive";
    public static final String ROLES = "roles";
    public static final String NUMBER_OF_PASSENGERS = "numberOfPassengers";
    public static final String JOURNEY_DATE = "journeyDate";
    public static final String SEAT_NAME = "seatName";
    public static final String CARD_NUMBER = "cardNumber";
    public static final String CARD_NAME = "cardName";
    public static final String TOTAL_AMOUNT = "totalAmount";
    public static final String SUCCESS = "success";
    public static final String SHOW = "show";
    public static final String CANCEL = "cancel";
    public static final String TICKETS = "tickets";
    public static final String NO_AVAILABLE_SEAT = "noAvailableSeat";
    public static final String SELECT_SEAT = "selectSeat";
    public static final String SHOW_BOOKED_TICKET = "showBookedTicket";
    public static final String TICKET_NUMBER = "ticketNumber";
    public static final String TICKET_PRICE = "ticketPrice";

    //strings for choice of operation.
    public static final String VIEW = "view";
    public static final String VIEW_STOPS = "viewStops";
    public static final String UPDATE_1 = "update";
    public static final String UPDATE_2 = "Update";
    public static final String UPDATE_3 = "UPDATE";
    public static final String DELETE_1 = "delete";
    public static final String DELETE_2 = "Delete";
    public static final String DELETE_3 = "DELETE";
    
    //strings for error message.
    public static final String ERROR_1 = "error";
    public static final String ERROR_CODE_001 = "Failed to create session factory";
    public static final String ERROR_CODE_002 = "Data for given input doesn't exists";
    
    //error messages for user related operations.
    public static final String ERROR_CODE_USER_001
        = "Failed to add user data into user table";
    public static final String ERROR_CODE_USER_002
        = "Failed to get user data into user table";
    public static final String ERROR_CODE_USER_003
        = "Failed to update user data into user table";
    public static final String ERROR_CODE_USER_004
        = "Failed to delete user data into user table";
    
    //error messages for role related operations.
    public static final String ERROR_CODE_ROLE_001
        = "Failed to add role data into role table";
    public static final String ERROR_CODE_ROLE_002
        = "Failed to get role data into role table";
    public static final String ERROR_CODE_ROLE_003
        = "Failed to update role data into role table";
    public static final String ERROR_CODE_ROLE_004
        = "Failed to delete role data into role table";
        
    //error messages for type related operations.
    public static final String ERROR_CODE_TYPE_001
        = "Failed to add type data into type table";
    public static final String ERROR_CODE_TYPE_002
        = "Failed to get type data into type table";
    public static final String ERROR_CODE_TYPE_003
        = "Failed to update type data into type table";
    public static final String ERROR_CODE_TYPE_004
        = "Failed to delete type data into type table";
        
    //error messages for busDetail related operations.
    public static final String ERROR_CODE_BUS_DETAIL_001
        = "Failed to add busDetail data into busDetail table";
    public static final String ERROR_CODE_BUS_DETAIL_002
        = "Failed to get busDetail data into busDetail table";
    public static final String ERROR_CODE_BUS_DETAIL_003
        = "Failed to update busDetail data into busDetail table";
    public static final String ERROR_CODE_BUS_DETAIL_004
        = "Failed to delete busDetail data into busDetail table";
    
    //error messages for route related operations.
    public static final String ERROR_CODE_ROUTE_001
        = "Failed to add route data into route table";
    public static final String ERROR_CODE_ROUTE_002
        = "Failed to get route data into route table";
    public static final String ERROR_CODE_ROUTE_003
        = "Failed to update route data into route table";
    public static final String ERROR_CODE_ROUTE_004
        = "Failed to delete route data into route table";
    
    //error messages for busStop related operations.
    public static final String ERROR_CODE_BUS_STOP_001
        = "Failed to add busStop data into busStop table";
    public static final String ERROR_CODE_BUS_STOP_002
        = "Failed to get busStop data into busStop table";
    public static final String ERROR_CODE_BUS_STOP_003
        = "Failed to update busStop data into busStop table";
    public static final String ERROR_CODE_BUS_STOP_004
        = "Failed to delete busStop data into busStop table";
    
    //error messages for organization related operations.
    public static final String ERROR_CODE_ORGANIZATION_001
        = "Failed to add organization data into organization table";
    public static final String ERROR_CODE_ORGANIZATION_002
        = "Failed to get organization data into organization table";
    public static final String ERROR_CODE_ORGANIZATION_003
        = "Failed to update organization data into organization table";
    public static final String ERROR_CODE_ORGANIZATION_004
        = "Failed to delete organization data into organization table";
    
    public static final String ACTIVATE = "activate";

    public static final String IN_ACTIVE = "inactive";

    public static final String BUS = "bus";
    
    public static final String BUS_DETAIL = "busDetail";
    
    public static final String BUS_DETAIL_1 = "BusDetail";
    
    public static final String BUS_DETAILS = "busDetails";

    public static final String BUS_STOP = "busStop";

    public static final String BUS_STOP_1 = "BusStop";
    
    public static final String BUS_STOPS = "busStops";
    
    public static final String BUS_TYPE = "busType";
    
    public static final String COUNT = "count";

    public static final String DEACTIVATE = "deactivate";
    
    public static final String FROM_PLACE = "fromPlace";
    
    public static final String NAME_1 = "Name";

    public static final String NEXT = "next";

    public static final String NEW = "new";

    public static final String ORGANIZATION = "organization";

    public static final String ORGANIZATIONS = "organizations";

    public static final String ORGANIZATION_1 = "Organization";

    public static final String PREVIOUS = "previous";
    
    public static final String PRICE = "price";
    
    public static final String REGISTRATION_NUMBER = "registrationNumber";
    
    public static final String REGISTRATION_NUMBERS = "registrationNumbers";

    public static final String REGISTER_NUMBER = "registerNumber";
    
    public static final String REGISTER_NUMBERS = "registerNumbers";
    
    public static final String REGISTER_NUMBERS_1 = "RegisterNumbers";
    
    public static final String ROUTE = "route";
    
    public static final String ROUTES = "routes";
    
    public static final String ROUTE_1 = "Route";
    
    public static final String STOP = "stop";
    
    public static final String STOPS = "stops";
    
    public static final String STOP_1 = "Stop";
    
    public static final String STOPS_1 = "Stops";
    
    public static final String SEARCH = "search";
    
    public static final String SEARCH_1 = "Search";
    
    public static final String SEARCH_PLACE = "searchPlace";

    public static final String SELECT = "select";

    public static final String SELECT_BUS = "selectBus";
    
    public static final String SEAT = "seat";
    
    public static final String SEATS = "seats";
    
    public static final String TO_PLACE = "toPlace";

    public static final String ONWARD = "onward";

    public static final String TYPE = "type";
    
    public static final String TYPES = "types";
    
    public static final String TYPE_1 = "Type";
    
    public static final String YES = "Yes";

    public static final int TICKET_SERIAL = 10101;
    
    public static int count;

    /**
     * Increase value of count by 5.
     *
     * @return count returns the count value.
     */
    public static int nextCount() {
        return count+=3;
    }
    
    /**
     * Decrease value of count by 5.
     *
     * @return count returns the count value.
     */
    public static int previousCount() {
        return count-=3;
    }
    
    /**
     * Initialize count value.
     *
     * @return count returns the count value.
     */
    public static int newCount() {
        return count = 0;
    }
}
