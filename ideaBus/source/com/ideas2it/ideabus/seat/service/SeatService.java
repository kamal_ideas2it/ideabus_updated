package com.ideas2it.ideabus.seat.service;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.seat.Seat;

/**
 * Manipulation operation of Seat.
 * By Adding, Removing, Getting, Updating.
 */
public interface SeatService {

    /**
     * Add the Seat detail. <br>
     * Details like seat Name.
     * @param name
     *         - name of the seat.
     */
    void addSeat(String name) throws IdeaBusException;

    /**
     * For Check the Seat detail available or not <br>
     * Details like seat Name.
     * @param name
     *         - name of the seat.
     * @return true
     *         - response to the user seat is available.
     * @return false
     *         - response to the user seat is unavailable.
     */
    boolean checkSeat(String name) throws IdeaBusException;

    /*
     * Get all seat detail.
     */
    List<Seat> getSeats() throws IdeaBusException;

    /*
     * Get seat detail by seat Name.
     * @param name
     *         - name of the seat.
     */
    Seat getSeat(String name) throws IdeaBusException;

    /*
     * Booked seat for update seat as InActive.
     * @param name
     *         - name of the seat.
     */
    Seat bookSeat(String name) throws IdeaBusException;
}
