package com.ideas2it.ideabus.seat.service.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.Iterator;
import java.util.List;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.seat.dao.impl.SeatDAOImpl;
import com.ideas2it.ideabus.seat.dao.SeatDAO;
import com.ideas2it.ideabus.seat.Seat;
import com.ideas2it.ideabus.seat.service.SeatService;
import com.ideas2it.ideabus.util.Constant;

/**
 * Manipulation operation of Seat.
 * By Adding, Removing, Getting, Updating.
 */
public class SeatServiceImpl implements SeatService {
    private SeatDAO seatDAO;

    /**
     * For Add the Seat detail to passing the variable to DAO. <br>
     * Details like seat Name.
     * @param name
     *         - name of the seat.
     */
    public void addSeat(String name) throws IdeaBusException {
        String status = Constant.ACTIVE;
        Seat seat = new Seat(name, status);
        seatDAO = new SeatDAOImpl();
        seatDAO.addSeat(seat);
    }

    /**
     * For Check the Seat detail available or not <br>
     * Details like seat Name.
     * @param name
     *         - name of the seat.
     * @return true
     *         - response to the user seat is available.
     * @return false
     *         - response to the user seat is unavailable.
     */
    public boolean checkSeat(String name) throws IdeaBusException {
        Seat seat = getSeat(name);
        if (null != seat) {
            return true;
        }            
        return false;
    }

    /*
     * Get all seat detail.
     * @return seats 
     *         - all seat detail.
     */
    public List<Seat> getSeats() throws IdeaBusException {
        seatDAO = new SeatDAOImpl();
        return seatDAO.getSeats();
    }

    /*
     * Get seat detail by seat Name.
     * @param name
     *         - name of the seat.
     * @return seat
     *         - all seat detail.
     */
    public Seat getSeat(String name) throws IdeaBusException {
        seatDAO = new SeatDAOImpl();
        return seatDAO.getSeat(name);
    }

    /*
     * Booked seat for update seat as InActive.
     * @param name
     *         - name of the seat.
     */
    public Seat bookSeat(String name) throws IdeaBusException {
        seatDAO = new SeatDAOImpl();
        Seat seat = getSeat(name);
        seat.setStatus(Constant.IN_ACTIVE);
        seatDAO.updateSeat(seat);
        return seat;
    }
}
