package com.ideas2it.ideabus.seat;
/**
 * @author Abi Showkath Ali and Kamal Batcha.
 *
 * Model class of Seat.
 */
public class Seat {
    private int id;
    private String name;
    private String status;

    public Seat() {
    }
    
    public Seat(String name, String status) {
        this.name = name;
        this.status = status;
    }
         
    /**
     * Initializing Getter and Setter
     */     
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
