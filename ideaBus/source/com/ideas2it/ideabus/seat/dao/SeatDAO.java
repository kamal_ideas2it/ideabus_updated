package com.ideas2it.ideabus.seat.dao;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.seat.Seat;

/**
 * Manipulation operation of Seat.
 * By Adding, Removing, Getting, Updating.
 */
public interface SeatDAO {

    /**
     * Add seat Detail.
     * @param seat.
     *             - seat detail like seat name.
     */
    void addSeat(Seat seat) throws IdeaBusException;

    /*
     * Get all seat detail.
     */
    List<Seat> getSeats() throws IdeaBusException;

    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get a single record of Seat from seat table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session
     * 5.Return seat data.
     *
     * @param seat.
     *             - seat detail.
     */
    Seat getSeat(String name) throws IdeaBusException;

    /**
     * Update seat Detail.
     * @param seat.
     *             - seat detail.
     */
    void updateSeat(Seat seat) throws IdeaBusException;
}
