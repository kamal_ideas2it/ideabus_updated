package com.ideas2it.ideabus.seat.dao.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Criteria; 
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException; 
import org.hibernate.Transaction;
import org.hibernate.Session; 

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.seat.dao.SeatDAO;
import com.ideas2it.ideabus.seat.Seat;
import com.ideas2it.ideabus.util.Constant;
import com.ideas2it.ideabus.util.Factory;

/**
 * Manipulation operation of Seat.
 * By Adding, Removing, Getting, Updating.
 * Using Linked List for fast manipulation.
 */
public class SeatDAOImpl implements SeatDAO {
    private Session session;
    private Transaction transaction;

    /**
     * Add seat Detail.
     * @param seat.
     *             - seat detail.
     */
    public void addSeat(Seat seat) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.save(seat);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Get All Seat detail from the Database and return to Seat Service.
     * @return seats.
     *             - All seat detail in the list.
     */
    public List<Seat> getSeats() throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        List<Seat> seats = new LinkedList<>();
        try {
            transaction = session.beginTransaction();
            seats = session.createQuery("FROM Seat").list();

            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return seats;
    }

    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get a single record of Seat from seat table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session
     * 5.Return seat data.
     *
     * @param seat.
     *             - seat detail.
     */
    public Seat getSeat(String name) throws IdeaBusException {
        Seat seat = new Seat();
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            Criteria criteria = session.createCriteria(Seat.class);
            criteria.add(Restrictions.eq("name",name));
            seat = (Seat)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return seat;
    }

    /**
     * Update seat Detail.
     * @param seat.
     *             - seat detail.
     */
    public void updateSeat(Seat seat) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.update(seat);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }   
}
