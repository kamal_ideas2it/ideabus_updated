package com.ideas2it.ideabus.user.controller;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import org.apache.log4j.Logger;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.user.User;
import com.ideas2it.ideabus.user.service.UserService;
import com.ideas2it.ideabus.user.service.impl.UserServiceImpl;
import com.ideas2it.ideabus.util.Constant;

/**
 * 1.Use to control the flow of data
 * 2.Handle the exceptions.
 */
public class UserController {
    private static Logger logger = Logger.getLogger(UserController.class);
    private UserService userService;
    
    /**
     * Add new user to user table and handle it's exception.
     *
     * @param firstName
     *              - contains first name of an user.
     * @param lastName
     *              - contains surname of an user.
     * @param gender
     *              - contains gender of an user.
     * @param age
     *              - contains age of an user.
     * @param phone
     *              - contains phone number of an user.
     * @param emailId
     *              - contains mailId of an user.
     * @param password
     *              - contains password of an user.
     * @param name
     *              - contains role of an user.
     *
     * @return a String containing a statement for display.
     */
    public String addUser(String firstName,String lastName, String gender,
        String age, String phone, String emailId, String password,String role)
        {
        userService = new UserServiceImpl();
        try {
            return userService.checkAndAddUser
            (firstName ,lastName ,gender ,age ,phone ,emailId ,password ,role);
        } catch (IdeaBusException e) {
            logger.error(e);
            return Constant.ERROR;
        }
    }
    
    /**
     * Check user exists in user table using their mailId
     * and handle it's exception.
     *
     * @param emailId
     *              - contains input(mailId).
     * @param password
     *              - contains input (password).
     * @param roleName
     *              - contains input (name of role).
     *
     */
    public boolean checkUser(String emailId, String password,
        String roleName) {
        userService = new UserServiceImpl();
        try {
            return userService.checkUser(emailId, password, roleName);
        } catch (IdeaBusException e) {
            logger.error(e);
            return false;
        }
    }
    
    /**
     * Check if the user for given mailId and role exists in table
     * and handle it's exception.
     *
     * @param emailId
     *              - contains emailId of an user.
     * @param role
     *              - contains name of the role.
     *
     * @return a boolean value.
     */
    public boolean isUser(String emailId ,String role) {
        userService = new UserServiceImpl();
        try {
            return userService.isUser(emailId,role);
        } catch (IdeaBusException e) {
            logger.error(e);
            return false;
        }
    }
    
    /**
     * Get the user from user table and handle it's exception.
     *
     * @param emailId
     *              - contains input(mailId).
     *
     * @return instance of user.
     */
    public User getUserByEmailId(String emailId) {
        userService = new UserServiceImpl();
        try {
            return userService.getUserByEmailId(emailId);
        } catch (IdeaBusException e) {
            logger.error(e);
            return null;
        }
    }
    
    /**
     * Update the user data for the given mailId in user table
     * and handle it's exception.
     *
     * @param firstName
     *              - contains first name of an user.
     * @param lastName
     *              - contains surname of an user.
     * @param gender
     *              - contains gender of an user.
     * @param age
     *              - contains age of an user.
     * @param phone
     *              - contains phone number of an user.
     * @param emailId
     *              - contains mailId of an user.
     * @param password
     *              - contains password of an user.
     * @param status
     *              - contains status of an user.
     */
    public void updateUser(String mailId,String firstName,String lastName,
        String gender, String age, String phone, String emailId, String password,
        String status) {
        userService = new UserServiceImpl();
        try {
            userService.updateUser(mailId, firstName, lastName, gender, age,
            phone, emailId, password, status);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
    }
    
    /**
     * Disable the user for the given mailId in the user table 
     * and handle it's exception.
     *
     * @param emailId
     *              - contains emailId of user.
     */
    public void deleteUser(String emailId){
        userService = new UserServiceImpl();
        try {
            userService.deleteUser(emailId);
        }  catch (IdeaBusException e) {
            logger.error(e);
        }
    }
}
