package com.ideas2it.ideabus.user.dao.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria; 
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions; 

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.user.User;
import com.ideas2it.ideabus.user.dao.UserDAO;
import com.ideas2it.ideabus.util.Constant;
import com.ideas2it.ideabus.util.Factory;

/**
 * Implements UserDAO and perform manipulation operations on input data.<br/>
 * 1.Add user into user table.<br/>
 * 2.Get list of users from user table and return them.<br/>
 * 3.Get single record of an user from user table and return them.<br/>
 * 4.Update existing user data in user table.<br/>
 */
public class UserDAOImpl implements UserDAO {
    private Session session;
    private Transaction transaction;

    /**
     * Adds the instance of User presistance class into the user table.<br/>
     * Get instance of Session from Factory and begin transaction for that session.<br/>
     * save user to user table and commit.<br/>
     * Handle the exception in transaction using rollback.<br/>
     * Finally close the session.<br/>
     *
     * @param user
     *          - contains instance of User class.
     * 
     */
    public void addUser(User user) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.save(user);
            transaction.commit();
        } catch (HibernateException e) {
            if ( null != transaction ) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_USER_001 + e);
        } finally {
            Factory.closeSession(session);
        }
    }

   /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get list of users from the Database and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session.
     * 5.Return list of users.
     *
     * @return contains list of users.
     */
    public List<User> getUsers() throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        List<User> users = new ArrayList<>();
        try {
            Criteria criteria = session.createCriteria(User.class);
            users = criteria.list();
            transaction.commit();
        } catch (HibernateException e) {
            if ( null != transaction ) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_USER_002 + e);
        } finally {
            Factory.closeSession(session);
        }
        return users;
    }

    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get a single record of user from user table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session
     * 5.Return user data.
     *
     * @param user.
     *             - contains details of user.
     */
    public User getUserByEmailId(String emailId) throws IdeaBusException {
        User user = new User();
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.eq(Constant.EMAILID,emailId));
            user = (User)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException e) {
            if ( null != transaction ) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_USER_002 + e);
        } finally {
            Factory.closeSession(session);
        }
        return user;
    }
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Update a user data in user table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session
     *
     * @param user
     *          - contains instance of user.
     */
    public void updateUser(User user) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.update(user);
            transaction.commit();
        } catch (HibernateException e) {
            if ( null != transaction ) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_USER_003 + e);
        } finally {
            Factory.closeSession(session);
        }
    }
}
