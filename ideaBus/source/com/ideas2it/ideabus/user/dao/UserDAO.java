package com.ideas2it.ideabus.user.dao;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.user.User;

/**
 * Perform manipulation operations on input and stored data.
 * 1.Add instance of User class int user table.
 * 2.Get list of users from user table and return them.
 * 3.Get single record of an user from user table and return them.
 * 4.Update existing user data in user table.
 */
public interface UserDAO {

    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Add user to user table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session
     *
     * @param user
     *          - contains instance of User class.
     */
    void addUser(User user) throws IdeaBusException;

   /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get list of users from the Database and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session.
     * 5.Return list of users.
     *
     * @return contains list of users.
     */
    List<User> getUsers() throws IdeaBusException;
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get a single record of user from user table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session
     * 5.Return user data.
     *
     * @param user.
     *             - contains details of user.
     */
    User getUserByEmailId(String emailId) throws IdeaBusException;

    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Update a user data in user table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session
     *
     * @param user
     *          - contains instance of user.
     */
    void updateUser(User user) throws IdeaBusException;
}
