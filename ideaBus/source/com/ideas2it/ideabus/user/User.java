package com.ideas2it.ideabus.user;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.HashSet;
import java.util.Set;

import com.ideas2it.ideabus.role.Role;

/**
 * Declare variables for user related functions and encapsulate them.
 */
public class User {
    private int id;
    private String firstName;
    private String lastName;
    private String gender;
    private int age;
    private String phone;
    private String emailId;
    private String password;
    private int date;
    private String status;
    private Set<Role> roles = new HashSet<>(0);
    
    public User() {}

    public User(String firstName,String lastName, String gender, int age, 
                String phone, String emailId, String password, String status
                , Set<Role> roles) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;
        this.emailId = emailId;
        this.phone = phone;
        this.password = password;
        this.status = status;
        this.roles = roles;
    }
    
    /**
     * Getter and Setter
     */
    public void setId(int id) {
        this.id = id;
    } 

    public int getId() {
        return id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public int getDate() {
        return date;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
    
    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Role> getRoles() {
        return roles;
    }
}
