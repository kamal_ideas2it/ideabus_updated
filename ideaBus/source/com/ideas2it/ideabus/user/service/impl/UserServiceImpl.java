package com.ideas2it.ideabus.user.service.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.role.Role;
import com.ideas2it.ideabus.role.service.RoleService;
import com.ideas2it.ideabus.role.service.impl.RoleServiceImpl;
import com.ideas2it.ideabus.user.User;
import com.ideas2it.ideabus.user.dao.UserDAO;
import com.ideas2it.ideabus.user.dao.impl.UserDAOImpl;
import com.ideas2it.ideabus.user.service.UserService;
import com.ideas2it.ideabus.util.Constant;

/**
 * Perform logical operations on data like,
 * 1.Check if user exists in user table then add new data into the table.
 * 2.Check user's role and add new role into them.
 * 3.Check the user in user table for given mailId and role.
 * 4.Update user data with given inputs.
 * 5.Change the staus of user to disable it.
 * 6.Get the list of users from user table.
 */
public class UserServiceImpl implements UserService {
    private static final Logger logger = Logger.getLogger
        (UserServiceImpl.class);
    private UserDAO userDAO;
    private RoleService roleService;

    /**
     * 1.Check whether user exists.
     * 2.If not get the role for the given name from role table.
     * 3.Create a new instance of user and set their values.
     * 4.Add the user into user table.
     *
     * @param firstName
     *              - contains first name of an user.
     * @param lastName
     *              - contains surname of an user.
     * @param gender
     *              - contains gender of an user.
     * @param age
     *              - contains age of an user.
     * @param phone
     *              - contains phone number of an user.
     * @param emailId
     *              - contains mailId of an user.
     * @param password
     *              - contains password of an user.
     * @param name
     *              - contains role of an user.
     *
     * @return a String containing a statement.
     */
    public String checkAndAddUser(String firstName ,String lastName ,
        String gender ,String age , String phone , String emailId ,
        String password ,String name) throws IdeaBusException {
        if ( "" == emailId) {
            return Constant.EMPTY;
        }
        boolean dataExists = checkUserTable(emailId);
        if (dataExists == true) {
            return checkUserAndRole(emailId ,name);
        } else {
            roleService = new RoleServiceImpl();
            Set<Role> userRoles = new HashSet<>();
            Role role = roleService.getRole(name);
            if ( null != role) {
                userRoles.add(role);
                User user = new User(firstName ,lastName ,gender ,getNumber(age) ,phone ,
                    emailId ,password ,Constant.ACTIVE ,userRoles);
                userDAO = new UserDAOImpl();
                userDAO.addUser(user);
            }
            return Constant.REGISTERED;
        }
    }
    
    /**
     * Convert text into a number.
     * 
     * @param input
     *           - contains input as String.
     *
     * @return the output as integer.
     */
    public int getNumber(String input) throws NumberFormatException {
        return Integer.parseInt(input);
    }
    
    /**
     * Check user exist in user from given mailId and role table,
     * 1.If user exists then return an indication that it already exists.
     * 2.If user for given role doesn't exists then add that role to the user.
     * 3.Return a statement for above functions.
     *
     * @param emailId
     *              - contains input mailId.
     * @param name
     *              - contains name of role.
     */
    public String checkUserAndRole(String emailId ,String role)
        throws IdeaBusException {
        boolean dataExists = checkUserRole(emailId,role);
        if (true == dataExists) {
            return (Constant.ALREADY + " " + Constant.REGISTERED);
        } else {
            User user = getUserByEmailId(emailId);
            addRole(user,role);
            return Constant.REGISTERED;
        }
    }
    
    /**
     * Check user exists in user table or not,
     * 1.Get the user for given mailId from user table.
     * 2.If it's status is inactive then change it to active.
     * 3.Then update the user in user table.
     * 4.Return a boolean value for above functions.
     *
     * @param emailId
     *              - contains emailId of user.
     *
     * @return a boolean value based on conditions.
     */
    public boolean checkUserTable(String emailId) throws IdeaBusException {
        User user = getUserByEmailId(emailId);
        if ( null != user ) {
            if (user.getStatus().equals(Constant.ACTIVE)) {
                return true;
            } else {
                userDAO = new UserDAOImpl();
                user.setStatus(Constant.ACTIVE);
                userDAO.updateUser(user);
                return true;
            }
        }
        return false;
    } 
     
    /**
     * Add new role to user,
     * 1.Get instance of role from role table for the given name.
     * 2.If the role exists then add it to user's role.
     * 3.Update the user in user table.
     *
     * @param user
     *          - contains instance of user.
     * @param name
     *          - contains name of role.
     */
    public void addRole(User user,String name) throws IdeaBusException {
        roleService = new RoleServiceImpl();
        Set<Role> userRoles = new HashSet<>();
        Role role = roleService.getRole(name);
        if ( null != role ) {
            userRoles = user.getRoles();
            userRoles.add(role);
            user.setRoles(userRoles);
            userDAO = new UserDAOImpl();
            userDAO.updateUser(user);
        }
    }
    
    /**
     * Check whether user is exists in user table,
     * 1.Check the user table for a user with given emailId and role exists.
     * 2.If the user exists check password matches the input password.
     * 3.And check that user is active or not.
     * 4.Return a boolean value for above functions.
     *
     * @param emailId
     *              - contains input(mailId).
     * @param password
     *              - contains input (password).
     * @param roleName
     *              - contains input (name of role).
     */
    public boolean checkUser(String emailId, String password,
        String roleName) throws IdeaBusException {
        boolean isUser = checkUserRole(emailId ,roleName);
        if (isUser == true) {
            User user = getUserByEmailId(emailId);
            if ( null == user.getEmailId() ){
                return false;
            } else {
                String status = user.getStatus();
                String passWord = user.getPassword();        
                if(passWord.equals(password) && status.equals(Constant.ACTIVE)){
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }
  
    /**
     * 1.Get the user from the user tables using it's existing mailId.
     * 2.set the user data with inputs.
     * 3.Update the user in user table.
     *
     * @param firstName
     *              - contains first name of an user.
     * @param lastName
     *              - contains surname of an user.
     * @param gender
     *              - contains gender of an user.
     * @param age
     *              - contains age of an user.
     * @param phone
     *              - contains phone number of an user.
     * @param emailId
     *              - contains mailId of an user.
     * @param password
     *              - contains password of an user.
     * @param status
     *              - contains status of an user.
     */
    public void updateUser(String mailId,String firstName,String lastName ,
        String gender, String age, String phone, String emailId, String password,
        String status) throws IdeaBusException {
        userDAO = new UserDAOImpl();
        User user = getUserByEmailId(mailId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setGender(gender);
        user.setAge(getNumber(age));
        user.setPhone(phone);
        user.setEmailId(emailId);
        user.setPassword(password);
        user.setStatus(status);
        userDAO.updateUser(user);
    }
   
    /**
     * Check if the user for mailId and role.
     *
     * @param emailId
     *              - contains emailId of an user.
     *
     * @ return a boolean value.
     */
    public boolean isUser(String emailId ,String role)
         throws IdeaBusException {
        return checkUserRole(emailId ,role);
    }
     
    /**
     * 1.Get the details of a single user using emailId from user table.
     * 2.Change it's status and update it's record in user table.
     *
     * @param emailId
     *              - contains mailId of an user.
     */
    public void deleteUser(String emailId) throws IdeaBusException {
        userDAO = new UserDAOImpl();
        User user = getUserByEmailId(emailId);
        user.setStatus(Constant.INACTIVE);
        userDAO.updateUser(user);
    }
    
    /**
     * Check role exists in user or not.
     * 1.Get user for the emailId from user table.
     * 2.Get the set of roles from that user.
     * 3.Iterate those set of roles into instances.
     * 4.Check the name in that instance with input.
     * 5 return boolean value for above function.
     *
     * @param emailID
     *          - contains emailId of user.
     * @param name
     *          - contains name of the role.
     *
     * @return a boolean value based on conditions.
     */
    public boolean checkUserRole(String emailId,String name)
        throws IdeaBusException {
        Role role = new Role();
        User user = getUserByEmailId(emailId);
        Set<Role> roles = null;
        try {
            roles = user.getRoles();
        } catch (NullPointerException e) {
            logger.error(e);
            throw new IdeaBusException(Constant.ERROR_CODE_002);
        }
        Iterator<Role> iterator = roles.iterator();
        while (iterator.hasNext()) {
            role = iterator.next();
            if(name.equals(role.getName())) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Get a user from user table using the mailId.
     *
     * @param emailId
     *              - contains emailId of user.
     */
    public User getUserByEmailId(String emailId) throws IdeaBusException {
        userDAO = new UserDAOImpl(); 
        return userDAO.getUserByEmailId(emailId);
    }
            
    /**
     * Get the list of users from user table.
     *
     * @return a list of user .
     */
    public List<User> getUsers() throws IdeaBusException {
       userDAO = new UserDAOImpl();
       return userDAO.getUsers();
    }
}
