package com.ideas2it.ideabus.busDetail.dao.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;

import org.hibernate.Criteria; 
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Restrictions;

import com.ideas2it.ideabus.busDetail.BusDetail;
import com.ideas2it.ideabus.busDetail.dao.BusDetailDAO;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.route.Route;
import com.ideas2it.ideabus.user.User;
import com.ideas2it.ideabus.util.Constant;
import com.ideas2it.ideabus.util.Factory;

/**
 * Perform manipulation operations on input data.
 * 1.Add busDetail into busDetail table.
 * 2.Get list of busDetails from busDetail table and return them.
 * 3.Get single record of an busDetail from busDetail table and return them.
 * 4.Update existing busDetail data in busDetail table.
 */
public class BusDetailDAOImpl implements BusDetailDAO {
    private BusDetail busDetail;
    private Session session;
    private Transaction transaction;
    
    /**
     * Add Bus Detail.
     * @param busDetail.
     *             - bus detail.
     */
    public void addBusDetail(BusDetail busDetail) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.save(busDetail);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Get all Bus detail.
     * @return busDetails 
     *         - all bus detail.
     */
    public BusDetail getBusDetail(Route route) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        busDetail = new BusDetail();
        try {
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(BusDetail.class);
            criteria.add(Restrictions.eq("route",route));
            busDetail = (BusDetail)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return busDetail;
    }

    /**
     * Get Bus detail by registration number.
     * @return busDetail
     *         - bus detail.
     */
    public BusDetail getBus(String registerNumber) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        busDetail = new BusDetail();
        try {
            Criteria criteria = session.createCriteria(BusDetail.class);
            criteria.add(Restrictions.eq
                (Constant.REGISTRATION_NUMBER,registerNumber));
            busDetail = (BusDetail)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return busDetail;
    }
    
    /**
     * Get Bus detail by the user.
     * @return busDetail
     *         - bus detail.
     */
    public List<BusDetail> getBuses(User user) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        List<BusDetail> buses = new ArrayList<>();
        try {
            Criteria criteria = session.createCriteria(BusDetail.class);
            criteria.add(Restrictions.eq(Constant.USER_1,user));
            buses = criteria.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return buses;
    }
    
        
    /**
     * Get list of register numbers from busDetails table.
     *
     * @param mailId
     *              - contains  mailId of user.
     *
     * @return a list of registerNumbers.
     */
    public List<String> getRegisterNumbers(User user) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        List<String> registerNumbers = new ArrayList<>();
        try {
            Criteria criteria = session.createCriteria(BusDetail.class);
            criteria.add(Restrictions.eq(Constant.USER_1,user));
            ProjectionList projectionList = Projections.projectionList();
            projectionList.add(Projections.property
                (Constant.REGISTRATION_NUMBER));
            criteria.setProjection(projectionList);
            registerNumbers = criteria.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return registerNumbers;
    }
    
    /**
     * Update Bus Detail.
     * @param busDetail.
     *             - bus detail.
     */
    public void updateBusDetail(BusDetail busDetail) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.update(busDetail);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /*
     * Get Bus detail by the instance route and date.
     * @param route.
     *             - route detail.
     * @param onward.
     *             - journey date of the bus.
     * @return list of buses.
     */
    public List<BusDetail> getBusDetailByRoute(Route route, String onward) 
                                                     throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        List<BusDetail> busDetails = new LinkedList<>();
        try {
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(BusDetail.class);
            criteria.add(Restrictions.eq("route", route));
            criteria.add(Restrictions.eq("date", onward));
            busDetails = criteria.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return busDetails;
    }
}
