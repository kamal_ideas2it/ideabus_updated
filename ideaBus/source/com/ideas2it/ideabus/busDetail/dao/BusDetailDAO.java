package com.ideas2it.ideabus.busDetail.dao;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;

import com.ideas2it.ideabus.busDetail.BusDetail;
import com.ideas2it.ideabus.route.Route;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.user.User;

/**
 * Manipulation operation of Bus Detail.
 * By Adding, Removing, Getting, Updating.
 */
public interface BusDetailDAO {

    /**
     * Add Bus Detail.
     * @param busDetail.
     *             - bus detail.
     */
    void addBusDetail(BusDetail busDetail) throws IdeaBusException ;

    /**
     * Get all Bus detail.
     * @return busDetails 
     *         - all bus detail.
     */
    BusDetail getBusDetail(Route route) throws IdeaBusException ;

    /**
     * Get Bus detail by registration number.
     * @return busDetail 
     *         - bus detail.
     */
    BusDetail getBus(String registerNumber) throws IdeaBusException ;
    
    /**
     * Get the list of user's buses.
     *
     * @param user
     *          - contains instance of User.
     *
     * @return a list of user's buses.
     */
    List<BusDetail> getBuses (User user) throws IdeaBusException ;
    
    /**
     * Get list of register numbers from busDetails table.
     *
     * @param mailId
     *              - contains  mailId of user.
     *
     * @return a list of registerNumbers.
     */
    List<String> getRegisterNumbers(User user) throws IdeaBusException ;
    
    /**
     * Update Bus Detail.
     * @param busDetail.
     *             - bus detail.
     */
    void updateBusDetail(BusDetail busDetail) throws IdeaBusException ;

    /*
     * Get all Bus detail.
     * @return busDetails 
     *         - all bus detail.
     */
    List<BusDetail> getBusDetailByRoute(Route route, String onward) 
        throws IdeaBusException;
}
