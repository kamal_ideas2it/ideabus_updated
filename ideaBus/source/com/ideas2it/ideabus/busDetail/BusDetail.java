package com.ideas2it.ideabus.busDetail;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.HashSet;
import java.util.Set;

import com.ideas2it.ideabus.organization.Organization;
import com.ideas2it.ideabus.route.Route;
import com.ideas2it.ideabus.seat.Seat;
import com.ideas2it.ideabus.type.Type;
import com.ideas2it.ideabus.user.User;

/**
 * Declare variables for busDetail related functions and encapsulate them.
 */
public class BusDetail {
    private int id;
    private int totalSeats;
    private int availableSeats;
    private float price;
    private String registrationNumber;
    private String date;
    private String status;
    private Organization organization;
    private Route route;
    private User user;
    private Set<Type> types = new HashSet<>(0);
    private Set<Seat> seats = new HashSet<>(0);
    
    public BusDetail() {
    }
    
    public BusDetail(String registrationNumber, int totalSeats ,float price, 
        String status, String date, Organization organization, 
        Route route, User user, Set<Type> types, Set<Seat> seats) {
        this.registrationNumber = registrationNumber;
        this.totalSeats = totalSeats;
        this.price = price;
        this.status = status;
        this.date = date;
        this.organization = organization;
        this.route = route;
        this.user = user;
        this.types = types;
        this.seats = seats;
    }
       
    /**
     * Initializing Getter and Setter
     */ 
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setTotalSeats(int totalSeats) {
        this.totalSeats = totalSeats;
    }

    public int getTotalSeats() {
        return totalSeats;
    }

    public void setAvailableSeats(int availableSeats) {
        this.availableSeats = availableSeats;
    }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getPrice() {
        return price;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public Route getRoute() {
        return route;
    }

    public void setSeats(Set<Seat> seats) {
        this.seats = seats;
    }

    public Set<Seat> getSeats() {
        return seats;
    }

    public void setTypes(Set<Type> types) {
        this.types = types;
    }

    public Set<Type> getTypes() {
        return types;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
