package com.ideas2it.ideabus.busDetail.service.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.lang.NullPointerException; 
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import com.ideas2it.ideabus.busDetail.BusDetail;
import com.ideas2it.ideabus.busDetail.dao.BusDetailDAO;
import com.ideas2it.ideabus.busDetail.dao.impl.BusDetailDAOImpl;
import com.ideas2it.ideabus.busDetail.service.BusDetailService;
import com.ideas2it.ideabus.busStop.BusStop;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.organization.Organization;
import com.ideas2it.ideabus.organization.service.OrganizationService;
import com.ideas2it.ideabus.organization.service.impl.OrganizationServiceImpl;
import com.ideas2it.ideabus.route.Route;
import com.ideas2it.ideabus.route.service.RouteService;
import com.ideas2it.ideabus.route.service.impl.RouteServiceImpl;
import com.ideas2it.ideabus.seat.Seat;
import com.ideas2it.ideabus.seat.service.SeatService;
import com.ideas2it.ideabus.seat.service.impl.SeatServiceImpl;
import com.ideas2it.ideabus.type.Type;
import com.ideas2it.ideabus.type.service.TypeService;
import com.ideas2it.ideabus.type.service.impl.TypeServiceImpl;
import com.ideas2it.ideabus.user.User;
import com.ideas2it.ideabus.user.service.UserService;
import com.ideas2it.ideabus.user.service.impl.UserServiceImpl;
import com.ideas2it.ideabus.util.Constant;

/**
 * Manipulation operation of Bus Details.
 * By Adding, Removing, Getting, Updating.
 */
public class BusDetailServiceImpl implements BusDetailService {
    private BusDetailDAO busDetailDAO;
    private OrganizationService organizationService;
    private RouteService routeService;
    private SeatService seatService;
    private TypeService typeService;
    private UserService userService;
    
    /**
     * For Add the Bus detail to passing the variable to DAO. <br>
     * Details like busName, registerNumber, busStopName, type, seatName, price.
     * @param busName
     *         - name of the bus.
     * @param registerNumber
     *         - register number of the bus.
     * @param routeName
     *         - route of the bus.
     * @param busStopName
     *         - bus stops for respective bus.
     * @param type
     *         - type of features in bus.
     * @param seat
     *         - seat name of respective bus.
     * @param price
     *         - ticket price for respective bus.
     */
    public String addBusDetail(String busName, String registerNumber,
        String routeName, String type, String seat, float price,
        String emailId, String date) throws IdeaBusException {
        boolean busDetailExist = checkBusDetail(registerNumber);
        if (true == busDetailExist) {
            return (Constant.ALREADY + " " + Constant.REGISTERED);
        } else {
            Set<Type> types = getTypes(type);  // get types of features
            Set<Seat> seats = getSeats(seat);
            int totalSeats = seats.size();
            Organization organization = getOrganization(busName);
            Route route = getRoute(routeName);
            userService = new UserServiceImpl();
            User user = userService.getUserByEmailId(emailId);
            BusDetail busDetail = new BusDetail(registerNumber, totalSeats , 
                                                   price, Constant.ACTIVE, date,
                                                   organization, route, user, 
                                                   types, seats);
            busDetailDAO = new BusDetailDAOImpl();
            busDetailDAO.addBusDetail(busDetail);
        } 
        return Constant.REGISTERED;
    }

    /**
     * Get the individual type value by splitting the string by commas.
     * @param type
     *         - type of features in bus.
     * @return types 
     *         - List of types of a bus.
     */
    public Set<Type> getTypes(String typeName) throws IdeaBusException {
        Type type;
        Set<Type> types = new HashSet<>();
        typeService = new TypeServiceImpl();
        boolean typeExists = false;
        for (String name : typeName.split(",")) {
            typeExists = typeService.checkType(name);
            if (true == typeExists) {
                type = typeService.getType(name);
                types.add(type);
            } else {
                types.add(new Type(name)); // add type
            }
        }
        return types;
    }

    /**
     * Get the individual seat value by splitting the string by commas.
     * @param seatName
     *         - Name of the seat.
     * @return seats 
     *         - List of seats in a bus.
     */
    public Set<Seat> getSeats(String seatName) throws IdeaBusException {
        String newSeat = new String(seatName);
        Set<Seat> seats = new HashSet<>();
        Seat seat;
        SeatService seatService;
        for (String name : newSeat.split(",")) {
            seatService = new SeatServiceImpl();
            boolean seatExists = seatService.checkSeat(name);
            if (seatExists == true) {
                seat = seatService.getSeat(name);
                seats.add(seat);
            } else {
                // add seat name and status
                seats.add(new Seat(name, Constant.ACTIVE));
            }
        }
        return seats;
    }

    /**
     * Get the organization name by checking the list for the organization
     * is already exist or not.
     *
     * @param busName
     *         - Name of the organization.
     * @return organization 
     *         - Detail of the organization.
     */
    public Organization getOrganization(String name) throws IdeaBusException {
        Organization organization;
        organizationService = new OrganizationServiceImpl();
        boolean organizationExist 
                         = organizationService.checkOrganization(name);
        if (true == organizationExist) {
            organization = organizationService.getOrganization(name);
        } else {
            organization = new Organization(name); // add organization
        }
        return organization;
    }
    
    /**
     * Get route for the given table from route table.
     *
     * @param routeName
     *              - contains the name of a route.
     *
     * @return the instance of route.
     */
    public Route getRoute(String routeName) throws IdeaBusException {
        routeService = new RouteServiceImpl();
        return routeService.getRouteByName(routeName);
    }
    
    /**
     * Get list of user's buses.
     *
     * @param mailId
     *              - contains  mailId of user.
     *
     * @return a list of buses.
     */
    public Set<BusDetail> getBusesByEmailId(String mailId) 
                                                throws IdeaBusException {
        userService = new UserServiceImpl();
        User user = userService.getUserByEmailId(mailId);
        busDetailDAO = new BusDetailDAOImpl();
        Set<BusDetail> busDetails = new HashSet<>(busDetailDAO.getBuses(user));
        return busDetails;
    }
    
    /**
     * Get Bus detail By registration Number.
     * @return BusDetil 
     *         - Bus detail.
     */
    public BusDetail getBusDetail(String registerNumber)
        throws IdeaBusException {
        busDetailDAO = new BusDetailDAOImpl();
        return busDetailDAO.getBus(registerNumber);
    }
        
    /**
     * Get list of register numbers from busDetails table.
     *
     * @param mailId
     *              - contains  mailId of user.
     *
     * @return a list of registerNumbers.
     */
    public List<String> getRegisterNumbers(String mailId) 
        throws IdeaBusException {
        userService = new UserServiceImpl();
        busDetailDAO = new BusDetailDAOImpl();
        User user = userService.getUserByEmailId(mailId);
        return busDetailDAO.getRegisterNumbers(user);
    }

    /**
     * Get available seat detail in selected bus.
     * @param registrationNumber
     *         - registrationNumber of the selected bus.
     * @return seats 
     *         - seat detail.
     */
    public Set<Seat> getAvailableSeats(String regNumber) 
         throws IdeaBusException {
        busDetailDAO = new BusDetailDAOImpl();
        BusDetail busDetail = busDetailDAO.getBus(regNumber);
        Set<Seat> seats = busDetail.getSeats();
        Set<Seat> availableSeats = new HashSet<>();
        Iterator<Seat> iterator = seats.iterator();
        Seat seat;
        while (iterator.hasNext()) {
            seat = iterator.next();
            if (seat.getStatus().equals(Constant.ACTIVE)) {
                availableSeats.add(seat);
            }
        }
        return availableSeats;
    }

    /**
     * Check if Bus detail exists in bus detail table.
     * @param regNumber
     *              - contains registration number of the bus.
     */
    public boolean checkBusDetail(String registerNumber)
         throws IdeaBusException {
        BusDetail busDetail = getBusDetail(registerNumber);
        if (null == busDetail) {
            return false;
        }
        return true;
    }
            
    /**
     * 1.Get the busDetail from the busDetail tables
     *   using it's existing register number.
     * 2.set new inputs into the busDetail.
     * 3.Update the busDetail in busDetail table.
     *
     *
     * @param organization
     *         - name of the travels.
     * @param routeName
     *         - name of the route.
     * @param types
     *         - different type of features in bus.
     * @param price
     *         - ticket price for respective seat.
     * @param registerNumber
     *         - new register number for the bus.
     * @param oldRegisterNumber
     *         - already registered register number of the bus.
     */
    public void updateBusDetail(String organizationName, String routeName,
        String types, float price, String registerNumber,
        String oldRegisterNumber, String date) throws IdeaBusException {
        busDetailDAO = new BusDetailDAOImpl();
        BusDetail busDetail = getBusDetail(oldRegisterNumber);
        busDetail.setOrganization(getOrganization(organizationName));
        busDetail.setRoute(getRoute(routeName));
        busDetail.setTypes(getTypes(types));
        busDetail.setPrice(price);
        busDetail.setRegistrationNumber(registerNumber);
        busDetail.setDate(date);
        busDetailDAO.updateBusDetail(busDetail);
    }
    
    /**
     * 1.Get the busDetail from the busDetail database
     *  using the given register number.
     * 2.Set the status of the bus.
     * 3.Update the busDetail into busDetail table.
     *
     * @param registerNumber
     *                  - contains the register number of the bus.
     * @param status
     *                  - contains the status for a bus.
     */
    public void setBusStatus(String registerNumber,String status)
        throws IdeaBusException {
        busDetailDAO = new BusDetailDAOImpl();
        BusDetail busDetail = getBusDetail(registerNumber);
        busDetail.setStatus(status);
        busDetailDAO.updateBusDetail(busDetail);
    }

    /*
     * Get list of user's buses.
     *
     * @param fromPlace
     *         - passenger arrival bus stop.
     * @param toPlace
     *         - Passenger departure bus stop.
     * @return a list of buses.
     */
    public Set<BusDetail> getBusDetailByBusStops(String fromPlace, 
        String toPlace, String onward) throws IdeaBusException {
        RouteService routeService = new RouteServiceImpl();
        Set<Route> routes = routeService.getRoutes(fromPlace, toPlace);
        busDetailDAO = new BusDetailDAOImpl();
        Set<BusDetail> busDetails = new HashSet<>(); 
        Iterator<Route> iterator = routes.iterator();
        Route route = new Route();
        List<BusDetail> newBusDetails;
        while (iterator.hasNext()) {
            route = iterator.next();
            newBusDetails = busDetailDAO.getBusDetailByRoute(route, onward);
            busDetails.addAll(newBusDetails);
        }
        return busDetails;
    }
}
