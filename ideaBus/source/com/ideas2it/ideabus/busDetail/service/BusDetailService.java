package com.ideas2it.ideabus.busDetail.service;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;
import java.util.Set;

import com.ideas2it.ideabus.busDetail.BusDetail;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.organization.Organization;
import com.ideas2it.ideabus.route.Route;
import com.ideas2it.ideabus.seat.Seat;
import com.ideas2it.ideabus.type.Type;

/**
 * Manipulation operation of Bus Details.
 * By Adding, Removing, Getting, Updating.
 */
public interface BusDetailService {

    /**
     * For Add the Bus detail to passing the variable service implement.
     * Details like busName, registerNumber, busStopName, type, seatName, price.
     *
     * @param busName
     *         - name of the bus.
     * @param registerNumber
     *         - register number of the bus.
     * @param routeName
     *         - route of the bus.
     * @param type
     *         - type of features in bus.
     * @param seatName
     *         - seat name of respective bus.
     * @param price
     *         - ticket price for respective bus.
     */
    String addBusDetail(String busName, String registerNumber, String routeName, 
        String type, String seatName, float price, String emailId, String date)
        throws IdeaBusException;

    /**
     * Get the individual type value by splitting the string by commas.
     * @param type
     *         - type of features in bus.
     * @return types 
     *         - List of types of a bus.
     */
    Set<Type> getTypes(String typeName)  throws IdeaBusException;

    /**
     * Get the individual seat value by splitting the string by commas.
     * @param seatName
     *         - Name of the seat.
     * @return seats 
     *         - List of seats in a bus.
     */
    Set<Seat> getSeats(String seatName) throws IdeaBusException;

    /**
     * Get the organization name by checking the list for the organization <br>
     * is already exist or not.
     * @param busName
     *         - Name of the organization.
     * @return organization 
     *         - Detail of the organization.
     */
    Organization getOrganization(String name) throws IdeaBusException ;

    /**
     * Get route for the given table from route table.
     *
     * @param routeName
     *              - contains the name of a route.
     *
     * @return the instance of route.
     */
    Route getRoute(String routeName) throws IdeaBusException;

    /**
     * Get list of user's buses.
     *
     * @param mailId
     *              - contains  mailId of user.
     *
     * @return a list of buses.
     */
    Set<BusDetail> getBusesByEmailId(String mailId) throws IdeaBusException;
    
    /**
     * Get list of register numbers from busDetails table.
     *
     * @param mailId
     *              - contains  mailId of user.
     *
     * @return a list of registerNumbers.
     */
    List<String> getRegisterNumbers(String mailId) throws IdeaBusException;
    
    /**
     * Get Bus detail By registration Number.
     * @return BusDetil 
     *         - Bus detail.
     */
    BusDetail getBusDetail(String registerNumber) throws IdeaBusException;

    /**
     * Get available seat detail in selected bus.
     * @param registrationNumber
     *         - registrationNumber of the selected bus.
     * @return seats 
     *         - seat detail.
     */
    Set<Seat> getAvailableSeats(String registrationNumber)
        throws IdeaBusException;

    /**
     * Check if Bus detail exists in bus detail table.
     * @param regNumber
     *              - contains registration number of the bus.
     */
    boolean checkBusDetail(String regNumber) throws IdeaBusException;
        
    /**
     * 1.Get the busDetail from the busDetail tables
     *   using it's existing register number.
     * 2.set new inputs into the busDetail.
     * 3.Update the busDetail in busDetail table.
     *
     *
     * @param organization
     *         - name of the travels.
     * @param routeName
     *         - name of the route.
     * @param types
     *         - different type of features in bus.
     * @param price
     *         - ticket price for respective seat.
     * @param registerNumber
     *         - new register number for the bus.
     * @param oldRegisterNumber
     *         - already registered register number of the bus.
     */
    void updateBusDetail(String organization, String routeName, String types,
        float price, String registerNumber, String oldRegisterNumber, 
        String date) throws IdeaBusException ;
    
    
    /**
     * 1.Get the busDetail from the busDetail database
     *  using the given register number.
     * 2.Set the status of the bus.
     * 3.Update the busDetail into busDetail table.
     *
     * @param registerNumber
     *                  - contains the register number of the bus.
     * @param status
     *                  - contains the status for a bus.
     */
    void setBusStatus(String registerNumber,String status)
        throws IdeaBusException;

    /*
     * Get list of user's buses.
     *
     * @param fromPlace
     *         - passenger arrival bus stop.
     * @param toPlace
     *         - Passenger departure bus stop.
     * @return a list of buses.
     */
    Set<BusDetail> getBusDetailByBusStops(String fromPlace, String toPlace, 
                                         String onward) throws IdeaBusException;
}
