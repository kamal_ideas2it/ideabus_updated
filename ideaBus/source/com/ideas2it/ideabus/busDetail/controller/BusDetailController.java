package com.ideas2it.ideabus.busDetail.controller;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import com.ideas2it.ideabus.busDetail.BusDetail;
import com.ideas2it.ideabus.busDetail.service.BusDetailService;
import com.ideas2it.ideabus.busDetail.service.impl.BusDetailServiceImpl;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.seat.Seat;
import com.ideas2it.ideabus.util.Constant;

/**
 * Manipulation operation of Bus Detail.
 * By Adding, Removing, Getting, Updating.
 */
public class BusDetailController {
	private static Logger logger = Logger.getLogger(BusDetailController.class);
    private BusDetailService busDetailService;

    /**
     * Pass inputs from servlet to service class. <br>
     * Details like busName, registerNumber, busStopName, type, seatName, price.
     * @param busName
     *         - name of the bus.
     * @param registerNumber
     *         - register number of the bus.
     * @param routeName
     *         - route of the bus.
     * @param busStopName
     *         - bus stops for respective bus.
     * @param type
     *         - type of features in bus.
     * @param seatName
     *         - seat name of respective bus.
     * @param price
     *         - ticket price for respective bus.
     * @param emailId
     *         - email id of the user.
     */
    public void addBusDetail(String busName, String registerNumber, 
        String routeName, String type, String seatName, 
        float price, String emailId, String date){
        try {
            busDetailService = new BusDetailServiceImpl();
            busDetailService.addBusDetail(busName, registerNumber, 
                routeName, type, seatName, price, emailId, date);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
    }

    /**
     * Get available seat detail in selected bus.
     * @param registrationNumber
     *         - registrationNumber of the selected bus.
     * @return seats 
     *         - seat detail.
     */
    public Set<Seat> getAvailableSeats(String registrationNumber) {
        Set<Seat> seats = null;
        try {
            busDetailService = new BusDetailServiceImpl();
            seats = busDetailService.getAvailableSeats(registrationNumber);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
        return seats;
    }
    
    /**
     * Get the list of user's buses.
     *
     * @param mailId
     *              - contains  mailId of user.
     *
     * @return a list of buses.
     */
    public Set<BusDetail> getBusesByEmailId(String mailId) {
        Set<BusDetail> busDetails = null;
        try {
            busDetailService = new BusDetailServiceImpl();
            return busDetailService.getBusesByEmailId(mailId);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
        return busDetails;
    }
        
    /**
     * Get the list of user's buses.
     *
     * @param mailId
     *              - contains  mailId of user.
     *
     * @return a list of buses.
     */
    public List<String> getRegisterNumbers(String mailId) {
        List<String> registerNumbers = null;
        try {
            busDetailService = new BusDetailServiceImpl();
            return busDetailService.getRegisterNumbers(mailId);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
        return registerNumbers;
    }
    
    /**
     * Get a single record of bus from busDetail table.
     *
     * @param registerNumber
     *                  - contains register number of a bus.
     *
     * @return a instance of busDetail.
     */
    public BusDetail getBusDetail(String registerNumber) {
        BusDetail busDetail = null;
        try {
            busDetailService = new BusDetailServiceImpl();
            return busDetailService.getBusDetail(registerNumber);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
        return busDetail;
    }
    
    /**
     * Update the details of bus for the given register number.
     *
     * @param organization
     *         - name of the travels.
     * @param routeName
     *         - name of the route.
     * @param types
     *         - different type of features in bus.
     * @param price
     *         - ticket price for respective seat.
     * @param registerNumber
     *         - new register number for the bus.
     * @param oldRegisterNumber
     *         - already registered register number of the bus.
     */
    public void updateBusDetail(String organization, String routeName,
        String types, float price, String registerNumber, 
        String oldRegisterNumber, String date) {
        try {
            busDetailService = new BusDetailServiceImpl();
            busDetailService.updateBusDetail(organization, routeName, types,
                price, registerNumber, oldRegisterNumber, date);
        } catch (IdeaBusException  e) {
            logger.error(e);
        }
    }
    
    /**
     * Change the status of the bus for the given register number.
     *
     * @param registerNumber
     *                  - contains the register number of the bus.
     * @param status
     *                  - contains the status for a bus.
     */
    public void setBusStatus(String registerNumber,String status) {
        try {
            busDetailService = new BusDetailServiceImpl();
            busDetailService.setBusStatus(registerNumber,status);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
    }

    /*
     * Get all Bus detail by bus Stops.
     *
     * @param fromPlace
     *         - passenger arrival bus stop.
     * @param toPlace
     *         - Passenger departure bus stop.
     * @return a list of buses.
     */
    public Set<BusDetail> getBusDetailByBusStops(String fromPlace, 
                                                String toPlace, String onward) {
        Set<BusDetail> busDetails = null;
        try {
            busDetailService = new BusDetailServiceImpl();
            busDetails = busDetailService.getBusDetailByBusStops(fromPlace, 
                                                               toPlace, onward);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
        return busDetails;
    }
}
