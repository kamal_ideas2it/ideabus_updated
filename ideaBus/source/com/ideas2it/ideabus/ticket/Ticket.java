package com.ideas2it.ideabus.ticket;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import com.ideas2it.ideabus.booking.Booking;
import com.ideas2it.ideabus.busDetail.BusDetail;
import com.ideas2it.ideabus.payment.Payment;

/**
 * Model class of Ticket.
 */
public class Ticket {

    private int id;
    private String date;
    private String journeyDate;
    private float totalAmount;
    private String status;
    private BusDetail busDetail;
    private Booking booking;
    private Payment payment;
    private String ticketNumber;
    
    public Ticket() {
    }

    public Ticket(String date, String journeyDate, float totalAmount, 
                      String status, BusDetail busDetail, Booking booking,
                      Payment payment, String ticketNumber) {
        this.journeyDate = journeyDate;
        this.totalAmount = totalAmount;
        this.date = date;
        this.status = status;
        this.busDetail = busDetail;
        this.booking = booking;
        this.payment = payment;
        this.ticketNumber = ticketNumber;
    }

    /**
     * Initializing Getter and Setter
     */
    public void setId(int id) {
        this.id = id;
    } 

    public int getId() {
        return id;
    }

    public void setJourneyDate(String journeyDate) {
        this.journeyDate = journeyDate;
    }

    public String getJourneyDate() {
        return journeyDate;
    }

    public void setTotalAmount(float totalAmount) {
        this.totalAmount = totalAmount;
    }

    public float getTotalAmount() {
        return totalAmount;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setBusDetail(BusDetail busDetail) {
        this.busDetail = busDetail;
    }

    public BusDetail getBusDetail() {
        return busDetail;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setTicketNumber(String TicketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }
}
