package com.ideas2it.ideabus;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.io.IOException;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;

import com.ideas2it.ideabus.ticket.controller.TicketController;
import com.ideas2it.ideabus.util.Constant;

/**
 * 1.Get inputs from web page and store them into database
 * 2.Get data from database based on conditions
 * and check whether given inputs exists or not.
 */
public class TicketServlet extends HttpServlet {  

    private TicketController ticketController;

    /**
     * Get the inputs from web page and pass them to service
     * @param request
     *              - contains object of HttpServletRequest.
     * @param response
     *              - contains object of HttpServletResponse.
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response)  
                                        throws ServletException, IOException {
        response.setContentType("text/html");//setting the content type  
        HttpSession session = request.getSession();  
        String userEmailId = (String)session.getAttribute(Constant.EMAIL_ID);
        String userEmailId = request.getParameter("emailId");
        String cardNumber = request.getParameter("cardNumber");
        String cardName = request.getParameter("cardName");
        String date = request.getParameter("date");
        float totalAmount = Float.parseFloat(request.getParameter("totalAmount"));
		ticketController = new TicketController();
        ticketController.addTicket(userEmailId, cardNumber, cardName,
                                       date, totalAmount);
        response.sendRedirect(Constant.PATH + "Success.jsp");
    }
}
