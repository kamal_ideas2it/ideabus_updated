package com.ideas2it.ideabus.ticket.service.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.lang.NullPointerException; 
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import com.ideas2it.ideabus.booking.Booking;
import com.ideas2it.ideabus.busDetail.BusDetail;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.payment.Payment;
import com.ideas2it.ideabus.ticket.Ticket;
import com.ideas2it.ideabus.ticket.dao.impl.TicketDAOImpl;
import com.ideas2it.ideabus.ticket.dao.TicketDAO;
import com.ideas2it.ideabus.ticket.service.TicketService;
import com.ideas2it.ideabus.util.Constant;

/**
 * Manipulation operation of ticket Details.
 * By Adding, Removing, Getting, Updating.
 */
public class TicketServiceImpl implements TicketService {
    private TicketDAO ticketDAO;

    /**
     * For Add the ticket detail to passing the variable to DAO. <br>
     * @param date
     *         - Date of booking ticket.
     * @param journeyDatee
     *         - date of journey.
     * @param totalAmount
     *         - total amount of the ticket.
     * @param status
     *         - Status of the ticket.
     * @param busDetail
     *         - busDetail id for the passenger.
     * @param booking
     *         - booking id for the passenger.
     * @param payment
     *         - payment id for the passenger.
     */
    public void addTicket(String date, String journeyDate, float totalAmount, 
                              String status, BusDetail busDetail, 
                              Booking booking, Payment payment, 
                              String ticketNumber) throws IdeaBusException {
        ticketDAO = new TicketDAOImpl();
        Ticket ticket = new Ticket(date, journeyDate, totalAmount, status, 
                                     busDetail, booking, payment, ticketNumber);
        ticketDAO.addTicket(ticket);
    }

    /**
     * Get the list of tickets by the BookingId for cancel the ticket. <br>
     * @param booking
     *         - Booking details.
     */
    public Ticket getTicket(Booking booking) 
        throws IdeaBusException {
        ticketDAO = new TicketDAOImpl();
        return ticketDAO.getTicket(booking);
    }

    /**
     * Get the ticket by the ticket serial number. <br>
     * @param ticketNumber
     *         -  serial number of the ticket.
     */
    public Ticket getBookingTicket(String ticketNumber) 
                                            throws IdeaBusException {
        ticketDAO = new TicketDAOImpl();
        return ticketDAO.getBookingTicket(ticketNumber);
    }

    /**
     * Cancel booked ticket by ticket serial Number inactive ticket status. <br>
     * @param ticketNumber
     *         -  serial number of the ticket.
     */
    public void cancelBookedTicket(String ticketNumber) 
                                                throws IdeaBusException {
        ticketDAO = new TicketDAOImpl();
        Ticket ticket = getBookingTicket(ticketNumber);
        ticket.setStatus(Constant.IN_ACTIVE);
        ticketDAO.updateTicket(ticket);
    }
}
