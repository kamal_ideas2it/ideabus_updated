package com.ideas2it.ideabus.ticket.service;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;

import com.ideas2it.ideabus.booking.Booking;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.busDetail.BusDetail;
import com.ideas2it.ideabus.payment.Payment;
import com.ideas2it.ideabus.ticket.Ticket;

/**
 * Manipulation operation of Ticket Details.
 * By Adding, Removing, Getting, Updating.
 */
public interface TicketService {

    /**
     * For Add the ticket detail to passing the variable to DAO. <br>
     * @param date
     *         - Date of booking ticket.
     * @param journeyDatee
     *         - date of journey.
     * @param totalAmount
     *         - total amount of the ticket.
     * @param status
     *         - Status of the ticket.
     * @param busDetail
     *         - busDetail id for the passenger.
     * @param booking
     *         - booking id for the passenger.
     * @param payment
     *         - payment id for the passenger.
     */
    void addTicket(String date, String journeyDate, float totalAmount, 
                       String status, BusDetail busDetail, Booking booking, 
                       Payment payment, String ticketNumber) 
                       throws IdeaBusException;

    /**
     * Get the list of tickets by the BookingId for cancel the ticket. <br>
     * @param booking
     *         - Booking details.
     */
    Ticket getTicket(Booking booking) throws IdeaBusException;

    /**
     * Get the ticket by the ticket serial number. <br>
     * @param ticketNumber
     *         -  serial number of the ticket.
     */
    Ticket getBookingTicket(String ticketNumber) throws IdeaBusException;

    /**
     * Cancel booked ticket by ticket serial Number inactive ticket status. <br>
     * @param ticketNumber
     *         -  serial number of the ticket.
     */
    void cancelBookedTicket(String ticketNumber) throws IdeaBusException;
}
