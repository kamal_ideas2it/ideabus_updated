package com.ideas2it.ideabus.ticket.dao.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria; 
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.ticket.Ticket;
import com.ideas2it.ideabus.ticket.dao.TicketDAO;
import com.ideas2it.ideabus.booking.Booking;
import com.ideas2it.ideabus.util.Constant;
import com.ideas2it.ideabus.util.Factory;

/**
 * Manipulation operation of Booking.
 * By Adding, Removing, Getting, Updating.
 * Using Linked List for fast manipulation.
 */
public class TicketDAOImpl implements TicketDAO {
	private static Logger logger = Logger.getLogger(TicketDAOImpl.class);
    private Session session;
    private Transaction transaction;

    /**
     * Add Ticket Detail.
     * @param ticket.
     *             - ticket detail.
     */
    public void addTicket(Ticket ticket) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.save(ticket);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Get All Ticket detail from the Database and return to Ticket Service.
     * @return tickets.
     *             - All ticket detail in the list.
     */
    public List<Ticket> getAllTickets() throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        List<Ticket> tickets = new LinkedList<>();
        try {
            transaction = session.beginTransaction();
            tickets = session.createQuery("FROM Ticket").list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return tickets;
    }

    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get a single record of user from user table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session
     * 5.Return user data.
     *
     * @param booking.
     *             - booking detail.
     */
    public Ticket getTicket(Booking booking) throws IdeaBusException {
        Ticket ticket;
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            Criteria criteria = session.createCriteria(Ticket.class);
            criteria.add(Restrictions.eq("booking",booking));
            ticket = (Ticket)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return ticket;
    }

    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get a single record of user from user table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session
     * 5.Return user data.
     *
     * @param ticketNumber.
     *             - Serial Number of the ticket.
     */
    public Ticket getBookingTicket(String ticketNumber) 
                                                   throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        Ticket ticket = new Ticket();
        try {
            Criteria criteria = session.createCriteria(Ticket.class);
            criteria.add(Restrictions.eq("ticketNumber", ticketNumber));
            ticket = (Ticket)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return ticket;
    }

    /**
     * Update ticket Detail.
     * @param ticket.
     *             - ticket detail.
     */
    public void updateTicket(Ticket ticket) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.update(ticket);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
