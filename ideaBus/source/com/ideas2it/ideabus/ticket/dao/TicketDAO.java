package com.ideas2it.ideabus.ticket.dao;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.booking.Booking;
import com.ideas2it.ideabus.ticket.Ticket;

/**
 * Manipulation operation of Ticket.
 * By Adding, Removing, Getting, Updating.
 * Using Linked List for fast manipulation.
 */
public interface TicketDAO {

    /**
     * Add Ticket Detail.
     * @param ticket.
     *             - Ticket detail.
     */
    void addTicket(Ticket ticket) throws IdeaBusException;

    /**
     * Get All Ticket detail from the Database and return to Ticket Service.
     * @return tickets.
     *             - All Ticket detail in the list.
     */
    List<Ticket> getAllTickets() throws IdeaBusException;

    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get a single record of user from user table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session
     * 5.Return user data.
     *
     * @param booking.
     *             - booking detail.
     */
    Ticket getTicket(Booking booking) throws IdeaBusException;

    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get a single record of user from user table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session
     * 5.Return user data.
     *
     * @param ticketNumber.
     *             - Serial Number of the ticket.
     */
    Ticket getBookingTicket(String ticketNumber) throws IdeaBusException;

    /**
     * Update ticket Detail.
     * @param ticket.
     *             - ticket detail.
     */
    void updateTicket(Ticket ticket) throws IdeaBusException;
}
