package com.ideas2it.ideabus.ticket.controller;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import org.apache.log4j.Logger;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.ticket.service.impl.TicketServiceImpl;
import com.ideas2it.ideabus.ticket.service.TicketService;
import com.ideas2it.ideabus.util.Constant;

/**
 * Manipulation operation of Ticket ticket.
 * By Adding, Removing, Getting, Updating.
 */
public class TicketController {
	private static Logger logger = Logger.getLogger
                                          (BookingController.class);
    private TicketService ticketService;

    /**
     * Pass inputs from servlet to service class. <br>
     * Details like numberOfPassenger, firstName, lastName, gender, <br> 
     * seatName, age, phoneNumber, emailId, date.
     * @param userEmailId
     *         - emailId of the user.
     * @param cardNumber
     *         - User debit/credit card number.
     * @param cardName
     *         - User debit/credit card name.
     * @param date
     *         - Date of Payment.
     * @param totalAmount
     *         - Total amount of the ticket.
     */
    public void addTicket(String userEmailId, String cardNumber, 
                             String cardName, String date, float totalAmount) {
        try {
		    ticketService = new TicketServiceImpl();
            ticketService.addTicket(userEmailId, cardNumber, cardName, date, 
                                          totalAmount);
        } catch (IdeaBusException e) {
            logger.error(e);
            return Constant.ERROR;
        }
    }
}
