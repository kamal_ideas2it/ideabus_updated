package com.ideas2it.ideabus.employee.dao.implement;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;

import com.ideas2it.ideabus.employee.dao.EmployeeDAO;
import com.ideas2it.ideabus.employee.Employee;
import com.ideas2it.ideabus.util.Factory;

public class EmployeeDAOImplement implements EmployeeDAO {

    public void addEmployee(Employee employee) {
        SessionFactory sessionFactory = Factory.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.save(employee);
            transaction.commit();
        } catch (HibernateException exception) {
            if (transaction != null) {
                transaction.rollback();
            }
            exception.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List<Employee> getEmployee() {
        SessionFactory sessionFactory = Factory.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
         List<Employee> employees = new LinkedList<>();
        try {
            transaction = session.beginTransaction();
            employees = session.createQuery("FROM Employee").list();

            transaction.commit();
        } catch (HibernateException exception) {
            if (transaction != null) {
                transaction.rollback();
            }
            exception.printStackTrace();
        } finally {
            session.close();
        }
        return employees;
    }
}
