package com.ideas2it.ideabus.employee.dao;

import java.util.List;

import com.ideas2it.ideabus.employee.Employee;

public interface EmployeeDAO {

    void addEmployee(Employee employee);

    List<Employee> getEmployee();
}
