package com.ideas2it.ideabus;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;

import com.ideas2it.ideabus.employee.service.EmployeeService;
import com.ideas2it.ideabus.employee.service.implement.EmployeeServiceImplement;

public class EmployeeServlet extends HttpServlet {  
    public void doPost(HttpServletRequest request, HttpServletResponse response)  
                                        throws ServletException, IOException {
        response.setContentType("text/html");//setting the content type  
        String designation = request.getParameter("designation");
        EmployeeService employeeService = new EmployeeServiceImplement();
        employeeService.addEmployee(designation);
    }
}
