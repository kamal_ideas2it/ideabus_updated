package com.ideas2it.ideabus.employee.service.implement;

import java.util.List;

import com.ideas2it.ideabus.employee.dao.EmployeeDAO;
import com.ideas2it.ideabus.employee.dao.implement.EmployeeDAOImplement;
import com.ideas2it.ideabus.employee.Employee;
import com.ideas2it.ideabus.employee.service.EmployeeService;

public class EmployeeServiceImplement implements EmployeeService {

    public void addEmployee(String name) {

        Employee employee = new Employee(name);
        EmployeeDAO employeeDAO = new EmployeeDAOImplement();
        employeeDao.addEmployee(employee);
    }

    public List<Employee> getEmployee() {
        EmployeeDAO employeeDAO = new EmployeeDAOImplement();
        return employeeDao.getEmployee();
    }
}
