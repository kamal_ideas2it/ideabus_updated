package com.ideas2it.ideabus.employee.service;

import java.util.List;

import com.ideas2it.ideabus.employee.Employee;

public interface EmployeeService {

    void addEmployee(String designation);

    List<Employee> getEmployee();
}
