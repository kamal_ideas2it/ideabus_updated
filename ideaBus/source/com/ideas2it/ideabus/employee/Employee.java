package com.ideas2it.ideabus.employee;

import com.ideas2it.ideabus.organization.Organization;
import com.ideas2it.ideabus.user.User;

public class Employee {
    private int id;
    private String name;
    private User user;
    private Organization organization;

    public Employee() {
    }

    public Employee(String name) {
        this.name = name;
        this.user = user;
        this.organization = organization;
    }
            
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setOrgaization(Organization organization) {
        this.organization = organization;
    }

    public Organization getOrganization() {
        return organization;
    }
}
