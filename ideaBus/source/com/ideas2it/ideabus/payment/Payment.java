package com.ideas2it.ideabus.payment;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import com.ideas2it.ideabus.user.User;

public class Payment {

    private int id;
    private String cardName;
    private String cardNumber;
    private String date;
    private float totalAmount;
    private String status;
    private User user;
    
    public Payment() {
    }

    public Payment(String cardName, String cardNumber, String date, 
                       float totalAmount, String status, User user) {
        this.cardName = cardName;
        this.cardNumber = cardNumber;
        this.totalAmount = totalAmount;
        this.date = date;
        this.status = status;
        this.user = user;
    }

    /**
     * Initializing Getter and Setter
     */
    public void setId(int id) {
        this.id = id;
    } 

    public int getId() {
        return id;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    } 

    public String getCardNumber() {
        return cardNumber;
    }

    public void setTotalAmount(float totalAmount) {
        this.totalAmount = totalAmount;
    }

    public float getTotalAmount() {
        return totalAmount;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
