package com.ideas2it.ideabus.refund;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import com.ideas2it.ideabus.payment.Payment;
import com.ideas2it.ideabus.ticket.Ticket;
import com.ideas2it.ideabus.user.User;

public class Refund {

    private int id;
    private Date date;
    private int amount;
    private String status;
    private User user;
    private Ticket ticket;
    private Payment payment;
    
    public Refund() {
    }

    public Refund(Date date, int amount, String status, User user, 
                      Ticket ticket, Payment payment) {
        this.amount = amount;
        this.date = date;
        this.status = status;
        this.user = user;
        this.ticket = ticket;
        this.payment = payment;
    }

    /**
     * Initializing Getter and Setter
     */
    public void setId(int id) {
        this.id = id;
    } 

    public int getId() {
        return id;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Payment getPayment() {
        return payment;
    }
}
