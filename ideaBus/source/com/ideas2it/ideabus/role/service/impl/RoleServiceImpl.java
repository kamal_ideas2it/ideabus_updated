package com.ideas2it.ideabus.role.service.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.Iterator;
import java.util.List;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.role.Role;
import com.ideas2it.ideabus.role.dao.RoleDAO;
import com.ideas2it.ideabus.role.dao.impl.RoleDAOImpl;
import com.ideas2it.ideabus.role.service.RoleService;
import com.ideas2it.ideabus.util.Constant;

/**
 * Perform logical operations on data like,
 * 1.Check if role exists in role table then add new data into the table.
 * 2.Get the list of roles from role.
 * 3.Get a role data from role table using it's name.
 * 4.Update role data with given input.
 * 5.Delete the role data from role table.
 */
public class RoleServiceImpl implements RoleService {
    private RoleDAO roleDAO;
    
    /**
     * 1.Check role data in the role table 
     * 2.Add new role data into the role table.
     *
     * @param name
     *         - name of the role.
     * @return contains a String.
     */
    public String checkAndAddRole(String name) throws IdeaBusException {
        boolean roleExists = checkRole(name);
        if (true == roleExists) {
            return (Constant.ALREADY + " " + Constant.ADDED);
        }
        roleDAO = new RoleDAOImpl();
        Role role = new Role(name);
        roleDAO.addRole(role);
        return Constant.ADDED;
    }
    
    /**
     * Check given role exists or not.
     *
     * @param name
     *          - contains name of role.
     *
     * @return contains boolean value.
     */
    public boolean checkRole(String name) throws IdeaBusException {
        Role role = getRole(name);
        if ( null == role ) {
                return false;
            }
        return true;
    }

    /**
     * Get list of roles from role table.
     * 
     * @return contains list of role.
     */
    public List<String> getRoles() throws IdeaBusException  {
       roleDAO = new RoleDAOImpl();
       return roleDAO.getRoles();
    }
       
    /**
     * Get the role data from role table using it's name.
     *
     * @param name
     *              - contains name of role.
     */
    public Role getRole(String name) throws IdeaBusException  {
        roleDAO = new RoleDAOImpl(); 
        return roleDAO.getRole(name);
    }
    
    /**
     * Update role data with the given input in role table.
     *
     * @param name
     *          - contains name of the role to be updated.
     * @param input
     *          - contains input.
     */
    public void updateRole(String name , String input) throws IdeaBusException {
        roleDAO = new RoleDAOImpl();
        Role role = getRole(name);
        role.setName(input);
        roleDAO.updateRole(role);
    }
        
    /**
     * Delete role data in role table.
     *
     * @param name
     *          - contains name of the role to be updated.
     */
    public void deleteRole(String name) throws IdeaBusException {
        roleDAO = new RoleDAOImpl();
        Role role = roleDAO.getRole(name);
        roleDAO.deleteRole(role);
    }
}
