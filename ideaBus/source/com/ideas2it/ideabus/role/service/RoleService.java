package com.ideas2it.ideabus.role.service;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.role.Role;

/**
 * Perform logical operations on data like,
 * 1.Check if role exists in role table then add new data into the table.
 * 2.Get the list of roles from role.
 * 3.Get a role data from role table using it's name.
 * 4.Update role data with given input.
 * 5.Delete the role data from role table.
 */
public interface RoleService {

    /**
     * 1.Check role data in the role table 
     * 2.Add new role data into the role table.
     *
     * @param name
     *         - name of the role.
     * @return contains a String.
     */
    String checkAndAddRole(String name) throws IdeaBusException;

    /**
     * Check given role exists or not.
     *
     * @param name
     *          - contains name of role.
     *
     * @return contains boolean value.
     */
    boolean checkRole(String name) throws IdeaBusException;
    
    /**
     * Get list of roles from role table.
     * 
     * @return contains list of role.
     */
    List<String> getRoles() throws IdeaBusException;
    
    /**
     * Get the role data from role table using it's name.
     *
     * @param name
     *              - contains name of role.
     */
    Role getRole(String name) throws IdeaBusException;
    
    /**
     * Update role data with the given input in role table.
     *
     * @param naem
     *          - contains name of the role to be updated.
     * @param input
     *          - contains input.
     */
    void updateRole(String name, String input) throws IdeaBusException;
    
    /**
     * Delete role data in role table.
     *
     * @param name
     *          - contains name of the role to be delete.
     */
    void deleteRole(String name) throws IdeaBusException;
}
