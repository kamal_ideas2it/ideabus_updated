package com.ideas2it.ideabus.role.controller;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.role.Role;
import com.ideas2it.ideabus.role.service.RoleService;
import com.ideas2it.ideabus.role.service.impl.RoleServiceImpl;
import com.ideas2it.ideabus.util.Constant;

/**
 * 1.Use to control the flow of data
 * 2.Handle the exceptions.
 */
public class RoleController {
    
    private static final Logger logger = Logger.getLogger(RoleController.class);
    
    private RoleService roleService;
    
    /**
     * Add role data into role table and handle it's exception.
     *
     * @param name
     *          - contains name of role.
     *
     * @return contains a String.
     */
    public String addRole(String name) {
        roleService = new RoleServiceImpl();
        try {
            return roleService.checkAndAddRole(name);
        } catch (IdeaBusException e) {
            logger.error(e);
            return Constant.ERROR;
        }
    }
    
    /**
     * Get the list of roles from roles table and handle it's exception.
     *
     * @return contains list of roles.
     */
    public List<String> getRoles() {
        roleService = new RoleServiceImpl();
        try {
            return roleService.getRoles();
        } catch (IdeaBusException e) {
            logger.error(e);
            return Collections.<String>emptyList();
        }
    }
    
    /**
     * Update the role data in role table and handle it's exception.
     *
     * @param role
     *          - contains name of the role.
     * @param input
     *          - contains input.
     */
    public void updateRole(String role,String input) {
        roleService = new RoleServiceImpl();
        try {
            roleService.updateRole(role,input);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
    }
        
    /**
     * Delete the role data in role table and hadle it's exception.
     *
     * @param role
     *          - contains name of the role.
     */
    public void deleteRole(String role) {
        roleService = new RoleServiceImpl();
        try {
            roleService.deleteRole(role);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
    }
}
