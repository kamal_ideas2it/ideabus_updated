package com.ideas2it.ideabus.role;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 *
 * Declare variables for role related functions and encapsulate them.
 */
public class Role {

    private int id;
    private String name;

    public Role() {
    }

    public Role(String name) {
        this.name = name;
    }

    /**
     * Getter and Setter
     */
    public void setId(int id) {
        this.id = id;
    } 

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
