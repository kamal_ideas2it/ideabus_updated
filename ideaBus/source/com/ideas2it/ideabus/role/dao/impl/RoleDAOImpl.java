package com.ideas2it.ideabus.role.dao.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria; 
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Restrictions;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.role.Role;
import com.ideas2it.ideabus.role.dao.RoleDAO;
import com.ideas2it.ideabus.util.Constant;
import com.ideas2it.ideabus.util.Factory;

/**
 * Perform manipulation operations on input.
 * 1.Add role data into role table.
 * 2.Get list of roles from role table and return them.
 * 2.Get single record of a role from role table and return them.
 * 3.Update existing role data in role table.
 * 4.Delere existing role data in role table.
 */
public class RoleDAOImpl implements RoleDAO {
    private Session session;
    private Transaction transaction;
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Add Role to role table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw the exception.
     * 5.Finally check and close the session.
     *
     * @param role.
     *             - role detail.
     */
    public void addRole(Role role) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.save(role);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_ROLE_001 + e);
        } finally {
            Factory.closeSession(session);
        }
    }
   
    /**
     * 1.Get Session factory that created this session,
     *  begin the transaction.
     * 2.Get list of roles from the Database and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session.
     * 5.Return list of roles.
     *
     * @return contains list of roles.
     */
    public List<String> getRoles() throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        List<String> roles = new ArrayList<>();
        try {
            Criteria criteria = session.createCriteria(Role.class);
            ProjectionList projectionList = Projections.projectionList();
            projectionList.add(Projections.property(Constant.NAME));
            criteria.setProjection(projectionList);
            roles = criteria.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_ROLE_002 + e);
        } finally {
            Factory.closeSession(session);
        }
        return roles;
    }

    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get a single record of role from role table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw thr exception.
     * 5.Finally check and close the session.
     * 6.Return role data.
     *
     * @param role.
     *             - role detail.
     */
    public Role getRole(String name) throws IdeaBusException {
        Role role = new Role();
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            Criteria criteria = session.createCriteria(Role.class);
            criteria.add(Restrictions.eq(Constant.NAME,name));
            role = (Role)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_ROLE_002 + e);
        } finally {
            Factory.closeSession(session);
        }
        return role;
    }
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Update a role data in role table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
          * 4.Catch and throw thr exception.
     * 5.Finally check and close the session.
     *
     * @param role
     *          - contains instance of role.
     */
    public void updateRole(Role role) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.update(role);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_ROLE_003 + e);
        } finally {
            Factory.closeSession(session);
        }
    }
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Delete a role data in role table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw thr exception.
     * 5.Finally check and close the session.
     *
     * @param role
     *          - contains instance of role.
     */
    public void deleteRole(Role role) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.delete(role);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_ROLE_004 + e);
        } finally {
            Factory.closeSession(session);
        }
    }
}
