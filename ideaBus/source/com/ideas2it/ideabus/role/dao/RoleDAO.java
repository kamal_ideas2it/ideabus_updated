package com.ideas2it.ideabus.role.dao;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.role.Role;

/**
 * Perform manipulation operations on input.
 * 1.Add instance of Role class int role table.
 * 2.Get list of roles from role table and return them.
 * 2.Get single record of a role from role table and return them.
 * 3.Update existing role data in role table.
 */
public interface RoleDAO {
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Add Role to role table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw thr exception.
     * 5.Finally check and close the session.
     *
     * @param role.
     *             - role detail.
     */
    void addRole(Role role) throws IdeaBusException;
            
    /**
     * 1.Get Session factory that created this session,
     *  begin the transaction.
     * 2.Get list of roles from the Database and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session.
     * 5.Return list of roles.
     *
     * @return contains list of busroles.
     */
    List<String> getRoles() throws IdeaBusException;

    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get a single record of role from role table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw thr exception.
     * 5.Finally check and close the session.
     * 6.Return role data.
     *
     * @param role.
     *             - role detail.
     */
    Role getRole(String name) throws IdeaBusException;
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Update a role data in role table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
          * 4.Catch and throw thr exception.
     * 5.Finally check and close the session.
     *
     * @param role
     *          - contains instance of role.
     */
    public void updateRole(Role role) throws IdeaBusException;
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Delete a role data in role table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw thr exception.
     * 5.Finally check and close the session.
     *
     * @param role
     *          - contains instance of role.
     */
    public void deleteRole(Role role) throws IdeaBusException;
}
