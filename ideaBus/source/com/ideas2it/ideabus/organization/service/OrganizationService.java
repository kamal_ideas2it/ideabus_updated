package com.ideas2it.ideabus.organization.service;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.organization.Organization;

/**
 * Perform logical operations on data like,
 * 1.Check if organization exists in organization table then add new data into the table.
 * 2.Get the list of organizations from organization.
 * 3.Get a organization data from organization table using it's name.
 * 4.Update organization data with given input.
 * 5.Delete the organization data from organization table.
 */
public interface OrganizationService {

    /**
     * 1.Check organization data in the organization table 
     * 2.Add new organization data into the organization table.
     *
     * @param name
     *         - name of the organization.
     * @return contains a String.
     */
    String checkAndAddOrganization(String name) throws IdeaBusException;

    /**
     * Check given organization exists or not.
     *
     * @param name
     *          - contains name of organization.
     *
     * @return contains boolean value.
     */
    boolean checkOrganization(String name) throws IdeaBusException;
    
    /**
     * Get list of organizations from organization table.
     * 
     * @return contains list of organization.
     */
    List<String> getOrganizations() throws IdeaBusException;
    
    /**
     * Get the organization data from organization table using it's name.
     *
     * @param name
     *              - contains name of organization.
     */
    Organization getOrganization(String name) throws IdeaBusException;
    
    /**
     * Update organization data with the given input in organization table.
     *
     * @param naem
     *          - contains name of the organization to be updated.
     * @param input
     *          - contains input.
     */
    void updateOrganization(String name , String input) throws IdeaBusException;
    
    /**
     * Delete organization data in organization table.
     *
     * @param name
     *          - contains name of the organization to be delete.
     */
    void deleteOrganization(String name) throws IdeaBusException;
}
