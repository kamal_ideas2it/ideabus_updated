package com.ideas2it.ideabus.organization.service.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.Iterator;
import java.util.List;

import com.ideas2it.ideabus.exception.IdeaBusException;

import com.ideas2it.ideabus.organization.Organization;
import com.ideas2it.ideabus.organization.dao.OrganizationDAO;
import com.ideas2it.ideabus.organization.dao.impl.OrganizationDAOImpl;
import com.ideas2it.ideabus.organization.service.OrganizationService;

import com.ideas2it.ideabus.util.Constant;

/**
 * Perform logical operations on data like,
 * 1.Check if organization exists in organization table then add new data into the table.
 * 2.Get the list of organizations from organization.
 * 3.Get a organization data from organization table using it's name.
 * 4.Update organization data with given input.
 * 5.Delete the organization data from organization table.
 */
public class OrganizationServiceImpl implements OrganizationService {
    private OrganizationDAO organizationDAO;
    
    /**
     * 1.Check organization data in the organization table 
     * 2.Add new organization data into the organization table.
     *
     * @param name
     *         - name of the organization.
     * @return contains a String.
     */
    public String checkAndAddOrganization(String name)
        throws IdeaBusException {
        boolean organizationExists = checkOrganization(name);
        if ( true == organizationExists ) {
            return (Constant.ALREADY + " " + Constant.ADDED);
        }
        organizationDAO = new OrganizationDAOImpl();
        Organization organization = new Organization(name);
        organizationDAO.addOrganization(organization);
        return Constant.ADDED;
    }
    
    /**
     * Check given organization exists or not.
     *
     * @param name
     *          - contains name of organization.
     *
     * @return contains boolean value.
     */
    public boolean checkOrganization(String name) throws IdeaBusException {
        Organization organization = getOrganization(name);
        if ( null == organization ) {
                return false;
            }
        return true;
    }

    /**
     * Get list of organizations from organization table.
     * 
     * @return contains list of organization.
     */
    public List<String> getOrganizations() throws IdeaBusException {
       organizationDAO = new OrganizationDAOImpl();
       return organizationDAO.getOrganizations();
    }
       
    /**
     * Get the organization data from organization table using it's name.
     *
     * @param name
     *              - contains name of organization.
     */
    public Organization getOrganization(String name) throws IdeaBusException {
        organizationDAO = new OrganizationDAOImpl(); 
        return organizationDAO.getOrganization(name);
    }
    
    /**
     * Update organization data with the given input in organization table.
     *
     * @param naem
     *          - contains name of the organization to be updated.
     * @param input
     *          - contains input.
     */
    public void updateOrganization(String name , String input)
        throws IdeaBusException {
        organizationDAO = new OrganizationDAOImpl();
        Organization organization = getOrganization(name);
        organization.setName(input);
        organizationDAO.updateOrganization(organization);
    }
        
    /**
     * Delete organization data in organization table.
     *
     * @param name
     *          - contains name of the organization to be updated.
     */
    public void deleteOrganization(String name) throws IdeaBusException {
        organizationDAO = new OrganizationDAOImpl();
        Organization organization = organizationDAO.getOrganization(name);
        organizationDAO.deleteOrganization(organization);
    }
}
