package com.ideas2it.ideabus.organization;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 *
 * Declare variables for organization related functions and encapsulate them.
 */
public class Organization {
    private int id;
    private String name;

    public Organization() {
    }

    public Organization(String name) {
        this.name = name;
    }
              
    /**
     * Getter and Setter
     */  
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
