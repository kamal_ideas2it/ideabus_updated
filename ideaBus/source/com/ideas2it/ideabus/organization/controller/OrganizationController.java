package com.ideas2it.ideabus.organization.controller;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.List;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.organization.Organization;
import com.ideas2it.ideabus.organization.service.impl.OrganizationServiceImpl;
import com.ideas2it.ideabus.organization.service.OrganizationService;
import com.ideas2it.ideabus.util.Constant;

/**
 * 1.Use to control the flow of data
 * 2.Handle the exceptions.
 */
public class OrganizationController {
    private static final Logger logger = Logger.getLogger
                                                 (OrganizationController.class);
    private OrganizationService organizationService;
    
    /**
     * Add organization date into organization table and handle it's exception.
     *
     * @param name
     *          - contains name of organization.
     *
     * @return contains a String.
     */
    public String addOrganization(String name) {
        organizationService = new OrganizationServiceImpl();
        try {
            return organizationService.checkAndAddOrganization(name);
        } catch (IdeaBusException e) {
            logger.error(e);
            return Constant.ERROR;
        }
    }
        
    /**
     * Get the list of organizations from organizations table and handle it's exception.
     *
     * @return contains list of organizations.
     */
    public List<String> getOrganizations() {
        organizationService = new OrganizationServiceImpl();
        try {
            return organizationService.getOrganizations();
        } catch (IdeaBusException e) {
            logger.error(e);
            return Collections.<String>emptyList();
        }
    }
    
    /**
     * Update the organization data in organization table and hadle it's exception.
     *
     * @param organization
     *          - contains name of the organization.
     * @param input
     *          - contains input.
     */
    public void updateOrganization(String organization,String input) {
        organizationService = new OrganizationServiceImpl();
        try {
            organizationService.updateOrganization(organization,input);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
    }
        
    /**
     * Delete the organization data in organization table and hadle it's exception.
     *
     * @param organization
     *          - contains name of the organization.
     */
    public void deleteOrganization(String organization) {
        organizationService = new OrganizationServiceImpl();
        try {
            organizationService.deleteOrganization(organization);
        }catch (IdeaBusException e) {
            logger.error(e);
        }
    }
}
