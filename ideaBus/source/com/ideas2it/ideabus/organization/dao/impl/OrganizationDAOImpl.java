package com.ideas2it.ideabus.organization.dao.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria; 
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Restrictions;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.organization.Organization;
import com.ideas2it.ideabus.organization.dao.OrganizationDAO;
import com.ideas2it.ideabus.util.Constant;
import com.ideas2it.ideabus.util.Factory;

/**
 * Perform manipulation operations on input.
 * 1.Add organization data into organization table.
 * 2.Get list of organizations from organization table and return them.
 * 2.Get single record of a organization from organization table and return them.
 * 3.Update existing organization data in organization table.
 * 4.Delere existing organization data in organization table.
 */
public class OrganizationDAOImpl implements OrganizationDAO {
    private Session session;
    private Transaction transaction;
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Add organization to organization table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw the exception.
     * 5.Finally check and close the session.
     *
     * @param organization.
     *             - organization detail.
     */
    public void addOrganization(Organization organization)
        throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.save(organization);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException
                (Constant.ERROR_CODE_ORGANIZATION_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get list of organizations from the Database and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session.
     * 5.Return list of organizations.
     *
     * @return contains list of organizations.
     */
    public List<String> getOrganizations() throws IdeaBusException {
        Session session = Factory.getSession();
        Transaction transaction = session.beginTransaction();
        List<String> organizations = new ArrayList<>();
        try {
            Criteria criteria = session.createCriteria(Organization.class);
            ProjectionList projectionList = Projections.projectionList();
            projectionList.add(Projections.property(Constant.NAME));
            criteria.setProjection(projectionList);
            organizations = criteria.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException
                (Constant.ERROR_CODE_ORGANIZATION_002 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return organizations;
    }
        
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get a single record of organization from organization table
     *   and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw the exception.
     * 5.Finally check and close the session.
     * 6.Return organization data.
     *
     * @param organization.
     *             - organization detail.
     */
    public Organization getOrganization(String name) throws IdeaBusException {
        Organization organization = new Organization();
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            Criteria criteria = session.createCriteria(Organization.class);
            criteria.add(Restrictions.eq(Constant.NAME,name));
            organization = (Organization)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException
                (Constant.ERROR_CODE_ORGANIZATION_002 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return organization;
    }
    
        
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Update a organization data in organization table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw the exception.
     * 5.Finally check and close the session.
     *
     * @param organization
     *          - contains instance of organization.
     */
    public void updateOrganization(Organization organization)
        throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.update(organization);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException
                (Constant.ERROR_CODE_ORGANIZATION_003 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Delete a organization data in organization table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw the exception.
     * 5.Finally check and close the session.
     *
     * @param organization
     *          - contains instance of organization.
     */
    public void deleteOrganization(Organization organization)
        throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.delete(organization);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException
                (Constant.ERROR_CODE_ORGANIZATION_004 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
