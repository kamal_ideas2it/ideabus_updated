package com.ideas2it.ideabus.organization.dao;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.organization.Organization;

/**
 * Perform manipulation operations on input.
 * 1.Add instance of organization class int organization table.
 * 2.Get list of organizations from organization table and return them.
 * 2.Get single record of a organization from organization table and return them.
 * 3.Update existing organization data in organization table.
 */
public interface OrganizationDAO {
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Add organization to organization table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw thr exception.
     * 5.Finally check and close the session.
     *
     * @param organization.
     *             - organization detail.
     */
    void addOrganization(Organization organization) throws IdeaBusException;

    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get list of organizations from the Database and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session.
     * 5.Return list of organizations.
     *
     * @return contains list of organizations.
     */
    List<String> getOrganizations() throws IdeaBusException;
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get a single record of organization from organization table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw thr exception.
     * 5.Finally check and close the session.
     * 6.Return organization data.
     *
     * @param organization.
     *             - organization detail.
     */
    Organization getOrganization(String name) throws IdeaBusException;
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Update a organization data in organization table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
          * 4.Catch and throw thr exception.
     * 5.Finally check and close the session.
     *
     * @param organization
     *          - contains instance of organization.
     */
    public void updateOrganization(Organization organization)
        throws IdeaBusException;
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Delete a organization data in organization table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw thr exception.
     * 5.Finally check and close the session.
     *
     * @param organization
     *          - contains instance of organization.
     */
    public void deleteOrganization(Organization organization)
        throws IdeaBusException;
}
