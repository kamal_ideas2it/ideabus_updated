package com.ideas2it.ideabus.busStop;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 *
 * Declare variables for busStop related functions and encapsulate them.
 */
public class BusStop {

    private int id;
    private String name;

    public BusStop() {
    }
        
    public BusStop(String name) {
        this.name = name;
    }

    /**
     * Getter and Setter
     */
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
