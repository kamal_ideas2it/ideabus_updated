package com.ideas2it.ideabus.busStop.service;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;

import com.ideas2it.ideabus.busStop.BusStop;
import com.ideas2it.ideabus.exception.IdeaBusException;

/**
 * Perform logical operations on data like,
 * 1.Check if busStop exists in busStop table then add new data into the table.
 * 2.Get the list of busStops from busStop.
 * 3.Get a busStop data from busStop table using it's name.
 * 4.Update busStop data with given input.
 * 5.Delete the busStop data from busStop table.
 */
public interface BusStopService {

    /**
     * 1.Check busStop data in the busStop table 
     * 2.Add new busStop data into the busStop table.
     *
     * @param name
     *         - name of the busStop.
     * @return contains a String.
     */
    String checkAndAddBusStop(String name) throws IdeaBusException;

    /**
     * Check given busStop exists or not.
     *
     * @param name
     *          - contains name of busStop.
     *
     * @return contains boolean value.
     */
    boolean checkBusStop(String name) throws IdeaBusException;
    
    /**
     * Get list of busStops from busStop table.
     * 
     * @return contains list of busStop.
     */
    List<BusStop> getBusStops() throws IdeaBusException;
    
    /**
     * Get list of stops from busStop table.
     * 
     * @param countType
     *              - contains name of count type.
     *
     * @return contains list of stop.
     */
    List<String> getStops(String countType) throws IdeaBusException;
    
    /**
     * Get the busStop data from busStop table using it's name.
     *
     * @param name
     *              - contains name of busStop.
     */
    BusStop getBusStop(String name) throws IdeaBusException;
    
    /**
     * Update busStop data with the given input in busStop table.
     *
     * @param naem
     *          - contains name of the busStop to be updated.
     * @param input
     *          - contains input.
     */
    void updateBusStop(String name , String input) throws IdeaBusException;
    
    /**
     * Delete busStop data in busStop table.
     *
     * @param name
     *          - contains name of the busStop to be delete.
     */
    void deleteBusStop(String name) throws IdeaBusException;
}
