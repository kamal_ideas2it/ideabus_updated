package com.ideas2it.ideabus.busStop.service.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException; 

import com.ideas2it.ideabus.busStop.BusStop;
import com.ideas2it.ideabus.busStop.dao.BusStopDAO;
import com.ideas2it.ideabus.busStop.dao.impl.BusStopDAOImpl;
import com.ideas2it.ideabus.busStop.service.BusStopService;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.util.Constant;

/**
 * Perform logical operations on data like,
 * 1.Check if busStop exists in busStop table then add new data into the table.
 * 2.Get the list of busStops from busStop.
 * 3.Get a busStop data from busStop table using it's name.
 * 4.Update busStop data with given input.
 * 5.Delete the busStop data from busStop table.
 */
public class BusStopServiceImpl implements BusStopService {
    private BusStopDAO busStopDAO;
    private BusStop busStop;
    
    /**
     * 1.Check busStop data in the busStop table 
     * 2.Add new busStop data into the busStop table.
     *
     * @param name
     *         - name of the busStop.
     * @return contains a String.
     */
    public String checkAndAddBusStop(String name) throws IdeaBusException  {
        BusStop busStop;
        String[] inputs = name.split(",");
        for(String busStopName : inputs) {  
            boolean busStopExist = checkBusStop(busStopName);
            if (busStopExist == true) {
                return (Constant.ALREADY + " " + Constant.ADDED);
            } else {
                busStop = new BusStop(busStopName);
                busStopDAO.addBusStop(busStop); // add busStop
            } 
        }
        return Constant.ADDED;
    }
    
    /**
     * Check given busStop exists or not.
     *
     * @param name
     *          - contains name of busStop.
     *
     * @return contains boolean value.
     */
    public boolean checkBusStop(String name) throws IdeaBusException {
        BusStop busStop;
        busStop = getBusStop(name);
        if ( busStop == null ) {
                return false;
            }
        return true;
    }

    /**
     * Get list of busStops from busStop table.
     * 
     * @return contains list of busStops.
     */
    public List<BusStop> getBusStops() throws IdeaBusException {
       busStopDAO = new BusStopDAOImpl();
       return busStopDAO.getBusStops();
    }
        
    /**
     * Get list of stops from busStop table.
     * 
     * @param countType
     *              - contains name of count type.
     * @return contains list of stops.
     */
    public List<String> getStops(String countType) throws IdeaBusException {
       int newCount = getCount(countType);
       busStopDAO = new BusStopDAOImpl();
       return busStopDAO.getStops(newCount);
    }
    
    /**
     * Get the busStop data from busStop table using it's name.
     *
     * @param name
     *              - contains name of busStop.
     */
    public BusStop getBusStop(String name) throws IdeaBusException {
        busStopDAO = new BusStopDAOImpl(); 
        return busStopDAO.getBusStop(name);
    }
    
    /**
     * Update busStop data with the given input in busStop table.
     *
     * @param naem
     *          - contains name of the busStop to be updated.
     * @param input
     *          - contains input.
     */
    public void updateBusStop(String name , String input) throws IdeaBusException {
        busStopDAO = new BusStopDAOImpl();
        busStop = getBusStop(name);
        busStop.setName(input);
        busStopDAO.updateBusStop(busStop);
    }
        
    /**
     * Delete busStop data in busStop table.
     *
     * @param name
     *          - contains name of the busStop to be updated.
     */
    public void deleteBusStop(String name) throws IdeaBusException {
        busStopDAO = new BusStopDAOImpl();
        busStop = busStopDAO.getBusStop(name);
        busStopDAO.deleteBusStop(busStop);
    }
    
    /**
     * Perfrom calucaltion based on type of count.
     *
     * @param countType
     *              - contains type of count.
     */
    public int getCount(String countType) throws IdeaBusException {
        if (countType.equals(Constant.NEXT)) {
            return nextCount();
        } else if (Constant.PREVIOUS.equals(countType)) {
            return previousCount();
        } else {
            return newCount();
        }
    }
    
        
    /**
     * Get the initial count value.
     *
     * @return an integer value based comparision.
     */
    public int newCount() {
        return Constant.newCount();
    }
    
    /**
     * 1.Get the number of rows in a table
     * 2.Increase count by 5.
     * 3.Compare count and number of rows.
     *
     * @return an integer value based comparision.
     */
    public int nextCount() throws IdeaBusException {
        busStopDAO = new BusStopDAOImpl();
        long rowCount = busStopDAO.getRowCount();
        if (rowCount < Constant.nextCount()) {
            return Constant.newCount();
        }
        return Constant.nextCount();
    }
    
    /**
     * 1.Get the number of rows in a table
     * 2.Increase count by 5.
     * 3.Compare count and number of rows.
     *
     * @return an integer value based comparision.
     */
    public int previousCount() {
        if (Constant.previousCount() < 0) {
            return Constant.newCount();
        }
        return Constant.previousCount();
    }
}
