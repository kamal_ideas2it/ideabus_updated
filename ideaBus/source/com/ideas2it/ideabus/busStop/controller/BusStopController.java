package com.ideas2it.ideabus.busStop.controller;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import org.apache.log4j.Logger;
 
import java.util.Collections;
import java.util.List;

import com.ideas2it.ideabus.busStop.BusStop;
import com.ideas2it.ideabus.busStop.service.BusStopService;
import com.ideas2it.ideabus.busStop.service.impl.BusStopServiceImpl;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.util.Constant;

/**
 * 1.Use to control the flow of data
 * 2.Handle the exceptions.
 */
public class BusStopController {
    private static final Logger logger = Logger.getLogger
                                                      (BusStopController.class);
    private BusStopService busStopService;

    /**
     * Add busStop data into busStop table and handle it's exception.
     *
     * @param name
     *          - contains name of busStop.
     *
     * @return contains a String.
     */
    public String addBusStop(String name) {
        busStopService = new BusStopServiceImpl();
        try {
            busStopService.checkAndAddBusStop(name);
            return Constant.ADDED; 
        }catch (IdeaBusException e) {
            logger.error(e);
            return Constant.ERROR;
        }
    }
    
    /**
     * Get the list of busStops from busStops table and handle it's exception.
     *
     * @return contains list of busStops.
     */
    public List<BusStop> getBusStops() {
        busStopService = new BusStopServiceImpl();
        try {
            return busStopService.getBusStops();
        } catch (IdeaBusException e) {
            logger.error(e);
            return Collections.<BusStop>emptyList();
        }
    }
    
    /**
     * Get the list of busStops from busStops table and handle it's exception.
     *
     * @return contains list of busStops.
     */
    public List<String> getStops(String count) {
        busStopService = new BusStopServiceImpl();
        try {
            return busStopService.getStops(count);
        } catch (IdeaBusException e) {
            logger.error(e);
            return Collections.<String>emptyList();
        }
    }
    
    /**
     * Update the busStop data in busStop table and hadle it's exception.
     *
     * @param busStop
     *          - contains name of the busStop.
     * @param input
     *          - contains input.
     */
    public void updateBusStop(String busStop,String input) {
        busStopService = new BusStopServiceImpl();
        try {
            busStopService.updateBusStop(busStop,input);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
    }
        
    /**
     * Delete the busStop data in busStop table and hadle it's exception.
     *
     * @param busStop
     *          - contains name of the busStop.
     */
    public void deleteBusStop(String busStop) {
        busStopService = new BusStopServiceImpl();
        try {
            busStopService.deleteBusStop(busStop);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
    }
}
