package com.ideas2it.ideabus.busStop.dao;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;

import com.ideas2it.ideabus.busStop.BusStop;
import com.ideas2it.ideabus.exception.IdeaBusException;

/**
 * Perform manipulation operations on input.
 * 1.Add instance of busStop class int busStop table.
 * 2.Get list of busStops from busStop table and return them.
 * 2.Get single record of a busStop from busStop table and return them.
 * 3.Update existing busStop data in busStop table.
 */
public interface BusStopDAO {
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Add busStop to busStop table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw thr exception.
     * 5.Finally check and close the session.
     *
     * @param busStop.
     *             - busStop detail.
     */
    void addBusStop(BusStop busStop) throws IdeaBusException ;

    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get list of busStops from the Database and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session.
     * 5.Return list of busStops.
     *
     * @return contains list of busStops.
     */
    List<BusStop> getBusStops() throws IdeaBusException ;
        
    /**
     * 1.Get Session factory that created this session,
     *  begin the transaction.
     * 2.Get list of stops from the Database and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session.
     * 5.Return list of stops.
     *
     * @return contains list of busStops.
     */
    List<String> getStops(int count) throws IdeaBusException ;
    
    /**
     * 1.Get Session factory that created this session,
     *  and begin the transaction.
     * 2.Get the number of rows in table.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session.
     * 5.Return list of stops.
     *
     * @return contains list of busStops.
     */
    long getRowCount() throws IdeaBusException ;
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get a single record of busStop from busStop table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw the exception.
     * 5.Finally check and close the session.
     * 6.Return busStop data.
     *
     * @param busStop.
     *             - busStop detail.
     */
    BusStop getBusStop(String name) throws IdeaBusException ;
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Update a busStop data in busStop table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw the exception.
     * 5.Finally check and close the session.
     *
     * @param busStop
     *          - contains instance of busStop.
     */
    public void updateBusStop(BusStop busStop) throws IdeaBusException ;
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Delete a busStop data in busStop table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw the exception.
     * 5.Finally check and close the session.
     *
     * @param busStop
     *          - contains instance of busStop.
     */
    public void deleteBusStop(BusStop busStop) throws IdeaBusException ;
}
