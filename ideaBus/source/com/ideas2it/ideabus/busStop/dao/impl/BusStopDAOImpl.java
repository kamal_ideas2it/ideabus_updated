package com.ideas2it.ideabus.busStop.dao.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria; 
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Restrictions;

import com.ideas2it.ideabus.busStop.BusStop;
import com.ideas2it.ideabus.busStop.dao.BusStopDAO;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.util.Constant;
import com.ideas2it.ideabus.util.Factory;

/**
 * Perform manipulation operations on input.
 * 1.Add busStop data into busStop table.
 * 2.Get list of busStops from busStop table and return them.
 * 2.Get single record of a busStop from busStop table and return them.
 * 3.Update existing busStop data in busStop table.
 * 4.Delere existing busStop data in busStop table.
 */
public class BusStopDAOImpl implements BusStopDAO {
    private Session session;
    private Transaction transaction;
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Add busStop to busStop table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw the exception.
     * 5.Finally check and close the session.
     *
     * @param busStop.
     *             - busStop detail.
     */
    public void addBusStop(BusStop busStop) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.save(busStop);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_STOP_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get list of busStops from the Database and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session.
     * 5.Return list of busStops.
     *
     * @return contains list of busStops.
     */
    public List<BusStop> getBusStops() throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        List<BusStop> busStops = new ArrayList<>();
        try {
            Criteria criteria = session.createCriteria(BusStop.class);
            criteria.setFirstResult(0);
            criteria.setMaxResults(5);
            busStops = criteria.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_STOP_002 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return busStops;
    }
    
    /**
     * 1.Get Session factory that created this session,
     *  begin the transaction.
     * 2.Get list of stops from the Database and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session.
     * 5.Return list of stops.
     *
     * @return contains list of stops.
     */
    public List<String> getStops(int count) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        List<String> stops = new ArrayList<>();
        try {
            Criteria criteria = session.createCriteria(BusStop.class);
            ProjectionList projectionList = Projections.projectionList();
            projectionList.add(Projections.property(Constant.NAME));
            criteria.setProjection(projectionList);
            criteria.setFirstResult(count);
            criteria.setMaxResults(6);
            stops = criteria.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_STOP_002 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return stops;
    }

    /**
     * 1.Get Session factory that created this session,
     *  and begin the transaction.
     * 2.Get the total number of rows in table.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session.
     * 5.Return list of stops.
     *
     * @return contains list of busStops.
     */
    public long getRowCount() throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        long rowCount = 0;
        try {
            Criteria criteria = session.createCriteria(BusStop.class);
            criteria.setProjection(Projections.rowCount());
            rowCount = (Long)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_STOP_002 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return rowCount;
    }
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get a single record of busStop from busStop table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw the exception.
     * 5.Finally check and close the session.
     * 6.Return busStop data.
     *
     * @param busStop.
     *             - busStop detail.
     */
    public BusStop getBusStop(String name) throws IdeaBusException {
        BusStop busStop = new BusStop();
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            Criteria criteria = session.createCriteria(BusStop.class);
            criteria.add(Restrictions.eq(Constant.NAME,name));
            busStop = (BusStop)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_STOP_002 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return busStop;
    }
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Update a busStop data in busStop table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw the exception.
     * 5.Finally check and close the session.
     *
     * @param busStop
     *          - contains instance of busStop.
     */
    public void updateBusStop(BusStop busStop) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.update(busStop);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_STOP_003 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Delete a busStop data in busStop table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw the exception.
     * 5.Finally check and close the session.
     *
     * @param busStop
     *          - contains instance of busStop.
     */
    public void deleteBusStop(BusStop busStop) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.delete(busStop);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_STOP_004 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
