package com.ideas2it.ideabus.booking.controller;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;
import java.util.LinkedList;

import org.apache.log4j.Logger;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.booking.service.impl.BookingServiceImpl;
import com.ideas2it.ideabus.booking.service.BookingService;
import com.ideas2it.ideabus.ticket.Ticket;
import com.ideas2it.ideabus.util.Constant;

/**
 * Manipulation operation of Booking ticket.
 * By Adding, Removing, Getting, Updating.
 */
public class BookingController {
	private static Logger logger = Logger.getLogger(BookingController.class);
    private BookingService bookingService;

    /**
     * Pass inputs from servlet to service class. <br>
     * Details like numberOfPassenger, firstName, lastName, gender, <br> 
     * seatName, age, phoneNumber, emailId, date.
     * @param numberOfPassenger
     *         - Total number of passenger for booking.
     * @param firstName
     *         - first name of each passenger in the array.
     * @param lastName
     *         - last name of each passenger in the array.
     * @param gender
     *         - gender of each passenger in the array.
     * @param age
     *         - age of each passenger in the array.
     * @param phoneNumber
     *         - PhoneNumber of the passenger.
     * @param emailId
     *         - emailId of the passenger.
     * @param date
     *         - Date of booking.
     * @param seatName
     *         - Seat name of the passengers.
     * @param userEmailId
     *         - emailId of the user.
     * @param cardNumber
     *         - User debit/credit card number.
     * @param cardName
     *         - User debit/credit card name.
     * @param totalAmount
     *         - Total amount of the ticket.
     * @param journeyDate
     *         - Date of Journey.
     */
    public void addBooking(int numberOfPassenger, String[] firstName, 
                               String[] lastName, String[] gender, int[] age,
                               String phoneNumber, String emailId, String date,
                               String[] seats, String userEmailId, 
                               String cardNumber, String cardName, 
                               float totalAmount, String journeyDate, 
                               String regNumber) {
        try {
		    bookingService = new BookingServiceImpl();
            bookingService.addBooking(numberOfPassenger, firstName, lastName, 
                                         gender, age, phoneNumber,emailId, date, 
                                         seats, userEmailId, cardNumber, 
                                         cardName, totalAmount, journeyDate, 
                                         regNumber);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
    }

    /**
     * Pass inputs from servlet to service class. <br>
     * @param emailId
     *         - emailId of the user.
     * @return list of booked tickets by the user.
     */
    public List<Ticket> showBookingTicket(String emailId) {
        List<Ticket> tickets = new LinkedList<>();;;
        try {
		    bookingService = new BookingServiceImpl();
            tickets = bookingService.showBookingTicket(emailId);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
        return tickets;
    }

    /**
     * Pass inputs from servlet to service class. <br>
     * @param ticketNumber
     *         - Serial Number of the ticket.
     */
    public void cancelBookedTicket(String ticketNumber) {
        try {
		    bookingService = new BookingServiceImpl();
            bookingService.cancelBookedTicket(ticketNumber);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
    } 
}
