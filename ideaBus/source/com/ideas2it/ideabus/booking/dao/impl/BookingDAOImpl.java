package com.ideas2it.ideabus.booking.dao.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Criteria; 
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;

import com.ideas2it.ideabus.booking.Booking;
import com.ideas2it.ideabus.booking.dao.BookingDAO;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.user.User;
import com.ideas2it.ideabus.util.Constant;
import com.ideas2it.ideabus.util.Factory;

/**
 * Manipulation operation of Booking.
 * By Adding, Removing, Getting, Updating.
 * Using Linked List for fast manipulation.
 */
public class BookingDAOImpl implements BookingDAO {
    private Session session;
    private Transaction transaction;

    /**
     * Add Booking Detail.
     * @param booking.
     *             - booking detail.
     */
    public void addBooking(Booking booking) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.save(booking);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Get All Booking detail from the Database and return to Booking Service.
     * @return bookings.
     *             - All booking detail in the list.
     */
    public List<Booking> getBooking() throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        List<Booking> bookings = new LinkedList<>();
        try {
            transaction = session.beginTransaction();
            bookings = session.createQuery("FROM Booking").list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return bookings;
    }

    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get a single record of user from user table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session
     * 5.Return user data.
     *
     * @param user.
     *             - user detail.
     */
    public List<Booking> getBookingTickets(User user) throws IdeaBusException {
        List<Booking> bookings = new LinkedList<>();
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            Criteria criteria = session.createCriteria(Booking.class);
            criteria.add(Restrictions.eq("user", user));
            bookings = criteria.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return bookings;
    }
}
