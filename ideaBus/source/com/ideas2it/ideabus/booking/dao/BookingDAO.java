package com.ideas2it.ideabus.booking.dao;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import com.ideas2it.ideabus.booking.Booking;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.user.User;

import java.util.List;

/**
 * Manipulation operation of Booking.
 * By Adding, Removing, Getting, Updating.
 * Using Linked List for fast manipulation.
 */
public interface BookingDAO {

    /**
     * Add Booking Detail.
     * @param booking.
     *             - booking detail.
     */
    void addBooking(Booking booking) throws IdeaBusException;

    /**
     * Get All Booking detail from the Database and return to Booking Service.
     * @return bookings.
     *             - All booking detail in the list.
     */
    List<Booking> getBooking() throws IdeaBusException;

    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get a single record of user from user table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session
     * 5.Return user data.
     *
     * @param user.
     *             - user detail.
     */
    List<Booking> getBookingTickets(User user) throws IdeaBusException;
}
