package com.ideas2it.ideabus.booking;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.Set;

import com.ideas2it.ideabus.passenger.Passenger;
import com.ideas2it.ideabus.user.User;

public class Booking {
    private int id;
    private int numberOfPassenger;
    private String emailId;
    private String phone;
    private String date;
    private String status;
    private User user;
    private Set<Passenger> passengers;

    public Booking() {
    }

    public Booking(int numberOfPassenger, String emailId, String phone, 
                       String date, String status, User user,
                       Set<Passenger> passengers) {
        this.numberOfPassenger = numberOfPassenger;
        this.emailId = emailId;
        this.phone = phone;
        this.date = date;
        this.status = status;
        this.user = user;
        this.passengers = passengers;
    }

    /**
     * Initializing Getter and Setter
     */
    public void setId(int id) {
        this.id = id;
    } 

    public int getId() {
        return id;
    }

    public void setNumberOfPassenger(int numberOfPassenger) {
        this.numberOfPassenger = numberOfPassenger;
    }

    public int getNumberOfPassenger() {
        return numberOfPassenger;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    
    public User getUser() {
        return user;
    }

    public void setPassengers(Set<Passenger> passengers) {
        this.passengers = passengers;
    }
    
    public Set<Passenger> getPassengers() {
        return passengers;
    }
}
