package com.ideas2it.ideabus.booking.service;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;
import java.util.Set;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.passenger.Passenger;
import com.ideas2it.ideabus.ticket.Ticket;

/**
 * Manipulation operation of Booking Details.
 * By Adding, Removing, Getting, Updating.
 */
public interface BookingService {

    /**
     * For Add the Booking detail to passing the variable to DAO. <br>
     * Details like numberOfPassenger, passenger, phoneNumber, emailId,<br>
     * date, Seat.
     * @param numberOfPassenger
     *         - Total number of passenger booking.
     * @param firstName
     *         - first name of the passenger.
     * @param lastName
     *         - last name of the passenger.
     * @param gender
     *         - Gender of the passenger.
     * @param age
     *         - age of the passenger.
     * @param phoneNumber
     *         - phone number of the passenger.
     * @param emailId
     *         - emailId of the passenger.
     * @param date
     *         - date of booking.
     * @param seat
     *         - name of the seat.
     * @param userEmailId
     *         - EmailId of the user.
     */
    void addBooking(int numberOfPassenger, String[] firstName, 
                               String[] lastName, String[] gender, int[] age,
                               String phoneNumber, String emailId, String date,
                               String[] seats, String userEmailId,
                               String cardNumber, String cardName, 
                               float totalAmount, String journeyDate,
                               String regNumber) throws IdeaBusException;

    /**
     * Add passenger information by splitting all collected array <br>
     * information into single passenger with respected seat.
     * @param firstName
     *         - first name of the passenger.
     * @param lastName
     *         - last name of the passenger.
     * @param gender
     *         - Gender of the passenger.
     * @param age
     *         - age of the passenger.
     * @param seat
     *         - name of the seat.
     * @param booking
     *         - booking id of the passenger.
     */
    Set<Passenger> addPassenger(String[] firstName, String[] lastName, 
                                 String[] gender, String status, int[] age, 
                                 String[] seats, int numberOfPassenger)
                                 throws IdeaBusException;

    /**
     * Show the list of booked ticket by the user for cancel the ticket. <br>
     * @param emailId
     *         - emailId of the user.
     */
    List<Ticket> showBookingTicket(String emailId) throws IdeaBusException;

    /**
     * Cancel the booked ticket by the user. <br>
     * @param ticketNumber
     *         - Serial Number of the ticket.
     */
    void cancelBookedTicket(String ticketNumber) throws IdeaBusException;
}
