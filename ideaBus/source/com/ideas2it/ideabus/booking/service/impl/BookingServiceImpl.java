package com.ideas2it.ideabus.booking.service.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.lang.NullPointerException; 
import java.lang.Math;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import com.ideas2it.ideabus.booking.Booking;
import com.ideas2it.ideabus.booking.dao.BookingDAO;
import com.ideas2it.ideabus.booking.dao.impl.BookingDAOImpl;
import com.ideas2it.ideabus.booking.service.BookingService;
import com.ideas2it.ideabus.busDetail.BusDetail;
import com.ideas2it.ideabus.busDetail.service.BusDetailService;
import com.ideas2it.ideabus.busDetail.service.impl.BusDetailServiceImpl;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.passenger.Passenger;
import com.ideas2it.ideabus.passenger.service.impl.PassengerServiceImpl;
import com.ideas2it.ideabus.passenger.service.PassengerService;
import com.ideas2it.ideabus.seat.Seat;
import com.ideas2it.ideabus.seat.service.impl.SeatServiceImpl;
import com.ideas2it.ideabus.seat.service.SeatService;
import com.ideas2it.ideabus.ticket.Ticket;
import com.ideas2it.ideabus.ticket.service.impl.TicketServiceImpl;
import com.ideas2it.ideabus.ticket.service.TicketService;
import com.ideas2it.ideabus.payment.Payment;
import com.ideas2it.ideabus.user.service.impl.UserServiceImpl;
import com.ideas2it.ideabus.user.service.UserService;
import com.ideas2it.ideabus.user.User;
import com.ideas2it.ideabus.util.Constant;

/**
 * Manipulation operation of Booking Details.
 * By Adding, Removing, Getting, Updating.
 */
public class BookingServiceImpl implements BookingService {
    private BookingDAO bookingDAO;
    private TicketService ticketService;

    /**
     * For Add the Booking detail to passing the variable to DAO. <br>
     * Details like numberOfPassenger, passenger, phoneNumber, emailId,<br>
     * date, Seat.
     * @param numberOfPassenger
     *         - Total number of passenger booking.
     * @param firstName
     *         - first name of the passenger.
     * @param lastName
     *         - last name of the passenger.
     * @param gender
     *         - Gender of the passenger.
     * @param age
     *         - age of the passenger.
     * @param phoneNumber
     *         - phone number of the passenger.
     * @param emailId
     *         - emailId of the passenger.
     * @param date
     *         - date of booking.
     * @param seat
     *         - name of the seat.
     * @param userEmailId
     *         - EmailId of the user.
     */
    public void addBooking(int numberOfPassenger, String[] firstName, 
                               String[] lastName, String[] gender, int[] age,
                               String phoneNumber, String emailId, String date,
                               String[] seats, String userEmailId, 
                               String cardNumber, String cardName, 
                               float totalAmount, String journeyDate, 
                               String regNumber) 
                                            throws IdeaBusException {
        String status = Constant.ACTIVE;
        double numberOne = Math.random() * Constant.TICKET_SERIAL;
        int serialNumber = (int)numberOne;
        String ticketNumber = (date + serialNumber);
        UserService userService = new UserServiceImpl();
        User user = userService.getUserByEmailId(userEmailId);
        BusDetailService busDetailService = new BusDetailServiceImpl();
        BusDetail busDetail = busDetailService.getBusDetail(regNumber);
        Payment payment = new Payment(cardName, cardNumber, date, 
                                          totalAmount, status, user);
        Set<Passenger> passengers = addPassenger(firstName, lastName, gender, 
                                                    status, age, seats, 
                                                    numberOfPassenger);
        Booking booking = new Booking(numberOfPassenger, emailId, phoneNumber, 
                                          date, status, user, passengers);
        ticketService = new TicketServiceImpl(); 
        ticketService.addTicket(date, journeyDate, totalAmount, status,
                                   busDetail, booking, payment, ticketNumber);
    }


    /**
     * Add passenger information by splitting all collected array <br>
     * information into single passenger with respected seat.
     * @param firstName
     *         - first name of the passenger.
     * @param lastName
     *         - last name of the passenger.
     * @param gender
     *         - Gender of the passenger.
     * @param age
     *         - age of the passenger.
     * @param seat
     *         - name of the seat.
     * @param booking
     *         - booking id of the passenger.
     */
    public Set<Passenger> addPassenger(String[] firstName, String[] lastName, 
                                 String[] gender, String status, int[] age, 
                                 String[] seats, int numberOfPassenger) 
                                                     throws IdeaBusException {
        Set<Passenger> passengers = new HashSet<>();
        String seatName = "";
        String fName = "";
        String lName = ""; 
        String passengerGender = ""; 
        int passengerAge = 0;
        SeatService seatService = new SeatServiceImpl();
        Seat newSeat;
        for (int index = 0; index < numberOfPassenger; index++) {
            fName = firstName[index];
            lName = lastName[index];
            passengerGender = gender[index];
            passengerAge = age[index];
            seatName = seats[index];
            newSeat = seatService.getSeat(seatName);
            Passenger passenger = new Passenger(fName, lName, passengerGender,
                                                     passengerAge, newSeat);
            passengers.add(passenger);
        }
        return passengers;
    }

    /**
     * Show the list of booked ticket by the user for cancel the ticket. <br>
     * @param emailId
     *         - emailId of the user.
     * @return tickets booked by the user.
     */
    public List<Ticket> showBookingTicket(String emailId) 
                                                    throws IdeaBusException {
        UserService userService = new UserServiceImpl();
        User user = userService.getUserByEmailId(emailId);
        List<Booking> bookings = bookingDAO.getBookingTickets(user);
        ticketService = new TicketServiceImpl();
        List<Ticket> tickets = new LinkedList<>();
        Ticket newTicket;
        List<Ticket> newTickets = new LinkedList<>();
        for (Booking booking : bookings) {
            newTicket = ticketService.getTicket(booking);
            newTickets.add(newTicket);
        }
        Iterator<Ticket> iterator = newTickets.iterator();
        while (iterator.hasNext()) {
            Ticket ticket = iterator.next();
            if(ticket.getStatus().equals(Constant.ACTIVE)) {
                tickets.add(ticket);
            }
        }
        return tickets;
    }

    /**
     * Cancel the booked ticket by the user. <br>
     * @param ticketNumber
     *         - Serial Number of the ticket.
     */
    public void cancelBookedTicket(String ticketNumber) 
                                                    throws IdeaBusException {
        ticketService = new TicketServiceImpl();
        ticketService.cancelBookedTicket(ticketNumber);
    }
}
