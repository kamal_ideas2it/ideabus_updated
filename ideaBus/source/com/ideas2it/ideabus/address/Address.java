package address;

import user.User;

public class Address {

    private int id;
    private int doorNumber;
    private String street;
    private String area;
    private String city;
    private String state;
    private String country;
    private int pincode;
    private User user;

    public Address() {
    }

    public Address(int doorNumber, String street, String area, String city, 
                      String state, String country, int pincode) {
        this.doorNumber = doorNumber;
        this.street = street;
        this.area = area;
        this.city = city;
        this.state = state;
        this.country = country;
        this.pincode = pincode;
    }

    /**
     * Initializing Getter and Setter
     */
    public void setId(int id) {
        this.id = id;
    } 

    public int getId() {
        return id;
    }

    public void setDoorNumber(int doorNumber) {
        this.doorNumber = doorNumber;
    }

    public int getDoorNumber() {
        return doorNumber;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreet() {
        return street;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getArea() {
        return area;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setPincode(int pincode) {
        this.pincode = pincode;
    }

    public int getPincode() {
        return pincode;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    
    public User getUser() {
        return user;
    }
}
