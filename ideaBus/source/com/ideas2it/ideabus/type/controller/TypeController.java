package com.ideas2it.ideabus.type.controller;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.List;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.type.Type;
import com.ideas2it.ideabus.type.service.TypeService;
import com.ideas2it.ideabus.type.service.impl.TypeServiceImpl;
import com.ideas2it.ideabus.util.Constant;

/**
 * 1.Use to control the flow of data
 * 2.Handle the exceptions.
 */
public class TypeController {
    private static final Logger logger = Logger.getLogger(TypeController.class);
    private TypeService typeService;
    
    /**
     * Add type date into type table and handle it's exception.
     *
     * @param name
     *          - contains name of type.
     *
     * @return contains a String.
     */
    public String addType(String name) {
        typeService = new TypeServiceImpl();
        try {
            return typeService.checkAndAddType(name);
        } catch (IdeaBusException e) {
            logger.error(e);
            return Constant.ERROR;
        }
    }
        
    /**
     * Get the list of types from types table and handle it's exception.
     *
     * @return contains list of types.
     */
    public List<String> getTypes() {
        typeService = new TypeServiceImpl();
        try {
            return typeService.getTypes();
        } catch (IdeaBusException e) {
            logger.error(e);
            return Collections.<String>emptyList();
        }
    }
    
    /**
     * Update the type data in type table and hadle it's exception.
     *
     * @param type
     *          - contains name of the type.
     * @param input
     *          - contains input.
     */
    public void updateType(String type,String input) {
        typeService = new TypeServiceImpl();
        try {
            typeService.updateType(type,input);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
    }
        
    /**
     * Delete the type data in type table and hadle it's exception.
     *
     * @param type
     *          - contains name of the type.
     */
    public void deleteType(String type) {
        typeService = new TypeServiceImpl();
        try {
            typeService.deleteType(type);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
    }
}
