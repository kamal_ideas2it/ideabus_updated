package com.ideas2it.ideabus.type;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 *
 * Model class of Type.
 */
public class Type {
    private int id;
    private String name;

    public Type() {
    }
    
    public Type(String name) {
        this.name = name;
    }

    /**
     * Initializing Getter and Setter
     */
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
