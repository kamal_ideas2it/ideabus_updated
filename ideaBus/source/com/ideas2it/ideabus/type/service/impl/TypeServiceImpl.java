package com.ideas2it.ideabus.type.service.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.Iterator;
import java.util.List;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.type.Type;
import com.ideas2it.ideabus.type.dao.TypeDAO;
import com.ideas2it.ideabus.type.dao.impl.TypeDAOImpl;
import com.ideas2it.ideabus.type.service.TypeService;
import com.ideas2it.ideabus.util.Constant;

/**
 * Perform logical operations on data like,
 * 1.Check if type exists in type table then add new data into the table.
 * 2.Get the list of types from type.
 * 3.Get a type data from type table using it's name.
 * 4.Update type data with given input.
 * 5.Delete the type data from type table.
 */
public class TypeServiceImpl implements TypeService {
    private TypeDAO typeDAO;
    
    /**
     * 1.Check type data in the type table 
     * 2.Add new type data into the type table.
     *
     * @param name
     *         - name of the type.
     * @return contains a String.
     */
    public String checkAndAddType(String name) throws IdeaBusException {
        boolean typeExists = checkType(name);
        if ( true == typeExists ) {
            return (Constant.ALREADY + " " + Constant.ADDED);
        }
        typeDAO = new TypeDAOImpl();
        Type type = new Type(name);
        typeDAO.addType(type);
        return Constant.ADDED;
    }
    
    /**
     * Check given type exists or not.
     *
     * @param name
     *          - contains name of type.
     *
     * @return contains boolean value.
     */
    public boolean checkType(String name) throws IdeaBusException {
        Type type = getType(name);
        if ( null == type ) {
                return false;
            }
        return true;
    }

    /**
     * Get list of types from type table.
     * 
     * @return contains list of type.
     */
    public List<String> getTypes() throws IdeaBusException {
       typeDAO = new TypeDAOImpl();
       return typeDAO.getTypes();
    }
       
    /**
     * Get the type data from type table using it's name.
     *
     * @param name
     *              - contains name of type.
     */
    public Type getType(String name) throws IdeaBusException {
        typeDAO = new TypeDAOImpl(); 
        return typeDAO.getType(name);
    }
    
    /**
     * Update type data with the given input in type table.
     *
     * @param naem
     *          - contains name of the type to be updated.
     * @param input
     *          - contains input.
     */
    public void updateType(String name , String input) throws IdeaBusException {
        typeDAO = new TypeDAOImpl();
        Type type = getType(name);
        type.setName(input);
        typeDAO.updateType(type);
    }
        
    /**
     * Delete type data in type table.
     *
     * @param name
     *          - contains name of the type to be updated.
     */
    public void deleteType(String name) throws IdeaBusException {
        typeDAO = new TypeDAOImpl();
        Type type = typeDAO.getType(name);
        typeDAO.deleteType(type);
    }
}
