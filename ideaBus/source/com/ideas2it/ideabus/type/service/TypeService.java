package com.ideas2it.ideabus.type.service;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */ 
import java.util.List;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.type.Type;

/**
 * Perform logical operations on data like,
 * 1.Check if type exists in type table then add new data into the table.
 * 2.Get the list of types from type.
 * 3.Get a type data from type table using it's name.
 * 4.Update type data with given input.
 * 5.Delete the type data from type table.
 */
public interface TypeService {

    /**
     * 1.Check type data in the type table 
     * 2.Add new type data into the type table.
     *
     * @param name
     *         - name of the type.
     * @return contains a String.
     */
    String checkAndAddType(String name) throws IdeaBusException ;

    /**
     * Check given type exists or not.
     *
     * @param name
     *          - contains name of type.
     *
     * @return contains boolean value.
     */
    boolean checkType(String name) throws IdeaBusException ;
    
    /**
     * Get list of types from type table.
     * 
     * @return contains list of type.
     */
    List<String> getTypes() throws IdeaBusException ;
    
    /**
     * Get the type data from type table using it's name.
     *
     * @param name
     *              - contains name of type.
     */
    Type getType(String name) throws IdeaBusException ;
    
    /**
     * Update type data with the given input in type table.
     *
     * @param naem
     *          - contains name of the type to be updated.
     * @param input
     *          - contains input.
     */
    void updateType(String name , String input) throws IdeaBusException ;
    
    /**
     * Delete type data in type table.
     *
     * @param name
     *          - contains name of the type to be delete.
     */
    void deleteType(String name) throws IdeaBusException ;
}
