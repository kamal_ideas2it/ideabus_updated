package com.ideas2it.ideabus.type.dao;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.type.Type;

/**
 * Perform manipulation operations on input.
 * 1.Add instance of type class int type table.
 * 2.Get list of types from type table and return them.
 * 2.Get single record of a type from type table and return them.
 * 3.Update existing type data in type table.
 */
public interface TypeDAO {
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Add type to type table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw thr exception.
     * 5.Finally check and close the session.
     *
     * @param type.
     *             - type detail.
     */
    void addType(Type type) throws IdeaBusException;

    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get list of types from the Database and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session.
     * 5.Return list of types.
     *
     * @return contains list of types.
     */
    List<String> getTypes() throws IdeaBusException;
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get a single record of type from type table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw thr exception.
     * 5.Finally check and close the session.
     * 6.Return type data.
     *
     * @param type.
     *             - type detail.
     */
    Type getType(String name) throws IdeaBusException;
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Update a type data in type table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
          * 4.Catch and throw thr exception.
     * 5.Finally check and close the session.
     *
     * @param type
     *          - contains instance of type.
     */
    public void updateType(Type type) throws IdeaBusException;
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Delete a type data in type table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw thr exception.
     * 5.Finally check and close the session.
     *
     * @param type
     *          - contains instance of type.
     */
    public void deleteType(Type type) throws IdeaBusException;
}
