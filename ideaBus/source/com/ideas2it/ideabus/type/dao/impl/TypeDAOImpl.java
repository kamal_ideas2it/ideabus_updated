package com.ideas2it.ideabus.type.dao.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria; 
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Restrictions;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.type.Type;
import com.ideas2it.ideabus.type.dao.TypeDAO;
import com.ideas2it.ideabus.util.Constant;
import com.ideas2it.ideabus.util.Factory;

/**
 * Perform manipulation operations on input.
 * 1.Add type data into type table.
 * 2.Get list of types from type table and return them.
 * 2.Get single record of a type from type table and return them.
 * 3.Update existing type data in type table.
 * 4.Delere existing type data in type table.
 */
public class TypeDAOImpl implements TypeDAO {
    
    private Session session;
    private Transaction transaction;
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Add type to type table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw the exception.
     * 5.Finally check and close the session.
     *
     * @param type.
     *             - type detail.
     */
    public void addType(Type type) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.save(type);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_TYPE_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get list of types from the Database and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Finally check and close the session.
     * 5.Return list of types.
     *
     * @return contains list of types.
     */
    public List<String> getTypes() throws IdeaBusException {
        Session session = Factory.getSession();
        Transaction transaction = session.beginTransaction();
        List<String> types = new ArrayList<>();
        try {
            Criteria criteria = session.createCriteria(Type.class);
            ProjectionList projectionList = Projections.projectionList();
            projectionList.add(Projections.property(Constant.NAME));
            criteria.setProjection(projectionList);
            types = criteria.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_TYPE_002 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return types;
    }
        
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Get a single record of type from type table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw the exception.
     * 5.Finally check and close the session.
     * 6.Return type data.
     *
     * @param type.
     *             - type detail.
     */
    public Type getType(String name) throws IdeaBusException {
        Type type = new Type();
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            Criteria criteria = session.createCriteria(Type.class);
            criteria.add(Restrictions.eq(Constant.NAME,name));
            type = (Type)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_TYPE_002 + e);
        } finally {
            if (session != null){
                session.close();
            }
        }
        return type;
    }
    
        
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Update a type data in type table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
          * 4.Catch and throw thr exception.
     * 5.Finally check and close the session.
     *
     * @param type
     *          - contains instance of type.
     */
    public void updateType(Type type) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.update(type);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_TYPE_003 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Delete a type data in type table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw thr exception.
     * 5.Finally check and close the session.
     *
     * @param type
     *          - contains instance of type.
     */
    public void deleteType(Type type) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.delete(type);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_TYPE_004 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
