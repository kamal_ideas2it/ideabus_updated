package com.ideas2it.ideabus.passenger;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import com.ideas2it.ideabus.seat.Seat;

public class Passenger {

    private int id;
    private String firstName;
    private String lastName;
    private String gender;
    private int age;
    private Seat seat;
    
    public Passenger() {
    }

    public Passenger(String firstName,String lastName, String gender, int age, 
                         Seat seat) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;
        this.seat = seat;
    }

    /**
     * Initializing Getter and Setter
     */
    public void setId(int id) {
        this.id = id;
    } 

    public int getId() {
        return id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setSeat(Seat seat) {
        this.seat = seat;
    }

    public Seat getSeat() {
        return seat;
    }
}
