package com.ideas2it.ideabus.passenger.dao.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.LinkedList;
import java.util.List;

import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.passenger.dao.PassengerDAO;
import com.ideas2it.ideabus.passenger.Passenger;
import com.ideas2it.ideabus.util.Constant;
import com.ideas2it.ideabus.util.Factory;

/**
 * Manipulation operation of Passenger.
 * By Adding, Removing, Getting, Updating.
 * Using Linked List for fast manipulation.
 */
public class PassengerDAOImpl implements PassengerDAO {
    private Session session;
    private Transaction transaction;

    /**
     * Add Passenger Detail.
     * @param passenger.
     *             - Passenger detail.
     */
    public void addPassenger(Passenger passenger) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.save(passenger);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Get All Booking detail from the Database and return to Booking Service.
     * @return bookings.
     *             - All booking detail in the list.
     */
    public List<Passenger> getPassenger() throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        List<Passenger> passengers = new LinkedList<>();
        try {
            transaction = session.beginTransaction();
            passengers = session.createQuery("FROM Passenger").list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_BUS_DETAIL_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return passengers;
    }
}
