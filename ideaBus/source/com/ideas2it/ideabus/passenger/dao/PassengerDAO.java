package com.ideas2it.ideabus.passenger.dao;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;

import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.passenger.Passenger;

/**
 * Manipulation operation of Passenger.
 * By Adding, Removing, Getting, Updating.
 * Using Linked List for fast manipulation.
 */
public interface PassengerDAO {

    /**
     * Add Passenger Detail.
     * @param passenger.
     *             - Passenger detail.
     */
    void addPassenger(Passenger passenger) throws IdeaBusException;

    /**
     * Get All Booking detail from the Database and return to Booking Service.
     * @return bookings.
     *             - All booking detail in the list.
     */
    List<Passenger> getPassenger() throws IdeaBusException;
}
