package com.ideas2it.ideabus.passenger.service;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import com.ideas2it.ideabus.booking.Booking;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.seat.Seat;

/**
 * Manipulation operation of Passenger Details.
 * By Adding, Removing, Getting, Updating.
 */
public interface PassengerService {

    /**
     * For Add the Passenger detail to passing the variable to DAO. <br>
     * Details like busName, registerNumber, busStopName, type, seatName, price.
     * @param firstName
     *         - First name of the passenger.
     * @param lastName
     *         - last name of the passenger.
     * @param gender
     *         - gender of the passenger.
     * @param age
     *         - age of the pasenger.
     * @param booking
     *         - booking id for the passenger.
     * @param seat
     *         - seat name of respective passenger..
     */
    public void addPassenger(String firstName, String lastName, String gender,
                                 int age, Seat seat) throws IdeaBusException;
}
