package com.ideas2it.ideabus.passenger.service.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import com.ideas2it.ideabus.booking.Booking;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.passenger.dao.impl.PassengerDAOImpl;
import com.ideas2it.ideabus.passenger.dao.PassengerDAO;
import com.ideas2it.ideabus.passenger.Passenger;
import com.ideas2it.ideabus.passenger.service.PassengerService;
import com.ideas2it.ideabus.seat.Seat;

/**
 * Manipulation operation of Passenger Details.
 * By Adding, Removing, Getting, Updating.
 */
public class PassengerServiceImpl implements PassengerService {
    private PassengerDAO passengerDAO;

    /**
     * For Add the Passenger detail to passing the variable to DAO. <br>
     * Details like busName, registerNumber, busStopName, type, seatName, price.
     * @param firstName
     *         - First name of the passenger.
     * @param lastName
     *         - last name of the passenger.
     * @param gender
     *         - gender of the passenger.
     * @param age
     *         - age of the pasenger.
     * @param booking
     *         - booking id for the passenger.
     * @param seat
     *         - seat name of respective passenger..
     */
    public void addPassenger(String firstName, String lastName, String gender,
                                 int age, Seat seat)
                                                      throws IdeaBusException {
        Passenger passenger = new Passenger(firstName, lastName, gender, age, 
                                                seat);
        passengerDAO = new PassengerDAOImpl();
        passengerDAO.addPassenger(passenger);
    }
}
