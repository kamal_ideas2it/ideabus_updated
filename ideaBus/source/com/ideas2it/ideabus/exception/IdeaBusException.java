package com.ideas2it.ideabus.exception;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 *
 * IdeaBusException extends Exception to create custom exception.
 */
public class IdeaBusException extends Exception{
    
    public IdeaBusException(String e) {
        super(e);
    }
}
