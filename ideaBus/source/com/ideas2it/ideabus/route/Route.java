package com.ideas2it.ideabus.route;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.HashSet;
import java.util.Set;

import com.ideas2it.ideabus.busStop.BusStop;

/**
 * Declare variables for route related functions and encapsulate them.
 */
public class Route {
    private int id;
    private String name;
    private float boardingTime;
    private float droppingTime;
    private Set<BusStop> busStops = new HashSet<>(0);

    public Route() {}
    
    public Route(String name, Set<BusStop> busStops) {
        this.name = name;
        this.boardingTime = boardingTime;
        this.droppingTime = droppingTime;
        this.busStops = busStops;
    }
             
    /**
     * Getter and Setter
     */       
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBoardingTime(float boardingTime) {
        this.boardingTime = boardingTime;
    }

    public void setDroppingTime(float droppingTime) {
        this.droppingTime = droppingTime;
    }


    public String getName() {
        return name;
    }

    public float getBoardingTime() {
        return boardingTime;
    }

    public float getDroppingTime() {
        return droppingTime;
    }

    
    public void setBusStops(Set<BusStop> busStops) {
        this.busStops = busStops;
    }

    public Set<BusStop> getBusStops() {
        return busStops;
    }
}
