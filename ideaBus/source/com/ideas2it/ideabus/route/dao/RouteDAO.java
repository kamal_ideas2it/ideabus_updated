package com.ideas2it.ideabus.route.dao;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;

import com.ideas2it.ideabus.busStop.BusStop;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.route.Route;

public interface RouteDAO {

    /**
     * Add route to route table.
     * @param route
     *          - contains instance of Route class.
     */
    void addRoute(Route route) throws IdeaBusException;
    
    /**
     * Get All Route detail from the Database and return to Route Service.
     * @return routes.
     *             - All route detail in the list.
     */
    List<String> getRoutes() throws IdeaBusException;
    
    /**
     * Get list of stops for the given route.
     *
     * @param routeName
     *             - contains name of the route.
     *
     * @return route.
     *             - route detail in the list.
     */
    List<String> getStops(String routeName) throws IdeaBusException;
    
    /**
     * Get Route detail from the Database and return to Route Service.
     * @return route.
     *             - route detail in the list.
     */
    Route getRouteByName(String routeName) throws IdeaBusException;

    /**
     * Get List of routes from route table and return them.
     * @return a list of routes.
     */
    List<Route> getRoutes(Integer[] busStopIds) throws IdeaBusException;
    
        
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Update a route data in route table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw the exception.
     * 5.Finally check and close the session.
     *
     * @param route
     *          - contains instance of route.
     */
    public void updateRoute(Route route) throws IdeaBusException;
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Delete a route data in route table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw thr exception.
     * 5.Finally check and close the session.
     *
     * @param route
     *          - contains instance of route.
     */
    public void deleteRoute(Route route) throws IdeaBusException;
}
