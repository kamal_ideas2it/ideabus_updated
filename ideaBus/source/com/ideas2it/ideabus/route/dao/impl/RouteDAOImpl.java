package com.ideas2it.ideabus.route.dao.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;

import org.hibernate.Criteria; 
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Restrictions;

import com.ideas2it.ideabus.busStop.BusStop;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.route.Route;
import com.ideas2it.ideabus.route.dao.RouteDAO;
import com.ideas2it.ideabus.util.Constant;
import com.ideas2it.ideabus.util.Factory;

/**
 * Perform create,read,update and delete operations on data.
 */
public class RouteDAOImpl implements RouteDAO {
    private Session session;
    private Transaction transaction;
    private Route route;
    
    /**
     * Add route to route table.
     * @param route
     *          - contains instance of route class.
     */
    public void addRoute(Route route) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.save(route);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_ROUTE_001 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Get All Route detail from the Database and return to Route Service.
     * @return routes.
     *             - All route detail in the list.
     */
    public List<String> getRoutes() throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        List<String> routes = new ArrayList<>();
        try {
            Criteria criteria = session.createCriteria(Route.class);
            ProjectionList projectionList = Projections.projectionList();
            projectionList.add(Projections.property(Constant.NAME));
            criteria.setProjection(projectionList);
            routes = criteria.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_ROUTE_002 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return routes;
    }

    /**
     * Get Route detail from the Database and return to Route Service.
     *
     * @param routeName
     *             - contains name of the route.
     *
     * @return route.
     *             - route detail in the list.
     */
    public List<String> getStops(String routeName) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        List<String> stops = new ArrayList<>();
        try {
            Criteria route = session.createCriteria(Route.class);
            route.add(Restrictions.eq(Constant.NAME,routeName));
            Criteria busStops = route.createCriteria(Constant.BUS_STOPS);
            ProjectionList projectionList = Projections.projectionList();
            projectionList.add(Projections.property(Constant.NAME));
            busStops.setProjection(projectionList);
            stops = busStops.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_ROUTE_002 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return stops;
    }

    /**
     * Get Route detail from the Database and return to Route Service.
     *
     * @param routeName
     *             - contains name of the route.
     *
     * @return route.
     *             - route detail in the list.
     */
    public Route getRouteByName(String routeName) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        route = new Route();
        try {
            Criteria criteria = session.createCriteria(Route.class);
            criteria.add(Restrictions.eq(Constant.NAME,routeName));
            route = (Route)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_ROUTE_002 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return route;
    }
    
    /**
     * Get List of routes from route table and return them.
     *
     * @param fromBusStop
     *             - contains name of the fromBusStop.
     * @param toBusStops
     *             - contains name of the toBusStops.
     *
     * @return a list of routes.
     */
    public List<Route> getRoutes(Integer[] busStopIds) throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        route = new Route();
        List<Route> routes = new LinkedList<>();
        try {
            Criteria routeCriteria = session.createCriteria(Route.class);
            Criteria bsCriteria = routeCriteria.createCriteria("busStops");
            bsCriteria.add(Restrictions.in("id",busStopIds));
            routes = routeCriteria.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_ROUTE_002 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return routes;
    }
        
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Update a route data in route table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw the exception.
     * 5.Finally check and close the session.
     *
     * @param route
     *          - contains instance of route.
     */
    public void updateRoute(Route route)throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.update(route);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_ROUTE_003 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
    
    /**
     * 1.Get instance of Session from Factory and using that session,
     * begin transaction for the open session.
     * 2.Delete a route data in route table and commit the changes.
     * 3.Handle the exception in transaction using rollback.
     * 4.Catch and throw the exception.
     * 5.Finally check and close the session.
     *
     * @param route
     *          - contains instance of route.
     */
    public void deleteRoute(Route route)throws IdeaBusException {
        session = Factory.getSession();
        transaction = session.beginTransaction();
        try {
            session.delete(route);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new IdeaBusException(Constant.ERROR_CODE_ROUTE_004 + e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
