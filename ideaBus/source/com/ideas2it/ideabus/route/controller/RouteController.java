package com.ideas2it.ideabus.route.controller;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
 
import com.ideas2it.ideabus.busStop.BusStop;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.route.Route;
import com.ideas2it.ideabus.route.service.RouteService;
import com.ideas2it.ideabus.route.service.impl.RouteServiceImpl;
import com.ideas2it.ideabus.util.Constant;

/**
 * 1.Use to control the flow of data
 * 2.Handle the exceptions.
 */
public class RouteController {
    private static final Logger logger = Logger.getLogger
        (RouteController.class);
    private RouteService routeService;

    /**
     * Add route data into route table and handle it's exception.
     *
     * @param name
     *          - contains name of route.
     *
     * @return contains a String.
     */
    public String addRoute(String routeName, String busStop) {
        routeService = new RouteServiceImpl();
        try {
            routeService.addRoute(routeName, busStop);
            return Constant.ADDED; 
        } catch (IdeaBusException e) {
            logger.error(e);
            return Constant.ERROR;
        }
    }
    
    /**
     * Get the list of routes from route table and handle it's exception.
     *
     * @return contains list of routes.
     */
    public List<String> getRoutes() {
        routeService = new RouteServiceImpl();
        try {
            return routeService.getRoutes();
        } catch (IdeaBusException e) {
            logger.error(e);
            return Collections.<String>emptyList();
        }
    }
     
    /**
     * Get the list of stops from route table and handle it's exception.
     *
     * @return contains list of routes.
     */
    public Set<BusStop> getBusStops(String route) {
        routeService = new RouteServiceImpl();
        try {
            return routeService.getBusStops(route);
        } catch (IdeaBusException e) {
            logger.error(e);
            return Collections.<BusStop>emptySet();
        }
    }
    
    /**
     * Get the list of stops from route table and handle it's exception.
     *
     * @return contains list of routes.
     */
    public List<String> getStops(String route) {
        routeService = new RouteServiceImpl();
        try {
            return routeService.getStops(route);
        } catch (IdeaBusException e) {
            logger.error(e);
            return Collections.<String>emptyList();
        }
    }
    
    /**
     * Update the route data in route table and hadle it's exception.
     *
     * @param route
     *          - contains name of the route.
     * @param input
     *          - contains input.
     */
    public void updateRoute(String route, String input, String choice) {
        routeService = new RouteServiceImpl();
        try {
            routeService.updateRoute(route,input,choice);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
    }
        
    /**
     * Delete the route data in route table and hadle it's exception.
     *
     * @param route
     *          - contains name of the route.
     */
    public void deleteRoute(String route) {
        routeService = new RouteServiceImpl();
        try {
            routeService.deleteRoute(route);
        } catch (IdeaBusException e) {
            logger.error(e);
        }
    }
}
