package com.ideas2it.ideabus.route.service;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.List;
import java.util.Set;

import com.ideas2it.ideabus.busStop.BusStop;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.route.Route;

public interface RouteService {

    /**
     * Check route exists and pass it to dao.
     * @param route
     *          - contains name of route.
     * @param busStop
     *          - contains names of bus stops
     * @return contains a String.
     */
    String addRoute(String name, String busStop) throws IdeaBusException ;

    /**
     * Check if route exists in route table.
     * @param routeName
     *              - contains name of route.
     */
    boolean checkRoute(String routeName) throws IdeaBusException ;

    /**
     * Get Route detail from the Database and return to Route Service.
     */
    Route getRouteByName(String routeName) throws IdeaBusException ;

    /**
     * Get the list of routes.
     * @return contains list of routes.
     */
    List<String> getRoutes() throws IdeaBusException ;
    
    /**
     * Get the list of stops.
     *
     * @param route
     *          - contains name of a route.
     * @return contains list of stops.
     */
    Set<BusStop> getBusStops(String routeName) throws IdeaBusException ;
      
    /**
     * Get the list of stops.
     *
     * @param route
     *          - contains name of the route.
     *
     * @return contains list of stops.
     */
    List<String> getStops(String routeName) throws IdeaBusException ;
    
    /**
     * Check inputs already exists in table or not.
     * @param input
     *              - contains a String of stop names.
     */
    Set<BusStop> checkStops(String input) throws IdeaBusException ;

    /**
     * Get the list of routes.
     * @return contains list of routes.
     */
    Set<Route> getRoutes(String fromPlace, String toPlace) throws IdeaBusException ;
        
    /**
     * Update route data with the given input in route table.
     *
     * @param naem
     *          - contains name of the route to be updated.
     * @param input
     *          - contains input.
     */
    void updateRoute(String name, String input ,String choice) throws IdeaBusException ;
        
    /**
     * Add the given stop to the route.
     *
     * @param route
     *          - contains instance of the route to be updated.
     * @param input
     *          - contains input.
     */
    void addStop(Route route, String input) throws IdeaBusException ;
            
    /**
     * Remove the given stop from the route.
     *
     * @param route
     *          - contains instance of the route to be updated.
     * @param input
     *          - contains input.
     */
    void deleteStop(Route route, String input) throws IdeaBusException ;
    
    /**
     * Delete route data in route table.
     *
     * @param name
     *          - contains name of the route to be delete.
     */
    void deleteRoute(String name) throws IdeaBusException ;
}
