package com.ideas2it.ideabus.route.service.impl;

/**
 * @author Abi Showkath Ali and Kamal Batcha.
 */
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.ideas2it.ideabus.busStop.BusStop;
import com.ideas2it.ideabus.busStop.service.BusStopService;
import com.ideas2it.ideabus.busStop.service.impl.BusStopServiceImpl;
import com.ideas2it.ideabus.exception.IdeaBusException;
import com.ideas2it.ideabus.route.Route;
import com.ideas2it.ideabus.route.dao.RouteDAO;
import com.ideas2it.ideabus.route.dao.impl.RouteDAOImpl;
import com.ideas2it.ideabus.route.service.RouteService;
import com.ideas2it.ideabus.util.Constant;

public class RouteServiceImpl implements RouteService {
    private BusStopService busStopService;
    private RouteDAO routeDAO;
    
    /**
     * Check route exists and pass it to DAO.
     *
     * @param route
     *          - contains name of route.
     * @param busStop
     *          - contains names of bus stops
     * @return contains a String.
     */
    public String addRoute(String routeName, String busStop)
        throws IdeaBusException {
        boolean routeExist = checkRoute(routeName);
        if (routeExist == true) {
            return (Constant.ALREADY + " " + Constant.ADDED);
        } else {
            Set<BusStop> stops = checkStops(busStop);
            Route route = new Route(routeName, stops);
            routeDAO = new RouteDAOImpl();
            routeDAO.addRoute(route);
        }
        return Constant.ADDED;
    }

    /**
     * Check if route exists in route table.
     * @param routeName
     *              - contains name of route.
     */
    public boolean checkRoute(String routeName) throws IdeaBusException {
        Route route =getRouteByName(routeName);
        if (null != route) {
            return true;
        }
        return false;
    }

    /**
     * Get Route detail from the Database and return to Route Service.
     * @return route.
     *             - route detail in the list.
     */
    public Route getRouteByName(String routeName) throws IdeaBusException {
       routeDAO = new RouteDAOImpl();
       return routeDAO.getRouteByName(routeName);
    }

    /**
     * Get the list of routes.
     * @return contains list of routes.
     */
    public List<String> getRoutes() throws IdeaBusException {
       routeDAO = new RouteDAOImpl();
       return routeDAO.getRoutes();
    }
  
    /**
     * Get the list of stops.
     *
     * @param route
     *          - contains name of the route.
     *
     * @return contains list of stops.
     */
    public Set<BusStop> getBusStops(String routeName) throws IdeaBusException {
       Route route = getRouteByName(routeName);
       return route.getBusStops();
    }  
      
    /**
     * Get the list of stops.
     *
     * @param route
     *          - contains name of the route.
     *
     * @return contains list of stops.
     */
    public List<String> getStops(String routeName) throws IdeaBusException {
       routeDAO = new RouteDAOImpl();
       return routeDAO.getStops(routeName);
    }
    
    /**
     * Check inputs already exists in table or not.
     * @param input
     *              - contains a String of stop names.
     */
    public Set<BusStop> checkStops(String input) throws IdeaBusException {
        BusStopService busStopService = new BusStopServiceImpl();
        BusStop busStop;
        String[] inputs = input.split(",");
        Set<BusStop> stops = new HashSet<>();
        for(String busStopName : inputs){  
            boolean busStopExist = busStopService.checkBusStop(busStopName);
            if (busStopExist == true) {
                busStop = busStopService.getBusStop(busStopName);
                stops.add(busStop);  
            } else {
                stops.add(new BusStop(busStopName)); // add busStop
            } 
        }
        return stops;
    }
     
    /**
     * Get the list of routes.
     * @return contains list of routes.
     */
    public Set<Route> getRoutes(String fromBusStop, String toBusStop)
       throws IdeaBusException {
        BusStopService busStopService = new BusStopServiceImpl();
        BusStop from = busStopService.getBusStop(fromBusStop);
        BusStop to = busStopService.getBusStop(toBusStop);
        Integer[] busStopIds = {from.getId(), to.getId()};
        routeDAO = new RouteDAOImpl();
        List<Route> routes = routeDAO.getRoutes(busStopIds);
        Set<Route> busRoutes = new HashSet<>(routes);
        return busRoutes;
    }
        
    /**
     * Update route data with the given input in route table.
     *
     * @param name
     *          - contains name of the route to be updated.
     * @param input
     *          - contains input.
     * @param choice
     *          - contains choice of operation.
     */
    public void updateRoute(String name, String input, String choice)
        throws IdeaBusException {
        routeDAO = new RouteDAOImpl();
        Route route = getRouteByName(name);
        if ( choice.equals(Constant.UPDATE_1 + Constant.ROUTE_1) ) {
            route.setName(input);
            routeDAO.updateRoute(route);
        } else if ( choice.equals(Constant.ADD + Constant.STOP_1) ) {
            addStop(route,input);
        } else if ( choice.equals(Constant.DELETE_1 + Constant.STOP_1) ) {
            deleteStop(route,input);
        }
    }
    
    /**
     * Add the given stop to the route.
     *
     * @param route
     *          - contains instance of the route to be updated.
     * @param input
     *          - contains input.
     */
    public void addStop(Route route, String input) throws IdeaBusException {
        busStopService = new BusStopServiceImpl();
        Set<BusStop> stops = checkStops(input);
        route.setBusStops(stops);
        routeDAO.updateRoute(route);
    }
        
    /**
     * Remove the given stop from the route.
     *
     * @param route
     *          - contains instance of the route to be updated.
     * @param input
     *          - contains input.
     */
    public void deleteStop(Route route, String input) throws IdeaBusException {
        busStopService = new BusStopServiceImpl();
        Set<BusStop> busStops = route.getBusStops();
        Set<BusStop> stops = checkStops(input);
        for (BusStop busStop : stops) {
            busStops.remove(busStop);
        }
        route.setBusStops(busStops);
        routeDAO.updateRoute(route);
    }
    
    /**
     * Delete route data in route table.
     *
     * @param name
     *          - contains name of the route to be updated.
     */
    public void deleteRoute(String name) throws IdeaBusException {
        routeDAO = new RouteDAOImpl();
        Route route = routeDAO.getRouteByName(name);
        routeDAO.deleteRoute(route);
    }
}
