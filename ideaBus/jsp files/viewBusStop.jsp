<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>List of BusStops</title>
  </head>
  <body>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <form action="/ideaBus/logout">
      <input type="submit" name="submit" value="LogOut"/>
    </form>
    <center>
      <c:forEach items="${busStops}" var="busStop">
        <option>
          <c:out value="${busStop}" />
        </option>
      </c:forEach>
      <form method = "get" action = "/ideaBus/busStop">
        <li><a>Previous</a>
          <input type="submit" name="submit" value="view"/>
          <input type = "hidden" name = "count" value = "previous" />
        </li>
      </form>
      <form method = "get" action = "/ideaBus/busStop">
        <li><a>Next</a>
          <input type="submit" name="submit" value="view"/>
          <input type = "hidden" name = "count" value = "next" />
        </li>
      </form>
    </center>
  </body>
</html>
