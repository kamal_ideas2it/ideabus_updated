<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
<head>
     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
     <link rel="stylesheet" type="text/css" href="css/style.css">
     <title>Update</title>
</head>
<body>
     <%     
         session.removeAttribute("mailId");
         session.removeAttribute("emailId");
         session.invalidate();
     %>
    <form action="/ideaBus/logout">
      <input type="submit" name="submit" value="LogOut"/>
    </form>
<center>
     <h1>You have successfully Updated your profile</h1>
     To login again <a href="../home.html">click here</a>.
</center>
</body>
</html>
