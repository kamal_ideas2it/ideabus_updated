<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%@ page import="java.io.*,java.util.*" %>
<%
 response.addHeader("Cache-Control", "no-cache,no-store,private,must-revalidate,max-stale=0,post-check=0,pre-check=0");
 response.addHeader("Pragma", "no-cache");
 response.addDateHeader ("Expires", 0);
%>
<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>IdeaBus</title>
    </head>
    <body>
      <% 
        if (session != null) { 
            if (session.getAttribute("emailId") == null) { 
                response.sendRedirect("agentLogin.jsp"); 
            } 
        } 
      %>
            <form action="/ideaBus/logout">
                <input type="submit" name="submit" value="LogOut"/>
              </li>
            </form>
      <table  align = "left">
        <li><a href="logout.jsp">Logout</a></li>
      </table>
      <center>
        <table border="1" width="50%" cellpadding="5">
        <thead>
          <tr>
            <th colspan="2">Welcome</th>
          </tr>
        </thead>
        <tbody>
          <ul>
            <form method = "get" action ="/ideaBus/busDetail">
            <input type = "hidden" name = "mailId"
                value = <%=(String)session.getAttribute("emailId")%> />
              <li><a>Add Bus</a>
                <input type="submit" name="submit" value="add"/>
              </li>
              <li><a>view Bus</a>
                <input type="submit" name="submit" value="view"/>
              </li>
              <li><a>update Bus</a>
                <input type="submit" name="submit" value="update"/>
              </li>
            </form>
            <li><a href="addOrganization.jsp">Add Organization</a></li>
            <form method = "get" action = "/ideaBus/organization">
              <li><a>view organizations</a>
                <input type="submit" name="submit" value="view"/>
              </li>
              <li><a>update organization</a>
                <input type="submit" name="submit" value="update"/>
              </li>
              <li><a>delete organization</a>
                <input type="submit" name="submit" value="delete"/>
              </li>
            </form>
            <li><a href="addBusStop.jsp">Add Bus Stops</a></li>
            <form method = "get" action = "/ideaBus/busStop">
              <li><a>view busStops</a>
                <input type="submit" name="submit" value="view"/>
              </li>
              <li><a>update busStop</a>
                <input type="submit" name="submit" value="update"/>
              </li>
              <li><a>delete busStop</a>
                <input type="submit" name="submit" value="delete"/>
              </li>
              <input type = "hidden" name = "count" value = "new" />
            </form>
            <li><a href="addRoute.jsp">Add Routes</a></li>
            <form method = "get" action = "/ideaBus/route">
              <li><a>view routes</a>
                <input type="submit" name="submit" value="view"/>
              </li>
              <li><a>update route</a>
                <input type="submit" name="submit" value="update"/>
              </li>
              <li><a>delete route</a>
                <input type="submit" name="submit" value="delete"/>
              </li>
            </form>
            <li><a href="addType.jsp">Add Types</a></li>
            <form method = "get" action = "/ideaBus/type">
              <li><a>view types</a>
                <input type="submit" name="submit" value="view"/>
              </li>
              <li><a>update type</a>
                <input type="submit" name="submit" value="update"/>
              </li>
              <li><a>delete type</a>
                <input type="submit" name="submit" value="delete"/>
              </li>
            </form>
          </ul>
        </tbody>
        </table>
      </center>
   </body>
</html>
