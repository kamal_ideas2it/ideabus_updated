<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head> 
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /> 
    <title>Login Page</title> 
    <link href="css/style.css" rel="stylesheet" type="text/css" />
  </head> 
  <body bgcolor = "aqua"> 
    <form method="get" action="/ideaBus/register"> 
      <div style="padding: 100px 0 0 250px;"> 
        <div id="login-box">
        <h2>Admin Login Page</h2> 
          Welcome to IdeaBus <br> <br> 
          <div id="login-box-name" style="margin-top:20px;">Email Id:</div> 
          <div id="login-box-field" style="margin-top:20px;"> 
            <input name="emailId" class="form-login"
                title="EmailId" value="" size="30" maxlength="50" /> 
          </div> 
          <div id="login-box-name">Password:</div>
          <div id="login-box-field"> 
            <input name="password" type="password" class="form-login"
                title="Password" value="" size="30" maxlength="50" /> 
          </div>
          <tr>
            <td>Role</td>
            <td>
              <input type="hidden" name="role" value="admin" />
            </td>
          </tr>
          <input style="margin-left:100px;" type="submit"
            name ="submit" value="Login" />
        </div> 
      </div> 
    </form> 
  </body> 
</html> 
