<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%
 response.addHeader("Cache-Control", "no-cache,no-store,private,must-revalidate,max-stale=0,post-check=0,pre-check=0");
 response.addHeader("Pragma", "no-cache");
 response.addDateHeader ("Expires", 0);
%>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title> Passenger </title>
  </head>
  <body>
            <form action="/ideaBus/logout">
                <input type="submit" name="submit" value="LogOut"/>
              </li>
            </form>
  <%String[] seats = request.getParameterValues("seatName");%>
    <%int n = seats.length;;%>
    <%String regNo = request.getParameter("registrationNumber");%>
    <%float ticketPrice = Float.parseFloat(request.getParameter("ticketPrice"));%>
    <%String onward = request.getParameter("onward");%>
    <form method="post" action="/ideaBus/booking">
      <center>
      <% for (int i = 0; i < n; i++) { %>
        <table border="1" width="50%" cellpadding="5">
          <thead>
            <tr>
              <th colspan="2">Enter Information Here</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>First Name</td>
              <td><input type="text" name="firstName<%=i%>" value="" /></td>
            </tr>
            <tr>
              <td>Last Name</td>
              <td><input type="text" name="lastName<%=i%>" value="" /></td>
            </tr>
            <tr>
              <td> Gender </td>
              <td>
                <form>
                  <input type="radio" name="gender<%=i%>" value="male" checked> 
                  Male
                  <input type="radio" name="gender<%=i%>" value="female"> Female
                  <input type="radio" name="gender<%=i%>" value="other"> Other  
                </form>
              </td>
            </tr>
            <tr>
              <td> Age </td>
              <td><input type="text" name="age<%=i%>" min = "0" max="100" 
                                                                value="" /></td>
            </tr>
          </tbody>
        </table>
        <%}%>
        <input type = "hidden" name = "numberOfPassengers" value = "<%=n%>"/>
        <tr>
          <td>Email Id</td>
          <td><input type="text" name="emailId" value="" /></td>
        </tr>
        <tr>
          <td>Phone Number</td>
          <td>
            <input type="text" min = "6999999999" max="9999999999" 
                   name="phoneNumber" value="" />
          </td>
        </tr>
        <tr>
          <td>Date of Booking</td>
          <td><input type="text" name="date" 
                             value="<%= new java.util.Date() %>" readonly/></td>
        </tr>
        <tr>
          <td>Journey date</td>
          <td><input type="text" name="journeyDate" value="<%=onward%>" 
                                                                 readonly/></td>
        </tr>
        <tr>
      <% for (int i = 0; i < n; i++) { %>
          <td><input type="hidden" name="seatName<%=i%>" 
                             value="<%= seats[i] %>" readonly/></td>
        <%}%>
        </tr>  
        <tr>
          <td>Enter Card Number</td>
          <td><input type="number" name="cardNumber" min = "99999999"
                                         max="9999999999999999" value="" /></td>
        </tr>
        <tr>
          <td>Enter Name on card</td>
          <td><input type="text" name="cardName" value="" /></td>
        </tr>
        <tr>
          <td>Total Amount</td>
          <td><input type="text" name="totalAmount" 
                                      value="<%=ticketPrice*n%>" readonly/></td>
        </tr>
        <tr>
          <td>Date on Payment</td>
          <td><input type="text" name="date"
                             value="<%= new java.util.Date() %>" readonly/></td>
        </tr>
        <tr>
          <td>Bus Reg Number</td>
          <td><input type="text" name="registerNumber" 
                                              value="<%=regNo%>" readonly/></td>
        </tr>
        <tr>                 
          <td><input type="submit" value="Submit" /></td>
          <td><input type="reset" value="Reset" /></td>
        </tr>
        <button type="button" name="back" onclick="history.back()">
        back
        </button> 
      </center>
    </form>
  </body>
</html>
