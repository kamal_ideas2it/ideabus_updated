<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Registration</title>
  </head>
    <body>
    <%
		if (session != null) {
			if (session.getAttribute("emailId") != null) {
				String name = (String) session.getAttribute("agent");
				out.print("Hello,  Welcome to ur Profile");
			} else {
				response.sendRedirect("adminLogin.jsp");
			}
		}
	%>
            <form action="/ideaBus/logout">
                <input type="submit" name="submit" value="LogOut"/>
              </li>
            </form>
      <form method="post" action="/ideaBus/register">
        <center>
          <table border="1" width="50%" cellpadding="5">
          <thead>
            <tr>
              <th colspan="2">Enter Information Here</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>First Name</td>
              <td><input type="text" name="firstName" value="" /></td>
            </tr>
            <tr>
              <td>Last Name</td>
              <td><input type="text" name="lastName" value="" /></td>
            </tr>
              <tr>
              <td>Role</td>
              <td><input type="radio" name="role" value="agent" checked> Agent</td>
            </tr>
            <tr>
              <td> Gender </td>
              <td>
                        <input type="radio" name="gender" value="male" checked> Male
                        <input type="radio" name="gender" value="female"> Female
                        <input type="radio" name="gender" value="other"> Other  
                      </td>
                    </tr>
                    <tr>
                      <td> Age </td>
                      <td><input type="text" name="age" value="" /></td>
                    </tr>
                    <tr>
                      <td>Phone Number</td>
                      <td><input type="text" min = "6999999999" max="9999999999"
                      name="phoneNumber" value="" /></td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td><input type="text" name="emailId" value="" /></td>
                    </tr>
                    <tr>
                      <td>Password</td>
                      <td><input type="password" name="password" value="" /></td>
                    <tr>
                    <tr>                       
                      <td><input type="submit" value="Submit" /></td>
                      <td><input type="reset" value="Reset" /></td>
                    </tr>
                    <tr>
                      <td colspan="2">Already registered!! <a href="agentLogin.jsp">Login Here</a></td>
                    </tr>
                  <button type="button" name="back" onclick="history.back()">back</button>
          </tbody>
        </table>
      </center>
    </form>
  </body>
</html>
