<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <title>Update</title>
  </head>
  <body>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <form action="/ideaBus/logout">
      <input type="submit" name="submit" value="LogOut"/>
    </form>
    <form method="get"  action="/ideaBus/busDetail">
      <center>
        <table border="1" width="50%" cellpadding="5">
          <thead>
            <tr>
              <th colspan="2">Enter Information Here</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Travels</td>
              <td>
                <input type="text" name="organization" var="busDetail"
                  value= "${busDetail.getOrganization().getName()}" />
              </td>
            </tr>
            <tr>
              <td>Route</td>
              <td>
                <select name="route" id="route">
                  <c:forEach items="${routes}" var="route">
                    <option>
                      <c:out value="${route}" />
                    </option>
                  </c:forEach>
                </select>
              </td>
            </tr>
            <tr>
              <td>Type</td>
              <td>
                <input type = "text" name = "type" value= "" />
              </td>
            </tr>
            <tr>
              <td>Triket price in Rs.</td>
              <td>
                <input type = "number" name = "price" var="busDetail"
                  value= "${busDetail.getPrice()}" />
              </td>
            </tr>
            <tr>
              <td>Register number</td>
              <td>
                <input type="text" name="registerNumber" var="busDetail"
                  value = "${busDetail.getRegistrationNumber()}" />
              </td>
            </tr>
            <tr>
              <td>Onward Date</td>
              <td>
                <input type="text" name="date" var="busDetail"
                  value = "${busDetail.getDate()}" />
              </td>
            </tr>
            <tr>
              <td>
                <input type="submit" name="submit" value="Update" />
              </td>
            </tr>
            <input type="hidden" name="registrationNumber" var="busDetail"
                value = "${busDetail.getRegistrationNumber()}" />
            <button type="button" name="back" onclick="history.back()">back</button>
          </tbody>
          </table>
        </center>
      </form>
  </body>
</html>
