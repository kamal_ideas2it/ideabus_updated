<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%@ page import="java.io.*,java.util.*" %>
<%
 response.addHeader("Cache-Control", "no-cache,no-store,private,must-revalidate,max-stale=0,post-check=0,pre-check=0");
 response.addHeader("Pragma", "no-cache");
 response.addDateHeader ("Expires", 0);
%>
  <html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>IdeaBus</title>
    </head>
    <body>
            <form action="/ideaBus/logout">
                <input type="submit" name="submit" value="LogOut"/>
            </form>
    <form method="get" action="/ideaBus/register">
      <% 
        if (session != null) { 
            if (session.getAttribute("emailId") == null) { 
                response.sendRedirect("customerLogin.jsp"); 
            } 
        } 
      %>
      <center>
        <table border="1" width="50%" cellpadding="5">
        <thead>
          <tr>
            <th colspan="2">Welcome</th>
          </tr>
        </thead>
        <tbody>
          <ul>
            <li>
              <input type="radio" name="role" value="customer" checked>Customer</input>
            </li>
              <li><a>view profile</a>
                <input type="submit" name="submit" value="view"/>
              </li>
            <li>
                <a>update profile</a>
                <input type="submit" name="submit" value="update">
            </li>
            <li>
                <a>delete profile</a>
                <input type="submit" name="submit" value="delete">
            </li>
            <li><a href="searchPlace.jsp">Book Ticket</a></li>
            <li><a href="logout.jsp">Logout</a></li>
          </ul>
        </tbody>
        </table>
      </form>
            <form method="get" action="/ideaBus/booking">
              <a>Cancel Ticket</a>
              <input type="submit" name="submit" value="show">
            </form> 
    </center>
  </body>
</html>
