

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <title>search</title>
  </head>
  <body>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <form action="/ideaBus/logout">
      <input type="submit" name="submit" value="LogOut"/>
    </form>
    <form method="post"  action="jsp files/addPassenger.jsp">
      <center>
        <table border="1" width="50%" cellpadding="5">
          <thead>
            <tr>
              <th colspan="2">Select Seat</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <table>
                <tr>
                  <td>Seat Name</td>
                  <td>
                    <c:forEach items="${seats}" var="seat">
                      <input type="checkbox" name="seatName" value="${seat.getName()}"/>
                      <c:out value="${seat.getName()}" />
                    </c:forEach>
                  </td>
                </tr>
              </table>
            </tr>
            <tr>
              <td><input type="text" name="registrationNumber" 
                                   value="${registerNumber}" readonly/></td>
            </tr>
            <tr>
              <td><input type="hidden" 
                               name="ticketPrice" value="${ticketPrice}" /></td>
            </tr>
            <tr>
              <td><input type="hidden" name="onward" value="${onward}" /></td>
            </tr>
            <tr align = "center">
              <td>
                <input type="submit" name="submit" value="select" >
              </td>
            </tr>
            <button type="button" name="back" onclick="history.back()">
            back
            </button>
          </tbody>
        </table>
      </center>
    </form>
  </body>
</html>


