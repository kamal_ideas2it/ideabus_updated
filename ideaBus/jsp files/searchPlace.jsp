<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <title>search</title>
  </head>
  <body>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <form action="/ideaBus/logout">
      <input type="submit" name="submit" value="LogOut"/>
    </form>
    <form method="get"  action="/ideaBus/busDetail">
      <center>
        <table border="1" width="50%" cellpadding="5">
          <thead>
            <tr>
              <th colspan="4">Select</th>
            </tr>
          </thead>
          <tbody>
          <tr>
            <td>From</td>
            <td>To</td>
            <td>Onward (eg: 12/06/2016)</td>
            <td>return (eg: 12/06/2016)</td>
          </tr>
          <tr>
            <td><input type="text" name="fromPlace" value="" /></td>
            <td><input type="text" name="toPlace" value="" /></td>
            <td><input type="date" name="onward" value = "12/12/2017"></td>
            <td><input type="date" name="return"></td>
          </tr>
          <tr align = "center">
            <td colspan="4">
              <input type="submit" name="submit" value="search" >
            </td>
          </tr>
            <button type="button" name="back" onclick="history.back()">
              back
            </button>
          </tbody>
        </table>
      </center>
    </form>        
  </body>
</html>
