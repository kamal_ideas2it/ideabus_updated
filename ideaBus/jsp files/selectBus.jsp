<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <title>search</title>
  </head>
  <body>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <form action="/ideaBus/logout">
      <input type="submit" name="submit" value="LogOut"/>
    </form>
    <form method="get"  action="/ideaBus/busDetail">
      <center>
        <table border="1" width="50%" cellpadding="5">
          <thead>
            <tr>
              <th colspan="2">Select Bus</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <table>
                <c:forEach items="${busDetails}" var="busDetail">
            <tr>
              <td>Bus Register Number</td>
              <td><input type="text" name="registerNumber" value="${busDetail.getRegistrationNumber()}" readonly/></td>
            </tr>
            <tr>
              <td>Price</td>
              <td><input type="number" name="price" value="${busDetail.getPrice()}" readonly /></td>
            </tr>
            <tr>
              <td>Total Seat</td>
              <td><input type="number" name="price" value="${busDetail.getTotalSeats()}" readonly /></td>
            </tr>
            <tr>
              <td><input type="hidden" name="onward" value="${onward}"/></td>
            </tr>
              <tr align = "center">
              <td>
                <input type="submit" name="submit" value="select" >
              </td>
            </tr>
            </c:forEach>
            <button type="button" name="back" onclick="history.back()">
              back
            </button>
          </tbody>
        </table>
      </center>
    </form>        
  </body>
</html>
