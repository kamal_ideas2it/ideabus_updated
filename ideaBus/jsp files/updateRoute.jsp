<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>List of Routes</title>
  </head>
  <body>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <form action="/ideaBus/logout">
      <input type="submit" name="submit" value="LogOut"/>
    </form>
    <form method="get" action="/ideaBus/route">
      <center>
        <select name = "route" id = "route">
          <c:forEach items="${routes}" var="route">
            <option> 
            <c:out value="${route}" />
            </option>
          </c:forEach>
        </select>
        <br><input type="text" name="name" value="" /></br>
        <br>
          <input type = "radio" name = "update" value = "updateRoute" />update route
          <input type = "radio" name = "update" value = "addStop" />add stop
          <input type = "radio" name = "update" value = "deleteStop" />remove stop
        </br>
        <br><input type="submit" name="submit" value="update route"/></br>
      </center>
    </form>
  </body>
</html>
