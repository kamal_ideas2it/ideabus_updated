<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <title>search</title>
  </head>
  <body>
    <form action="/ideaBus/logout">
      <input type="submit" name="submit" value="LogOut"/>
    </form>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@page contentType="text/html" import="java.util.*" %>
    <%int n = Integer.parseInt(request.getParameter("numberOfPassenger"));%>
    <%String[] seats = request.getParameterValues("seatName");%>
    <form method="get"  action="booking">
      <center>
        <table border="1" width="50%" cellpadding="5">
          <thead>
            <tr>
              <th colspan="2">Select Bus</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Enter Card Number</td>
              <td><input type="number" min = "10" max="20"  
                         name="cardNumber" value="" /></td>
            </tr>
            <tr>
              <td>Enter Name on card</td>
              <td><input type="text" name="cardName" value="" /></td>
            </tr>
            <tr>
              <td>Total Amount</td>
              <td><input type="text" name="totalAmount" value="" /></td>
            </tr>
            <tr>
              <td>Date on Payment</td>
              <td><input type="text" name="date" value="" /></td>
            </tr>
            <center>
              <% for (int i = 0; i < n; i++) { %>
              <table border="1" width="50%" cellpadding="5">
                <thead>
                  <tr>
                    <th colspan="2">Enter Information Here</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>First Name</td>
                    <td><input type="text" name="firstName<%=i%>" 
                                           value="firstName<%=i%>" /></td>
                  </tr>
                  <tr>
                    <td>Last Name</td>
                    <td><input type="text" name="lastName<%=i%>" 
                                           value="lastName<%=i%>" /></td>
                  </tr>
                  <tr>
                    <td> Gender </td>
                    <td>
                      <form>
                        <td><input type="text" name="gender<%=i%>" 
                                               value="gender<%=i%>" /></td>
                      </form>
                    </td>
                  </tr>
                  <tr>
                    <td> Age </td>
                    <td><input type="text" name="age<%=i%>" value="age<%=i%>" />
                    </td>
                  </tr>
                </tbody>
              </table>
              <%}%>
              <%  for (String seat : seats) {%>
              <td><input type="text" name="seat" value="<%=seat%>" /></td>
              <%}%>
                <input type = "hidden" name = "numberOfPassengers" 
                                                    value = "<%=n%>"/>
              <tr>
                <td>Email Id</td>
                <td><input type="text" name="emailId" value="" /></td>
              </tr>
              <tr>
                <td>Phone Number</td>
                <td>
                  <input type="text" min = "6999999999" max="9999999999" 
                                                  name="phoneNumber" value="" />
                </td>
              </tr>
              <tr>
                <td>Date of Booking</td>
                <td><input type="text" name="date"
                                     value="<%= new java.util.Date() %>" /></td>
              </tr>
              <tr>
                <td>Journey date</td>
                <td><input type="text" name="journeyDate" value="" /></td>
              </tr>
              <tr>
                <td> Seat </td>
                <td><input type="text" name="seatName" value="" /></td>
              </tr>  
              <tr align = "center">
                <td>
                  <input type="submit" name="submit" value="select" >
                </td>
              </tr>
              <button type="button" name="back" onclick="history.back()">
              back
              </button>
            </center>
          </tbody>
        </table>
      </center>
    </form>        
  </body>
</html>
