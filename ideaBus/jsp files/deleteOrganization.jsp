<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
  <head>
    <meta http-equiv="Content-organization" content="text/html; charset=UTF-8">
    <title>List of Organizations</title>
  </head>
  <body>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
    <form action="/ideaBus/logout">
      <input type="submit" name="submit" value="LogOut"/>
    </form>
    <form method="get" action="/ideaBus/organization">
      <center>
        <select name = "organization" id = "organization">
          <c:forEach items="${organizations}" var="organization">
            <option> 
            <c:out value="${organization}" />
            </option>
          </c:forEach>
        </select>
        <input type="submit" name="submit" value="delete organization"/>
      </center>
    </form>  
  </body>
</html>
