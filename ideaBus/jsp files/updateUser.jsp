<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <title>Update</title>
  </head>
  <body>
    <form action="/ideaBus/logout">
      <input type="submit" name="submit" value="LogOut"/>
    </form>
    <form method="get"  action="/ideaBus/register">
      <center>
        <table border="1" width="50%" cellpadding="5">
          <thead>
            <tr>
              <th colspan="2">Enter Information Here</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>First Name</td>
              <td>
                <input type="text" name="firstName" var="user"
                  value= "${user.getFirstName()}" />
              </td>
            </tr>
            <tr>
              <td>Last Name</td>
              <td>
                <input type="text" name="lastName" var="user"
                  value= "${user.getLastName()}" /> 
              </td>
            </tr>
            <tr>
              <td>Gender</td>
              <td>
                <input type="radio" name="gender"  value="male" checked/> Male
                <input type="radio" name="gender"  value="female"/> Female
                <input type="radio" name="gender"  value="other"/> Other
              </td>
            </tr>
            <tr>
              <td>Age</td>
              <td>
                <input type="number" name="age" var="user"
                  value= "${user.getAge()}" />
              </td>
            </tr>
            <tr>
              <td>Phone Number</td>
              <td>
                <input type="number" name="phoneNumber" var="user"
                  value= "${user.getPhone()}" />
              </td>
            </tr>
            <tr>
              <td>Email</td>
              <td>
                <input type="text" name="emailId" var="user"
                  value= "${user.getEmailId()}" />
              </td>
            </tr>
            <tr>
              <td>Password</td>
              <td>
                <input type="password" name="password" value="" />
              </td>
            </tr>
            <input type="hidden" id="thisField" name="status" value="active">
            <tr>
              <td>
                <input type="submit" name="submit" value="Update" />
              </td>
            </tr>
            <button type="button" name="back" onclick="history.back()">back</button>
          </tbody>
          </table>
        </center>
      </form>
  </body>
</html>
