<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>Bus Details</title>
  </head>
  <body>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <form action="/ideaBus/logout">
      <input type="submit" name="submit" value="LogOut"/>
    </form>
    <center>
    <table border="1" width="50%" cellpadding="5">
    <thead>
      <tr>
        <th colspan="6">List of Buses</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Serial.No</td>
        <td>Travels</td>
        <td>Route</td>
        <td>Type</td>
        <td>Ticket price</td>
        <td>Bus Status</td>
        <td>Register number</td>
      </tr>
      <tr>
        <c:forEach items="${busDetails}" var="busDetail">
        <tr>
          <td>
            <c:out value="${busDetail.getId()}" />
          </td>
          <td>
            <c:out value="${busDetail.getOrganization().getName()}" />
          </td>
          <td>
            <c:out value="${busDetail.getRoute().getName()}" />
          </td>
          <td>
            <c:forEach items="${busDetail.getTypes()}" var = "type">
              <c:out value="${type.getName()}" /><br>
            </c:forEach>
          </td>
          <td>
            <c:out value="${busDetail.getPrice()}" />
          </td>
          <td>
            <c:out value="${busDetail.getStatus()}" />
          </td>
          <td>
            <c:out value="${busDetail.getRegistrationNumber()}" />
          </td>
          <td>
            <c:out value="${busDetail.getDate()}" />
          </td>
        </tr>
        </c:forEach>
      </tr>
    </tbody>
    </table>
    <button type="button" name="back" onclick="history.back()">back</button>
    </center>
  </body>
</html>
