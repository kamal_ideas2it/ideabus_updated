<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <title>logout Page</title>
  </head>
  <body>
<%@ page import="java.io.*,java.util.*" %>
    <%     
      session.removeAttribute("emailId");
      session.invalidate();
      response.setIntHeader("Refresh", 10);
    %>
<%
for (Cookie cookie : request.getCookies()) {
    cookie.setValue("");
    cookie.setMaxAge(0);
    cookie.setPath("/");

    response.addCookie(cookie);
}
%>
<%
 response.addHeader("Cache-Control", "no-cache,no-store,private,must-revalidate,max-stale=0,post-check=0,pre-check=0");
 response.addHeader("Pragma", "no-cache");
 response.addDateHeader ("Expires", 0);
%>
<%
 String emailId = null;
 Cookie[] cookies = request.getCookies();
 if (cookies !=null) {
     for (Cookie cookie : cookies) {
        if (cookie.getName().equals("emailId")) {
            emailId = cookie.getValue();
        }
     }
 }
%>
    <center>
      <h1>You have successfully logged out</h1>
      To login again <a href="../home.html">click here</a>.
    </center>
  </body>
</html>
