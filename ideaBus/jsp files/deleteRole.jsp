<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>List of Roles</title>
  </head>
  <body>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <form action="/ideaBus/logout">
      <input type="submit" name="submit" value="LogOut"/>
    </form>
    <form method="get" action="/ideaBus/role">
      <center>
        <select name = "role" id = "role">
          <c:forEach items="${roles}" var="role">
            <option> 
            <c:out value="${role}" />
            </option>
          </c:forEach>
        </select>
        <input type="submit" name="submit" value="delete role"/>
      </center>
    </form>
  </body>
</html>
