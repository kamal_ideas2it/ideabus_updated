<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Bus Detail</title>
  </head>
  <body>
    <%
		if (session != null) {
			if (session.getAttribute("emailId") != null) {
				String name = (String) session.getAttribute("agent");
			} else {
				response.sendRedirect("agentLogin.jsp");
			}
		}
	%>
            <form action="/ideaBus/logout">
                <input type="submit" name="submit" value="LogOut"/>
              </li>
            </form>
    <form method="post" action="/ideaBus/busDetail">
      <input type = "hidden" name = "mailId"
                        value = <%=(String)session.getAttribute("emailId")%> />
      <center>
        <table border="1" width="50%" cellpadding="5">
          <thead>
            <tr>
              <th colspan="2">Enter Information Here</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Bus Name</td>
              <td><input type="text" name="name" value="" /></td>
            </tr>
            <tr>
              <td>Registration Number</td>
              <td><input type="text" name="registerNumber" value="" /></td>
            </tr>
            <tr>
              <td>Route</td>
              <td>
                <select name = "route">
                  <c:forEach items="${routes}" var="route">
                    <option>
                      <c:out value="${route}" />
                    </option>
                  </c:forEach>
                </select>
              </td>
            </tr>
            <tr>
              <td>Bus Type<br>(eg:AC, NON-AC, Sleeper, Semi-Sleeper..)</td>
              <td>
                <input type="text" name="busType" value="" />
              </td>
            </tr>
            <tr>
              <td>Add Seat Name<br>(eg:A1, A2, B1, B2..)</td>
              <td>
                <input type="text" name="seatName" value="" />
              </td>
            </tr>
            <tr>
            <td>Price</td>
              <td>
                <input type="number" name="price" value="" />
              </td>
            </tr>   
            <tr>
            <td>Date</td>
              <td>
                <input type="text" name="date" value="" />
              </td>
            </tr> 
            <tr>                       
              <td><input type="submit" value="Submit" /></td>
              <td><input type="reset" value="Reset" /></td>
            </tr> 
          </tbody>
        </table>
      </center>
    </form>
  </body>
</html>
