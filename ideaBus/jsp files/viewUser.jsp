<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
  <head>
    <title>Your Profile</title>
  </head>
  <body>
      <form action="/ideaBus/logout">
      <input type="submit" name="submit" value="LogOut"/>
    </form>
    <center>
      <table border="1" width="50%" cellpadding="5">
          <thead>
            <tr>
              <th colspan="2">Profile</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>First Name</td>
              <td>
                ${user.getFirstName()}
              </td>
            </tr>
            <tr>
              <td>Last Name</td>
              <td>
                ${user.getLastName()} 
              </td>
            </tr>
            <tr>
              <td>Gender</td>
              <td>
                ${user.getGender()}
              </td>
            </tr>
            <tr>
              <td>Age</td>
              <td>
                ${user.getAge()}
              </td>
            </tr>
            <tr>
              <td>Phone Number</td>
              <td>
                ${user.getPhone()}
              </td>
            </tr>
            <tr>
              <td>Email</td>
              <td>
                ${user.getEmailId()}
              </td>
            </tr>
            <tr>
              <td>
                <button type="button" name="back" onclick="history.back()">
                  back
                </button>
              </td>
            </tr>
            
          </tbody>
          </table>
    </center>
  </body>
</html>
