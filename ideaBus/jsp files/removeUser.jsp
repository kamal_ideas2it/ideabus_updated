<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <title>Removed Page</title>
  </head>
  <body>
    <%     
        session.removeAttribute("emailId");
        session.invalidate();
    %>
    <form action="/ideaBus/logout">
      <input type="submit" name="submit" value="LogOut"/>
    </form>
    <center>
      <h1>You have Removed your account successfully</h1>
      Create again <a href="../home.html">click here</a>.
    </center>
  </body>
</html>
