<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>List of Roles</title>
  </head>
  <body>
    <form action="/ideaBus/logout">
      <input type="submit" name="submit" value="LogOut"/>
    </form>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <center>
      <c:forEach items="${roles}" var="role">
        <br>
          <c:out value="${role}" />
        </br>
      </c:forEach>
    </center>
  </body>
</html>
